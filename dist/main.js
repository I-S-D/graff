/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/events/events.js":
/*!***************************************!*\
  !*** ./node_modules/events/events.js ***!
  \***************************************/
/***/ ((module) => {

// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



var R = typeof Reflect === 'object' ? Reflect : null
var ReflectApply = R && typeof R.apply === 'function'
  ? R.apply
  : function ReflectApply(target, receiver, args) {
    return Function.prototype.apply.call(target, receiver, args);
  }

var ReflectOwnKeys
if (R && typeof R.ownKeys === 'function') {
  ReflectOwnKeys = R.ownKeys
} else if (Object.getOwnPropertySymbols) {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target)
      .concat(Object.getOwnPropertySymbols(target));
  };
} else {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target);
  };
}

function ProcessEmitWarning(warning) {
  if (console && console.warn) console.warn(warning);
}

var NumberIsNaN = Number.isNaN || function NumberIsNaN(value) {
  return value !== value;
}

function EventEmitter() {
  EventEmitter.init.call(this);
}
module.exports = EventEmitter;
module.exports.once = once;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._eventsCount = 0;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
var defaultMaxListeners = 10;

function checkListener(listener) {
  if (typeof listener !== 'function') {
    throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
  }
}

Object.defineProperty(EventEmitter, 'defaultMaxListeners', {
  enumerable: true,
  get: function() {
    return defaultMaxListeners;
  },
  set: function(arg) {
    if (typeof arg !== 'number' || arg < 0 || NumberIsNaN(arg)) {
      throw new RangeError('The value of "defaultMaxListeners" is out of range. It must be a non-negative number. Received ' + arg + '.');
    }
    defaultMaxListeners = arg;
  }
});

EventEmitter.init = function() {

  if (this._events === undefined ||
      this._events === Object.getPrototypeOf(this)._events) {
    this._events = Object.create(null);
    this._eventsCount = 0;
  }

  this._maxListeners = this._maxListeners || undefined;
};

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function setMaxListeners(n) {
  if (typeof n !== 'number' || n < 0 || NumberIsNaN(n)) {
    throw new RangeError('The value of "n" is out of range. It must be a non-negative number. Received ' + n + '.');
  }
  this._maxListeners = n;
  return this;
};

function _getMaxListeners(that) {
  if (that._maxListeners === undefined)
    return EventEmitter.defaultMaxListeners;
  return that._maxListeners;
}

EventEmitter.prototype.getMaxListeners = function getMaxListeners() {
  return _getMaxListeners(this);
};

EventEmitter.prototype.emit = function emit(type) {
  var args = [];
  for (var i = 1; i < arguments.length; i++) args.push(arguments[i]);
  var doError = (type === 'error');

  var events = this._events;
  if (events !== undefined)
    doError = (doError && events.error === undefined);
  else if (!doError)
    return false;

  // If there is no 'error' event listener then throw.
  if (doError) {
    var er;
    if (args.length > 0)
      er = args[0];
    if (er instanceof Error) {
      // Note: The comments on the `throw` lines are intentional, they show
      // up in Node's output if this results in an unhandled exception.
      throw er; // Unhandled 'error' event
    }
    // At least give some kind of context to the user
    var err = new Error('Unhandled error.' + (er ? ' (' + er.message + ')' : ''));
    err.context = er;
    throw err; // Unhandled 'error' event
  }

  var handler = events[type];

  if (handler === undefined)
    return false;

  if (typeof handler === 'function') {
    ReflectApply(handler, this, args);
  } else {
    var len = handler.length;
    var listeners = arrayClone(handler, len);
    for (var i = 0; i < len; ++i)
      ReflectApply(listeners[i], this, args);
  }

  return true;
};

function _addListener(target, type, listener, prepend) {
  var m;
  var events;
  var existing;

  checkListener(listener);

  events = target._events;
  if (events === undefined) {
    events = target._events = Object.create(null);
    target._eventsCount = 0;
  } else {
    // To avoid recursion in the case that type === "newListener"! Before
    // adding it to the listeners, first emit "newListener".
    if (events.newListener !== undefined) {
      target.emit('newListener', type,
                  listener.listener ? listener.listener : listener);

      // Re-assign `events` because a newListener handler could have caused the
      // this._events to be assigned to a new object
      events = target._events;
    }
    existing = events[type];
  }

  if (existing === undefined) {
    // Optimize the case of one listener. Don't need the extra array object.
    existing = events[type] = listener;
    ++target._eventsCount;
  } else {
    if (typeof existing === 'function') {
      // Adding the second element, need to change to array.
      existing = events[type] =
        prepend ? [listener, existing] : [existing, listener];
      // If we've already got an array, just append.
    } else if (prepend) {
      existing.unshift(listener);
    } else {
      existing.push(listener);
    }

    // Check for listener leak
    m = _getMaxListeners(target);
    if (m > 0 && existing.length > m && !existing.warned) {
      existing.warned = true;
      // No error code for this since it is a Warning
      // eslint-disable-next-line no-restricted-syntax
      var w = new Error('Possible EventEmitter memory leak detected. ' +
                          existing.length + ' ' + String(type) + ' listeners ' +
                          'added. Use emitter.setMaxListeners() to ' +
                          'increase limit');
      w.name = 'MaxListenersExceededWarning';
      w.emitter = target;
      w.type = type;
      w.count = existing.length;
      ProcessEmitWarning(w);
    }
  }

  return target;
}

EventEmitter.prototype.addListener = function addListener(type, listener) {
  return _addListener(this, type, listener, false);
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.prependListener =
    function prependListener(type, listener) {
      return _addListener(this, type, listener, true);
    };

function onceWrapper() {
  if (!this.fired) {
    this.target.removeListener(this.type, this.wrapFn);
    this.fired = true;
    if (arguments.length === 0)
      return this.listener.call(this.target);
    return this.listener.apply(this.target, arguments);
  }
}

function _onceWrap(target, type, listener) {
  var state = { fired: false, wrapFn: undefined, target: target, type: type, listener: listener };
  var wrapped = onceWrapper.bind(state);
  wrapped.listener = listener;
  state.wrapFn = wrapped;
  return wrapped;
}

EventEmitter.prototype.once = function once(type, listener) {
  checkListener(listener);
  this.on(type, _onceWrap(this, type, listener));
  return this;
};

EventEmitter.prototype.prependOnceListener =
    function prependOnceListener(type, listener) {
      checkListener(listener);
      this.prependListener(type, _onceWrap(this, type, listener));
      return this;
    };

// Emits a 'removeListener' event if and only if the listener was removed.
EventEmitter.prototype.removeListener =
    function removeListener(type, listener) {
      var list, events, position, i, originalListener;

      checkListener(listener);

      events = this._events;
      if (events === undefined)
        return this;

      list = events[type];
      if (list === undefined)
        return this;

      if (list === listener || list.listener === listener) {
        if (--this._eventsCount === 0)
          this._events = Object.create(null);
        else {
          delete events[type];
          if (events.removeListener)
            this.emit('removeListener', type, list.listener || listener);
        }
      } else if (typeof list !== 'function') {
        position = -1;

        for (i = list.length - 1; i >= 0; i--) {
          if (list[i] === listener || list[i].listener === listener) {
            originalListener = list[i].listener;
            position = i;
            break;
          }
        }

        if (position < 0)
          return this;

        if (position === 0)
          list.shift();
        else {
          spliceOne(list, position);
        }

        if (list.length === 1)
          events[type] = list[0];

        if (events.removeListener !== undefined)
          this.emit('removeListener', type, originalListener || listener);
      }

      return this;
    };

EventEmitter.prototype.off = EventEmitter.prototype.removeListener;

EventEmitter.prototype.removeAllListeners =
    function removeAllListeners(type) {
      var listeners, events, i;

      events = this._events;
      if (events === undefined)
        return this;

      // not listening for removeListener, no need to emit
      if (events.removeListener === undefined) {
        if (arguments.length === 0) {
          this._events = Object.create(null);
          this._eventsCount = 0;
        } else if (events[type] !== undefined) {
          if (--this._eventsCount === 0)
            this._events = Object.create(null);
          else
            delete events[type];
        }
        return this;
      }

      // emit removeListener for all listeners on all events
      if (arguments.length === 0) {
        var keys = Object.keys(events);
        var key;
        for (i = 0; i < keys.length; ++i) {
          key = keys[i];
          if (key === 'removeListener') continue;
          this.removeAllListeners(key);
        }
        this.removeAllListeners('removeListener');
        this._events = Object.create(null);
        this._eventsCount = 0;
        return this;
      }

      listeners = events[type];

      if (typeof listeners === 'function') {
        this.removeListener(type, listeners);
      } else if (listeners !== undefined) {
        // LIFO order
        for (i = listeners.length - 1; i >= 0; i--) {
          this.removeListener(type, listeners[i]);
        }
      }

      return this;
    };

function _listeners(target, type, unwrap) {
  var events = target._events;

  if (events === undefined)
    return [];

  var evlistener = events[type];
  if (evlistener === undefined)
    return [];

  if (typeof evlistener === 'function')
    return unwrap ? [evlistener.listener || evlistener] : [evlistener];

  return unwrap ?
    unwrapListeners(evlistener) : arrayClone(evlistener, evlistener.length);
}

EventEmitter.prototype.listeners = function listeners(type) {
  return _listeners(this, type, true);
};

EventEmitter.prototype.rawListeners = function rawListeners(type) {
  return _listeners(this, type, false);
};

EventEmitter.listenerCount = function(emitter, type) {
  if (typeof emitter.listenerCount === 'function') {
    return emitter.listenerCount(type);
  } else {
    return listenerCount.call(emitter, type);
  }
};

EventEmitter.prototype.listenerCount = listenerCount;
function listenerCount(type) {
  var events = this._events;

  if (events !== undefined) {
    var evlistener = events[type];

    if (typeof evlistener === 'function') {
      return 1;
    } else if (evlistener !== undefined) {
      return evlistener.length;
    }
  }

  return 0;
}

EventEmitter.prototype.eventNames = function eventNames() {
  return this._eventsCount > 0 ? ReflectOwnKeys(this._events) : [];
};

function arrayClone(arr, n) {
  var copy = new Array(n);
  for (var i = 0; i < n; ++i)
    copy[i] = arr[i];
  return copy;
}

function spliceOne(list, index) {
  for (; index + 1 < list.length; index++)
    list[index] = list[index + 1];
  list.pop();
}

function unwrapListeners(arr) {
  var ret = new Array(arr.length);
  for (var i = 0; i < ret.length; ++i) {
    ret[i] = arr[i].listener || arr[i];
  }
  return ret;
}

function once(emitter, name) {
  return new Promise(function (resolve, reject) {
    function eventListener() {
      if (errorListener !== undefined) {
        emitter.removeListener('error', errorListener);
      }
      resolve([].slice.call(arguments));
    };
    var errorListener;

    // Adding an error listener is not optional because
    // if an error is thrown on an event emitter we cannot
    // guarantee that the actual event we are waiting will
    // be fired. The result could be a silent way to create
    // memory or file descriptor leaks, which is something
    // we should avoid.
    if (name !== 'error') {
      errorListener = function errorListener(err) {
        emitter.removeListener(name, eventListener);
        reject(err);
      };

      emitter.once('error', errorListener);
    }

    emitter.once(name, eventListener);
  });
}


/***/ }),

/***/ "./src/js/events.js":
/*!**************************!*\
  !*** ./src/js/events.js ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! events */ "./node_modules/events/events.js");
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(events__WEBPACK_IMPORTED_MODULE_0__);


const engine = new events__WEBPACK_IMPORTED_MODULE_0__.EventEmitter()

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (engine);

/***/ }),

/***/ "./src/js/graph.js":
/*!*************************!*\
  !*** ./src/js/graph.js ***!
  \*************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "graph": () => (/* binding */ graph)
/* harmony export */ });
/* harmony import */ var _events__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./events */ "./src/js/events.js");


const graph = {
  vertex: {
    keys: new Set(),
    map: {}
  },
  edges: {
    keys: new Set(),
    map: {}
  },

  /**
   * Добавить новую вершину в граф
   * @param {number} x
   * @param {number} y
   * @returns {Symbol}
   */
  addVertex(x, y, label='#', style = 0) {
    let key = Symbol(label);
    this.vertex.keys.add(key);
    this.vertex.map[key] = {
      x: x,
      y: y,
      label: label,
      style: style,
      connected: new Set(),
      inverse: new Set()
    };
    _events__WEBPACK_IMPORTED_MODULE_0__.default.emit('addVertex', key);
    return key;
  },

  /**
   * Добавить новое ребро
   * @param {Symbol} start
   * @param {Symbol} end
   * @returns {Symbol}
   */
  addEdge(start, end, label='10', bend = 0, style = 0, bidirectional = true) {
    let key = Symbol();
    this.edges.keys.add(key);
    this.edges.map[key] = {
      start: start,
      end: end,
      distance: null,
      bend: bend,
      style: style,
      bidirectional: bidirectional
    };
    this.vertex.map[start].connected.add(key);
    this.vertex.map[end].inverse.add(key);
    _events__WEBPACK_IMPORTED_MODULE_0__.default.emit('addEdge', key);
    return key;
  },

  /**
   * Удалить какую-то вершину
   * @param {Symbol} key 
   */
  destroyEdge(key) {
    this.vertex.map[this.edges.map[key].end].inverse.delete(key);
    this.vertex.map[this.edges.map[key].start].connected.delete(key);
    delete this.edges.map[key];
    this.edges.keys.delete(key);
    _events__WEBPACK_IMPORTED_MODULE_0__.default.emit('destroyEdge', key);
  },

  /**
   * Удаляем вершину
   * @param {Symbol} key 
   */
  destroyVertex(key) {
    for (let i of this.vertex.map[key].connected) {
      this.destroyEdge(i);
    }
    for (let i of this.vertex.map[key].inverse) {
      this.destroyEdge(i);
    }
    delete this.vertex.map[key];
    this.vertex.keys.delete(key);
    _events__WEBPACK_IMPORTED_MODULE_0__.default.emit('destroyVertex', key);
  },

  /**
   * Меняем стиль ребра
   * @param {Symbol} key 
   * @param {number} newStyle 
   */
  changeEdgeStyle(key, newStyle) {
    this.edges.map[key].style = newStyle;
    _events__WEBPACK_IMPORTED_MODULE_0__.default.emit('changeEdgeStyle', key);
  },

  /**
   * Меняем стиль вершины
   * @param {Symbol} key 
   * @param {number} newStyle 
   */
  changeVertexStyle(key, newStyle) {
    this.vertex.map[key].style = newStyle;
    _events__WEBPACK_IMPORTED_MODULE_0__.default.emit('changeVertexStyle', key);
  },
  /**
   * Передвигаем вершину
   * @param {Symbol} key 
   * @param {number} newX 
   * @param {number} newY 
   */
  moveVertex(key, newX, newY) {
    this.vertex.map[key].x = newX;
    this.vertex.map[key].y = newY;
    _events__WEBPACK_IMPORTED_MODULE_0__.default.emit('moveVertex', key);
  },

  setLabel(key, newLabel) {
    this.vertex.map[key].label = newLabel;
    _events__WEBPACK_IMPORTED_MODULE_0__.default.emit('setLabel', key);
  }
};

/***/ }),

/***/ "./src/js/render.js":
/*!**************************!*\
  !*** ./src/js/render.js ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "renderEvents": () => (/* binding */ renderEvents),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _graph__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./graph */ "./src/js/graph.js");
/* harmony import */ var _events__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./events */ "./src/js/events.js");
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! events */ "./node_modules/events/events.js");
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(events__WEBPACK_IMPORTED_MODULE_2__);




// На каком расстоянии от вершины ставим перпендикуляр ребру
// с контрольными точками кривой Безье (для гнутых рёбер)
const splitCurve = 0.3;

const nSplitCurve = 1 - splitCurve;

const ns = 'http://www.w3.org/2000/svg';

const renderEvents = new events__WEBPACK_IMPORTED_MODULE_2__.EventEmitter()

/** 
 * SVG рендер
*/
const render = {
  dom: {
    vertex: {},
    edge: {},
    vertexGroup: document.querySelector('.workspace svg #vertex-group'),
    edgeGroup: document.querySelector('.workspace svg #edge-group'),
    svg: document.querySelector('.workspace svg'),
    workspace: document.querySelector('.workspace'),
    selectionCircle: document.querySelector('circle#selection')
  },
  position: {
    x: 0,
    y: 0
  },
  wiewport: {
    width: 100,
    height: 100,
    halfwidth: 50,
    halfheight: 50,
    offsetX: 50,
    offsetY: 50
  },
  style: {
    vertex: [
      { bg: '#fff', fg: '#000', br: '#000' },
      { bg: '#000', fg: '#fff', br: '#000' },
      { bg: '#bbb', fg: '#000', br: '#000' }
    ],
    edge: [
      0,
      2,
      10,
      '5 5 2 5'
    ],
    radius: 20
  },
  selection: {
    /** @type {null | 'vertex' | 'edge'} */
    type: null,
    id: null,
  },
  addVertex(key) {
    console.log('%cRENDER%c Add vertex %cSymbol(' + (key.description ?? '') + ')',
      'color: white; background: #4c9692; padding: 5px 10px; display: inline-block;',
      'display: inline-block; padding: 5px 5px',
      'display: inline-block; padding: 5px 0px; color: #4c9692');
    let vertex = _graph__WEBPACK_IMPORTED_MODULE_0__.graph.vertex.map[key];
    let style = this.style.vertex[vertex.style]

    let group = document.createElementNS(ns, 'g');
    this.dom.vertexGroup.appendChild(group);

    let circ = document.createElementNS(ns, 'circle');
    circ.setAttributeNS(null, 'stroke-width', '2');
    circ.setAttributeNS(null, 'cx', this.wiewport.offsetX + vertex.x);
    circ.setAttributeNS(null, 'cy', this.wiewport.offsetY + vertex.y);
    circ.setAttributeNS(null, 'r', this.style.radius);
    circ.setAttributeNS(null, 'fill', this.style.vertex[vertex.style].bg);
    circ.setAttributeNS(null, 'stroke', this.style.vertex[vertex.style].br);
    group.appendChild(circ);

    let text = document.createElementNS(ns, 'text');
    text.setAttributeNS(null, 'x', this.wiewport.offsetX + vertex.x);
    text.setAttributeNS(null, 'y', this.wiewport.offsetY + vertex.y);
    text.setAttributeNS(null, 'fill', style.fg);
    text.textContent = vertex.label;

    group.appendChild(text);
    this.dom.vertex[key] = {
      group: group,
      circ: circ,
      text: text,
    };
    group.addEventListener('mousedown', (e) => renderEvents.emit('vertex-mouse-down', key, e))
    renderEvents.emit('vertex-added', key, this.dom.vertex[key])
  },
  destroyVertex(key) {
    console.log('%cRENDER%c Destroy vertex %cSymbol(' + (key.description ?? '') + ')',
      'color: white; background: #4c9692; padding: 5px 10px; display: inline-block;',
      'display: inline-block; padding: 5px 5px',
      'display: inline-block; padding: 5px 0px; color: #4c9692');
    if (this.selection.id == key) {
      this.select(null);
    }
    let vertex = this.dom.vertex[key];
    vertex.group.remove();
    vertex.circ.remove();
    vertex.text.remove();
    delete vertex.group;
    delete vertex.circ;
    delete vertex.text;
    delete this.dom.vertex[key];
    renderEvents.emit('vertex-destroyed', key)
  },
  addEdge(key) {
    console.log('%cRENDER%c Add edge %cSymbol(' + (key.description ?? '') + ')',
      'color: white; background: #4c9692; padding: 5px 10px; display: inline-block;',
      'display: inline-block; padding: 5px 5px',
      'display: inline-block; padding: 5px 0px; color: #4c9692');
    let start = _graph__WEBPACK_IMPORTED_MODULE_0__.graph.vertex.map[_graph__WEBPACK_IMPORTED_MODULE_0__.graph.edges.map[key].start];
    let end = _graph__WEBPACK_IMPORTED_MODULE_0__.graph.vertex.map[_graph__WEBPACK_IMPORTED_MODULE_0__.graph.edges.map[key].end];

    let group = document.createElementNS(ns, 'g');
    this.dom.edgeGroup.appendChild(group);

    let clickZone = document.createElementNS(ns, 'path');
    clickZone.setAttributeNS(null, 'fill', 'none');
    clickZone.setAttributeNS(null, 'stroke', '#4c9692');
    clickZone.setAttributeNS(null, 'opacity', '0');
    clickZone.setAttributeNS(null, 'stroke-width', '15');
    group.appendChild(clickZone);

    let visible = document.createElementNS(ns, 'path');
    visible.setAttributeNS(null, 'fill', 'none');
    visible.setAttributeNS(null, 'stroke-width', '2');
    visible.setAttributeNS(null, 'stroke', '#000');
    group.appendChild(visible);

    group.addEventListener('mousedown', (e) => renderEvents.emit('edge-mouse-down', key, e))

    this.dom.edge[key] = {
      group: group,
      visible: visible,
      clickZone: clickZone
    };
    renderEvents.emit('edge-added', key, this.dom.edge[key])
    this.repositionEdge(key);
  },
  destroyEdge(key) {
    console.log('%cRENDER%c Destroy edge %cSymbol(' + (key.description ?? '') + ')',
      'color: white; background: #4c9692; padding: 5px 10px; display: inline-block;',
      'display: inline-block; padding: 5px 5px',
      'display: inline-block; padding: 5px 0px; color: #4c9692');
    if (this.selection.id == key) {
      this.select(null);
    }
    let edge = this.dom.edge[key];
    edge.group.remove();
    edge.visible.remove();
    edge.clickZone.remove();
    delete edge.group;
    delete edge.visible;
    delete edge.clickZone;
    delete this.dom.edge[key];
    renderEvents.emit('edge-destroyed', key)
  },
  repositionEdge(id) {
    let edge = _graph__WEBPACK_IMPORTED_MODULE_0__.graph.edges.map[id];
    let start = _graph__WEBPACK_IMPORTED_MODULE_0__.graph.vertex.map[edge.start];
    let end = _graph__WEBPACK_IMPORTED_MODULE_0__.graph.vertex.map[edge.end];
    if (edge.bend) {
      let v = {
        x: end.x - start.x,
        y: end.y - start.y
      }
      let d = Math.sqrt(v.x * v.x + v.y * v.y);
      let path =
        `M ${start.x + this.wiewport.offsetX} ` +
        `${start.y + this.wiewport.offsetY} ` +
        `C ${start.x + splitCurve * v.x + v.y / d * edge.bend + this.wiewport.offsetX} ` +
        `${start.y + splitCurve * v.y - v.x / d * edge.bend + this.wiewport.offsetY} ` +
        `${start.x + nSplitCurve * v.x + v.y / d * edge.bend + this.wiewport.offsetX} ` +
        `${start.y + nSplitCurve * v.y - v.x / d * edge.bend + this.wiewport.offsetY} ` +
        `${end.x + this.wiewport.offsetX} ` +
        `${end.y + this.wiewport.offsetY}`;
      this.dom.edge[id].visible.setAttributeNS(null, 'd', path);
      this.dom.edge[id].clickZone.setAttributeNS(null, 'd', path);
    } else {
      let path =
        `M ${start.x + this.wiewport.offsetX} ${start.y + this.wiewport.offsetY} ` +
        `L ${end.x + this.wiewport.offsetX} ${end.y + this.wiewport.offsetY}`
      this.dom.edge[id].visible.setAttributeNS(null, 'd', path);
      this.dom.edge[id].clickZone.setAttributeNS(null, 'd', path);
    }
  },
  move(x, y) {
    this.position.x = x;
    this.position.y = y;
    this.dom.edgeGroup.setAttributeNS(null, 'transform', 'translate(' + this.position.x + ', ' + this.position.y + ')');
    this.dom.vertexGroup.setAttributeNS(null, 'transform', 'translate(' + this.position.x + ', ' + this.position.y + ')');
    this.dom.selectionCircle.setAttributeNS(null, 'transform', 'translate(' + this.position.x + ', ' + this.position.y + ')');
    document.body.style.backgroundPositionX = x % 100 - 5 + 'px';
    document.body.style.backgroundPositionY = y % 100 - 5 + 'px';
  },
  resize() {
    let rect = this.dom.workspace.getBoundingClientRect();
    this.dom.svg.setAttribute('width', rect.width);
    this.dom.svg.setAttribute('height', rect.height);
    this.wiewport.halfwidth = Math.round((this.wiewport.width = rect.width) / 2);
    this.wiewport.halfheight = Math.round((this.wiewport.height = rect.height) / 2);
    this.wiewport.offsetX = 0;
    this.wiewport.offsetY = 0;
    //this.reposition();
  },
  /**
   * 
   * @param {null|'vertex'|'edge'} type 
   * @param {Symbol} key 
   */
  select(type, key) {
    if (this.selection.type == 'vertex') {
      this.dom.selectionCircle.setAttributeNS(null, 'opacity', 0);
    } else if (this.selection.type == 'edge') {
      this.dom.edge[this.selection.id].clickZone.setAttributeNS(null, 'opacity', 0);
    }

    this.selection.type = type;
    this.selection.id = key;

    if (type == 'vertex') {
      this.dom.selectionCircle.setAttributeNS(null, 'opacity', 0.5);
      this.dom.selectionCircle.setAttributeNS(null, 'cx', _graph__WEBPACK_IMPORTED_MODULE_0__.graph.vertex.map[key].x);
      this.dom.selectionCircle.setAttributeNS(null, 'cy', _graph__WEBPACK_IMPORTED_MODULE_0__.graph.vertex.map[key].y);
    } else if (type == 'edge') {
      this.dom.edge[key].clickZone.setAttributeNS(null, 'opacity', 0.5);
    }
    renderEvents.emit('selection-changed', type, key)
  },
  moveSelection() {
    if (this.selection.type == 'vertex') {
      // this.dom.selectionCircle.setAttributeNS(null, 'opacity', 1);
      this.dom.selectionCircle.setAttributeNS(null, 'cx', _graph__WEBPACK_IMPORTED_MODULE_0__.graph.vertex.map[this.selection.id].x);
      this.dom.selectionCircle.setAttributeNS(null, 'cy', _graph__WEBPACK_IMPORTED_MODULE_0__.graph.vertex.map[this.selection.id].y);
    } else if (this.selection.type == 'edge') {
      // this.dom.edge[this.selection.id].clickZone.setAttributeNS(null, 'opacity', 1);
    } else {
      // this.dom.selectionCircle.setAttributeNS(null, 'opacity', 0);
    }
  },
  moveVertex(key) {
    let vertex = _graph__WEBPACK_IMPORTED_MODULE_0__.graph.vertex.map[key];
    this.dom.vertex[key].circ.setAttributeNS(null, 'cx', this.wiewport.offsetX + vertex.x);
    this.dom.vertex[key].circ.setAttributeNS(null, 'cy', this.wiewport.offsetY + vertex.y);
    this.dom.vertex[key].text.setAttributeNS(null, 'x', this.wiewport.offsetX + vertex.x);
    this.dom.vertex[key].text.setAttributeNS(null, 'y', this.wiewport.offsetY + vertex.y);
    for (let i of vertex.connected)
      this.repositionEdge(i);
    for (let i of vertex.inverse)
      this.repositionEdge(i);
    if (this.selection.type == 'vertex' && this.selection.id == key)
      this.moveSelection();
  },
  restyleVertex(key) {
    let vertex = _graph__WEBPACK_IMPORTED_MODULE_0__.graph.vertex.map[key];
    let dom = this.dom.vertex[key];
    dom.circ.setAttributeNS(null, 'fill', this.style.vertex[vertex.style].bg);
    dom.circ.setAttributeNS(null, 'stroke', this.style.vertex[vertex.style].br);
    dom.text.setAttributeNS(null, 'fill', this.style.vertex[vertex.style].fg);
  },
  setLabel(key) {
    this.dom.vertex[key].text.textContent = _graph__WEBPACK_IMPORTED_MODULE_0__.graph.vertex.map[key].label;
  }
}

render.dom.svg.addEventListener('mousemove', e => renderEvents.emit('mouse-move', e))
render.dom.svg.addEventListener('mousedown', e => renderEvents.emit('mouse-down', e))
document.addEventListener('mouseup', e => renderEvents.emit('mouse-up', e))

const load = {
  edges: {
    parse(str) {
      return str.split('\n')
        .map(i => i.split(' ').filter(i => i))
        .map(i => (i.length == 1 ? { type: 'vertex', label: i[0] } :
          i.length == 2 || i.length == 3 ? { type: 'edge', start: i[0], end: i[1], w: i[2] } :
            i.length == 0 ? { type: 'nothing' } :
              { type: 'something' }))
    }
  }
};

window.addEventListener('resize', _ => render.resize());
render.resize();
render.move(render.wiewport.halfwidth, render.wiewport.halfheight);

_events__WEBPACK_IMPORTED_MODULE_1__.default.on('addVertex', d => render.addVertex(d))
_events__WEBPACK_IMPORTED_MODULE_1__.default.on('addEdge', d => render.addEdge(d))
_events__WEBPACK_IMPORTED_MODULE_1__.default.on('destroyEdge', d => render.destroyEdge(d))
_events__WEBPACK_IMPORTED_MODULE_1__.default.on('destroyVertex', d => render.destroyVertex(d))
// engine.on('changeEdgeStyle', render.changeEdgeStyle)
_events__WEBPACK_IMPORTED_MODULE_1__.default.on('changeVertexStyle', d => render.restyleVertex(d))
_events__WEBPACK_IMPORTED_MODULE_1__.default.on('moveVertex', d => render.moveVertex(d))
_events__WEBPACK_IMPORTED_MODULE_1__.default.on('setLabel', d => render.setLabel(d))

console.log(render)

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (render);

/***/ }),

/***/ "./src/js/tools/all.js":
/*!*****************************!*\
  !*** ./src/js/tools/all.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _pan__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pan */ "./src/js/tools/pan.js");
/* harmony import */ var _select__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./select */ "./src/js/tools/select.js");
/* harmony import */ var _basic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./basic */ "./src/js/tools/basic.js");
/* harmony import */ var _create__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./create */ "./src/js/tools/create.js");
/* harmony import */ var _rename__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./rename */ "./src/js/tools/rename.js");
/* harmony import */ var _force__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./force */ "./src/js/tools/force.js");





// Not actually tool


/***/ }),

/***/ "./src/js/tools/basic.js":
/*!*******************************!*\
  !*** ./src/js/tools/basic.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _graph__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../graph */ "./src/js/graph.js");
/* harmony import */ var _render__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../render */ "./src/js/render.js");



document.addEventListener('keydown', function(e) {
  // Ignore events in input fields
  if (['input', 'textarea'].includes(e.target.tagName.toLowerCase()))
    return;
  if (e.key == 'Delete') {
    if (_render__WEBPACK_IMPORTED_MODULE_1__.default.selection.type == 'vertex')
      _graph__WEBPACK_IMPORTED_MODULE_0__.graph.destroyVertex(_render__WEBPACK_IMPORTED_MODULE_1__.default.selection.id);
    else if (_render__WEBPACK_IMPORTED_MODULE_1__.default.selection.type == 'edge')
      _graph__WEBPACK_IMPORTED_MODULE_0__.graph.destroyEdge(_render__WEBPACK_IMPORTED_MODULE_1__.default.selection.id);
  }
});

/***/ }),

/***/ "./src/js/tools/create.js":
/*!********************************!*\
  !*** ./src/js/tools/create.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _graph__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../graph */ "./src/js/graph.js");
/* harmony import */ var _render__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../render */ "./src/js/render.js");
/* harmony import */ var _fn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./fn */ "./src/js/tools/fn.js");
/* harmony import */ var _state__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./state */ "./src/js/tools/state.js");





let pathPreview = _render__WEBPACK_IMPORTED_MODULE_1__.default.dom.svg.querySelector('#edge-preview');
let start = { x: 0, y: 0 }

_render__WEBPACK_IMPORTED_MODULE_1__.default.dom.svg.addEventListener('click', e => {
  if (_state__WEBPACK_IMPORTED_MODULE_3__.activeTool.name == 'add-vertex')
    _graph__WEBPACK_IMPORTED_MODULE_0__.graph.addVertex(e.clientX - _render__WEBPACK_IMPORTED_MODULE_1__.default.position.x, e.clientY - _render__WEBPACK_IMPORTED_MODULE_1__.default.position.y);
});

_render__WEBPACK_IMPORTED_MODULE_1__.renderEvents.on('mouse-move', e => {
  if (_state__WEBPACK_IMPORTED_MODULE_3__.activeTool.name == 'add-edge') {
    if (_state__WEBPACK_IMPORTED_MODULE_3__.activeTool.opts.start)
      pathPreview.setAttributeNS(null, 'd', `m ${start.x} ${start.y} l ${e.clientX - start.x} ${e.clientY - start.y} z`)
  }
});

_render__WEBPACK_IMPORTED_MODULE_1__.renderEvents.on('vertex-mouse-down', (key, e) => {
  if (_state__WEBPACK_IMPORTED_MODULE_3__.activeTool.name == 'add-edge') {
    if (_state__WEBPACK_IMPORTED_MODULE_3__.activeTool.opts.start) {
      _graph__WEBPACK_IMPORTED_MODULE_0__.graph.addEdge(_state__WEBPACK_IMPORTED_MODULE_3__.activeTool.opts.start, key);
      _state__WEBPACK_IMPORTED_MODULE_3__.activeTool.opts.start = null;
      pathPreview.style.opacity = 0;
    } else {
      _state__WEBPACK_IMPORTED_MODULE_3__.activeTool.opts.start = key;
      start.x = _graph__WEBPACK_IMPORTED_MODULE_0__.graph.vertex.map[key].x + _render__WEBPACK_IMPORTED_MODULE_1__.default.position.x;
      start.y = _graph__WEBPACK_IMPORTED_MODULE_0__.graph.vertex.map[key].y + _render__WEBPACK_IMPORTED_MODULE_1__.default.position.y;
      pathPreview.setAttributeNS(null, 'd', `m ${start.x} ${start.y} l ${e.clientX - start.x} ${e.clientY - start.y} z`)
      pathPreview.style.opacity = 1;
    }
  }
})

;(0,_fn__WEBPACK_IMPORTED_MODULE_2__.deftool)("#add-vertex", 'add-vertex')
;(0,_fn__WEBPACK_IMPORTED_MODULE_2__.deftool)("#add-edge", 'add-edge', { allowSelection: false }, () => {
  _render__WEBPACK_IMPORTED_MODULE_1__.default.select(null);
}, () => {
  pathPreview.style.opacity = 0;
});

/***/ }),

/***/ "./src/js/tools/fn.js":
/*!****************************!*\
  !*** ./src/js/tools/fn.js ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "deftool": () => (/* binding */ deftool),
/* harmony export */   "makeToggle": () => (/* binding */ makeToggle),
/* harmony export */   "makePopup": () => (/* binding */ makePopup)
/* harmony export */ });
/* harmony import */ var _state__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./state */ "./src/js/tools/state.js");


let selected = null;
let selectedExit = () => {};

let renameInput = document.querySelector('#label');

function deepClone(obj) {
  const clObj = {};
  for(const i in obj) {
    if (obj[i] instanceof Object) {
      clObj[i] = deepClone(obj[i]);
      continue;
    }
    clObj[i] = obj[i];
  }
  return clObj;
}

function deftool(sel, name, opts = {}, onActive = () => {}, onExit = () => {}) {
  let elem = document.querySelector(sel);
  opts.allowSelection = opts.allowSelection ?? true;

  let fn = () => {
    if (selected) {
      selected.classList.remove('active');
      selectedExit();
    }
    selected = elem;
    elem.classList.add('active');
    _state__WEBPACK_IMPORTED_MODULE_0__.activeTool.name = name;
    _state__WEBPACK_IMPORTED_MODULE_0__.activeTool.opts = deepClone(opts);;
    renameInput.setAttribute('style', "top: -100px; left: -100px;")
    onActive();
    selectedExit = onExit;
  };

  if (elem.classList.contains('active'))
    fn();
  elem.addEventListener('click', fn);
}

function makeToggle(sel, callback, dflt=false) {
  let elem = document.querySelector(sel);
  let on = dflt;
  elem.addEventListener('click', (e) => {
    on = !on;
    elem.classList.toggle('active', on);
    callback(on);
  });
}

let currentPopup = null;

function makePopup(selector, buttonSelector, when) {
  let button = document.querySelector(buttonSelector);
  let popup = document.querySelector(selector);
  popup.style.display = 'none';
  button.addEventListener('click', e => {
    if (when && !when())
      return;
    e.shouldNotClosePopup = true;
    if (currentPopup)
      currentPopup.style.display = 'none';
    popup.style.display = null;
    currentPopup = popup;
  });

  popup.addEventListener('click', e => {
    e.shouldNotClosePopup = true;
  });
}

/***/ }),

/***/ "./src/js/tools/force.js":
/*!*******************************!*\
  !*** ./src/js/tools/force.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _graph__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../graph */ "./src/js/graph.js");
/* harmony import */ var _fn__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fn */ "./src/js/tools/fn.js");
/* harmony import */ var _render__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../render */ "./src/js/render.js");




const preferredEdgeLength = 100;
const preferredDistance = 100;
const forceFactor = 0.05;
const returnSpeed = 1;

let isPhysicsActive = false;

(0,_fn__WEBPACK_IMPORTED_MODULE_1__.makeToggle)('#force', (st) => {
  isPhysicsActive = st;
  if (st)
    requestAnimationFrame(step);
});

function step() {
  if (!isPhysicsActive)
    return;
  
  let resulting = {};
  
  for (let i of _graph__WEBPACK_IMPORTED_MODULE_0__.graph.vertex.keys) {
    // Do not apply on selected
    if (_render__WEBPACK_IMPORTED_MODULE_2__.default.selection.type == 'vertex' && _render__WEBPACK_IMPORTED_MODULE_2__.default.selection.id == i)
      continue;
    let a = _graph__WEBPACK_IMPORTED_MODULE_0__.graph.vertex.map[i];
    let x = a.x, y = a.y;
    for (let j of _graph__WEBPACK_IMPORTED_MODULE_0__.graph.vertex.keys) {
      let b = _graph__WEBPACK_IMPORTED_MODULE_0__.graph.vertex.map[j];
      // console.log(i, a, j, b);
      let dx = a.x - b.x,
          dy = a.y - b.y;
      if (dx == 0) dx = Math.random() * 2 - 1;
      if (dy == 0) dy = Math.random() * 2 - 1;
      let d = Math.sqrt(dx*dx + dy*dy);
      if (i == j)
        continue;
      let found = false;
      for (let q of a.connected)
        if (found || _graph__WEBPACK_IMPORTED_MODULE_0__.graph.edges.map[q].end == j) {
          found = true;
          break;
        }
      for (let q of a.inverse)
        if (found || _graph__WEBPACK_IMPORTED_MODULE_0__.graph.edges.map[q].start == j) {
          found = true;
          break;
        }
      if (found) {
        // console.log(i, 'connected to', j)
        x -= dx / d * (d-preferredEdgeLength) * forceFactor;
        y -= dy / d * (d-preferredEdgeLength) * forceFactor;
      } else if (d < preferredDistance) {
        x -= dx / d * (d-preferredDistance) * forceFactor;
        y -= dy / d * (d-preferredDistance) * forceFactor;
      }
    }
    resulting[i] = { x: x, y: y };
  }

  for (let i of _graph__WEBPACK_IMPORTED_MODULE_0__.graph.vertex.keys) {
    // console.log('Move', i, 'to', resulting[i]);
    if (resulting[i])
      _graph__WEBPACK_IMPORTED_MODULE_0__.graph.moveVertex(i, resulting[i].x, resulting[i].y)
  }

  requestAnimationFrame(step);
}

/***/ }),

/***/ "./src/js/tools/pan.js":
/*!*****************************!*\
  !*** ./src/js/tools/pan.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _state__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./state */ "./src/js/tools/state.js");
/* harmony import */ var _render__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../render */ "./src/js/render.js");
/* harmony import */ var _fn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./fn */ "./src/js/tools/fn.js");




let isActive = false;
let x = 0;
let y = 0;

_render__WEBPACK_IMPORTED_MODULE_1__.renderEvents.on('mouse-down', e => {
  if (e.ctrlKey || _state__WEBPACK_IMPORTED_MODULE_0__.activeTool.name == 'move') {
    isActive = true;
    x = _render__WEBPACK_IMPORTED_MODULE_1__.default.position.x - e.clientX;
    y = _render__WEBPACK_IMPORTED_MODULE_1__.default.position.y - e.clientY;
  }
});

_render__WEBPACK_IMPORTED_MODULE_1__.renderEvents.on('mouse-move', e => {
  if (e.ctrlKey || _state__WEBPACK_IMPORTED_MODULE_0__.activeTool.name == 'move') {
    if (!isActive) return;
    _render__WEBPACK_IMPORTED_MODULE_1__.default.move(
      x + e.clientX,
      y + e.clientY
    );
  }
});

_render__WEBPACK_IMPORTED_MODULE_1__.renderEvents.on('mouse-up', e => {
  isActive = false;
})

;(0,_fn__WEBPACK_IMPORTED_MODULE_2__.deftool)("#move", 'move', { allowSelection: false })

/***/ }),

/***/ "./src/js/tools/rename.js":
/*!********************************!*\
  !*** ./src/js/tools/rename.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _render__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../render */ "./src/js/render.js");
/* harmony import */ var _graph__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../graph */ "./src/js/graph.js");
/* harmony import */ var _fn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./fn */ "./src/js/tools/fn.js");
/* harmony import */ var _state__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./state */ "./src/js/tools/state.js");





let renameInput = document.querySelector('#label');
let activeVertex = null;

_render__WEBPACK_IMPORTED_MODULE_0__.renderEvents.on('vertex-mouse-down', (vx, evt) => {
  if (_state__WEBPACK_IMPORTED_MODULE_3__.activeTool.name == 'rename') {
    renameInput.style.left = _graph__WEBPACK_IMPORTED_MODULE_1__.graph.vertex.map[vx].x + _render__WEBPACK_IMPORTED_MODULE_0__.default.position.x + 'px';
    renameInput.style.top = _graph__WEBPACK_IMPORTED_MODULE_1__.graph.vertex.map[vx].y + _render__WEBPACK_IMPORTED_MODULE_0__.default.position.y + 'px';
    renameInput.value = _graph__WEBPACK_IMPORTED_MODULE_1__.graph.vertex.map[vx].label;
    // Bug in firefox?
    setTimeout(() => {
      renameInput.focus()
      renameInput.select()
    }, 1);
    activeVertex = vx;
  }
});

renameInput.addEventListener('input', () => {
  if (activeVertex)
    _graph__WEBPACK_IMPORTED_MODULE_1__.graph.setLabel(activeVertex, renameInput.value);
})

;(0,_fn__WEBPACK_IMPORTED_MODULE_2__.deftool)('#rename', 'rename');

/***/ }),

/***/ "./src/js/tools/select.js":
/*!********************************!*\
  !*** ./src/js/tools/select.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _state__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./state */ "./src/js/tools/state.js");
/* harmony import */ var _render__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../render */ "./src/js/render.js");
/* harmony import */ var _graph__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../graph */ "./src/js/graph.js");
/* harmony import */ var _fn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./fn */ "./src/js/tools/fn.js");





let drag = {
  active: false,
  x: 0,
  y: 0,
  snap: false
}

_render__WEBPACK_IMPORTED_MODULE_1__.renderEvents.on('mouse-move', e => {
  //console.log('mouse-move')
  if (drag.active && _state__WEBPACK_IMPORTED_MODULE_0__.activeTool.name == 'select') {
    if (!drag.active) return;
    let x = drag.x + e.clientX,
        y = drag.y + e.clientY
    _graph__WEBPACK_IMPORTED_MODULE_2__.graph.moveVertex(
      _render__WEBPACK_IMPORTED_MODULE_1__.default.selection.id,
      drag.snap ? Math.round(x / 10) * 10 : x,
      drag.snap ? Math.round(y / 10) * 10 : y
    );
  }
});

_render__WEBPACK_IMPORTED_MODULE_1__.renderEvents.on('mouse-up', e => {
  //console.log('mouse-up')
  drag.active = false;
});

_render__WEBPACK_IMPORTED_MODULE_1__.renderEvents.on('vertex-mouse-down', (key, e) => {
  //console.log('vertex-mouse-down')
  drag.active = true;
  drag.x = _graph__WEBPACK_IMPORTED_MODULE_2__.graph.vertex.map[key].x - e.clientX;
  drag.y = _graph__WEBPACK_IMPORTED_MODULE_2__.graph.vertex.map[key].y - e.clientY;
})

;(0,_fn__WEBPACK_IMPORTED_MODULE_3__.makeToggle)('#snap-grid', (st) => drag.snap = st)
;(0,_fn__WEBPACK_IMPORTED_MODULE_3__.deftool)("#select", 'select')

/***/ }),

/***/ "./src/js/tools/state.js":
/*!*******************************!*\
  !*** ./src/js/tools/state.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "activeTool": () => (/* binding */ activeTool)
/* harmony export */ });
/* harmony import */ var _render__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../render */ "./src/js/render.js");


let activeTool = {
  name: null,
  opts: {}
}

_render__WEBPACK_IMPORTED_MODULE_0__.renderEvents.on('vertex-mouse-down', (key) => {
  if (activeTool.opts.allowSelection)
    _render__WEBPACK_IMPORTED_MODULE_0__.default.select('vertex', key);
})

_render__WEBPACK_IMPORTED_MODULE_0__.renderEvents.on('edge-mouse-down', (key) => {
  if (activeTool.opts.allowSelection)
    _render__WEBPACK_IMPORTED_MODULE_0__.default.select('edge', key);
})

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		if(__webpack_module_cache__[moduleId]) {
/******/ 			return __webpack_module_cache__[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _js_graph__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./js/graph */ "./src/js/graph.js");
/* harmony import */ var _js_render__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./js/render */ "./src/js/render.js");
/* harmony import */ var _js_tools_all__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./js/tools/all */ "./src/js/tools/all.js");




let v1 = _js_graph__WEBPACK_IMPORTED_MODULE_0__.graph.addVertex(0, 100, "a");
let v2 = _js_graph__WEBPACK_IMPORTED_MODULE_0__.graph.addVertex(100, 0, "b");
_js_graph__WEBPACK_IMPORTED_MODULE_0__.graph.addEdge(v1, v2);

let v3 = _js_graph__WEBPACK_IMPORTED_MODULE_0__.graph.addVertex(200, 200, "1");
let v4 = _js_graph__WEBPACK_IMPORTED_MODULE_0__.graph.addVertex(100, 300, "2");
let v5 = _js_graph__WEBPACK_IMPORTED_MODULE_0__.graph.addVertex(200, 400, "3");
let v6 = _js_graph__WEBPACK_IMPORTED_MODULE_0__.graph.addVertex(300, 300, "4");
_js_graph__WEBPACK_IMPORTED_MODULE_0__.graph.addEdge(v3, v4, '', -50);
_js_graph__WEBPACK_IMPORTED_MODULE_0__.graph.addEdge(v4, v5, '', -50);
_js_graph__WEBPACK_IMPORTED_MODULE_0__.graph.addEdge(v5, v6, '', -50);
_js_graph__WEBPACK_IMPORTED_MODULE_0__.graph.addEdge(v6, v3, '', -50);

// load.list(' 1   2  \n2 3\n3 4\nthis that\na b 10\n123\n\n123 456 789 1011')
})();

/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9ncmFmZi8uL25vZGVfbW9kdWxlcy9ldmVudHMvZXZlbnRzLmpzIiwid2VicGFjazovL2dyYWZmLy4vc3JjL2pzL2V2ZW50cy5qcyIsIndlYnBhY2s6Ly9ncmFmZi8uL3NyYy9qcy9ncmFwaC5qcyIsIndlYnBhY2s6Ly9ncmFmZi8uL3NyYy9qcy9yZW5kZXIuanMiLCJ3ZWJwYWNrOi8vZ3JhZmYvLi9zcmMvanMvdG9vbHMvYWxsLmpzIiwid2VicGFjazovL2dyYWZmLy4vc3JjL2pzL3Rvb2xzL2Jhc2ljLmpzIiwid2VicGFjazovL2dyYWZmLy4vc3JjL2pzL3Rvb2xzL2NyZWF0ZS5qcyIsIndlYnBhY2s6Ly9ncmFmZi8uL3NyYy9qcy90b29scy9mbi5qcyIsIndlYnBhY2s6Ly9ncmFmZi8uL3NyYy9qcy90b29scy9mb3JjZS5qcyIsIndlYnBhY2s6Ly9ncmFmZi8uL3NyYy9qcy90b29scy9wYW4uanMiLCJ3ZWJwYWNrOi8vZ3JhZmYvLi9zcmMvanMvdG9vbHMvcmVuYW1lLmpzIiwid2VicGFjazovL2dyYWZmLy4vc3JjL2pzL3Rvb2xzL3NlbGVjdC5qcyIsIndlYnBhY2s6Ly9ncmFmZi8uL3NyYy9qcy90b29scy9zdGF0ZS5qcyIsIndlYnBhY2s6Ly9ncmFmZi93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly9ncmFmZi93ZWJwYWNrL3J1bnRpbWUvY29tcGF0IGdldCBkZWZhdWx0IGV4cG9ydCIsIndlYnBhY2s6Ly9ncmFmZi93ZWJwYWNrL3J1bnRpbWUvZGVmaW5lIHByb3BlcnR5IGdldHRlcnMiLCJ3ZWJwYWNrOi8vZ3JhZmYvd2VicGFjay9ydW50aW1lL2hhc093blByb3BlcnR5IHNob3J0aGFuZCIsIndlYnBhY2s6Ly9ncmFmZi93ZWJwYWNrL3J1bnRpbWUvbWFrZSBuYW1lc3BhY2Ugb2JqZWN0Iiwid2VicGFjazovL2dyYWZmLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVhOztBQUViO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjs7QUFFbkI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUIsc0JBQXNCO0FBQ3ZDOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLG1CQUFtQixTQUFTO0FBQzVCO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7O0FBRUEsaUNBQWlDLFFBQVE7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixpQkFBaUI7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsaUJBQWlCLE9BQU87QUFDeEI7QUFDQTtBQUNBOztBQUVBO0FBQ0EsUUFBUSx5QkFBeUI7QUFDakM7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUIsZ0JBQWdCO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxHQUFHO0FBQ0g7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM2RxQzs7QUFFckMsbUJBQW1CLGdEQUFZOztBQUUvQixpRUFBZSxNQUFNLEU7Ozs7Ozs7Ozs7Ozs7OztBQ0pROztBQUV0QjtBQUNQO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxPQUFPO0FBQ3BCLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLGlEQUFXO0FBQ2Y7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxPQUFPO0FBQ3BCLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxpREFBVztBQUNmO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksaURBQVc7QUFDZixHQUFHOztBQUVIO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLGlEQUFXO0FBQ2YsR0FBRzs7QUFFSDtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsT0FBTztBQUNwQjtBQUNBO0FBQ0E7QUFDQSxJQUFJLGlEQUFXO0FBQ2YsR0FBRzs7QUFFSDtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsT0FBTztBQUNwQjtBQUNBO0FBQ0E7QUFDQSxJQUFJLGlEQUFXO0FBQ2YsR0FBRztBQUNIO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsT0FBTztBQUNwQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksaURBQVc7QUFDZixHQUFHOztBQUVIO0FBQ0E7QUFDQSxJQUFJLGlEQUFXO0FBQ2Y7QUFDQSxFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdkgrQjtBQUNGO0FBQ1E7O0FBRXJDO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFTyx5QkFBeUIsZ0RBQVk7O0FBRTVDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2QsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxPQUFPLHFDQUFxQztBQUM1QyxPQUFPLHFDQUFxQztBQUM1QyxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLGVBQWUseUJBQXlCO0FBQ3hDO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLG9CQUFvQixxQkFBcUIsbUJBQW1CLHVCQUF1QjtBQUNuRiw2QkFBNkI7QUFDN0IsNkJBQTZCLGtCQUFrQjtBQUMvQyxpQkFBaUIsb0RBQWdCO0FBQ2pDOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLG9CQUFvQixxQkFBcUIsbUJBQW1CLHVCQUF1QjtBQUNuRiw2QkFBNkI7QUFDN0IsNkJBQTZCLGtCQUFrQjtBQUMvQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLG9CQUFvQixxQkFBcUIsbUJBQW1CLHVCQUF1QjtBQUNuRiw2QkFBNkI7QUFDN0IsNkJBQTZCLGtCQUFrQjtBQUMvQyxnQkFBZ0Isb0RBQWdCLENBQUMsbURBQWU7QUFDaEQsY0FBYyxvREFBZ0IsQ0FBQyxtREFBZTs7QUFFOUM7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0Esb0JBQW9CLHFCQUFxQixtQkFBbUIsdUJBQXVCO0FBQ25GLDZCQUE2QjtBQUM3Qiw2QkFBNkIsa0JBQWtCO0FBQy9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLGVBQWUsbURBQWU7QUFDOUIsZ0JBQWdCLG9EQUFnQjtBQUNoQyxjQUFjLG9EQUFnQjtBQUM5QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsZ0NBQWdDO0FBQzdDLFdBQVcsZ0NBQWdDO0FBQzNDLGFBQWEseUVBQXlFO0FBQ3RGLFdBQVcseUVBQXlFO0FBQ3BGLFdBQVcsMEVBQTBFO0FBQ3JGLFdBQVcsMEVBQTBFO0FBQ3JGLFdBQVcsOEJBQThCO0FBQ3pDLFdBQVcsOEJBQThCO0FBQ3pDO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxhQUFhLGdDQUFnQyxHQUFHLGdDQUFnQztBQUNoRixhQUFhLDhCQUE4QixHQUFHLDhCQUE4QjtBQUM1RTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLGFBQWEscUJBQXFCO0FBQ2xDLGFBQWEsT0FBTztBQUNwQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLDBEQUEwRCxvREFBZ0I7QUFDMUUsMERBQTBELG9EQUFnQjtBQUMxRSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLDBEQUEwRCxvREFBZ0I7QUFDMUUsMERBQTBELG9EQUFnQjtBQUMxRSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLGlCQUFpQixvREFBZ0I7QUFDakM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxpQkFBaUIsb0RBQWdCO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsNENBQTRDLG9EQUFnQjtBQUM1RDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0NBQW9DLDhCQUE4QjtBQUNsRSw0Q0FBNEMsZ0RBQWdEO0FBQzVGLDZCQUE2QixrQkFBa0I7QUFDL0MsZUFBZSxvQkFBb0I7QUFDbkM7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSwrQ0FBUztBQUNULCtDQUFTO0FBQ1QsK0NBQVM7QUFDVCwrQ0FBUztBQUNUO0FBQ0EsK0NBQVM7QUFDVCwrQ0FBUztBQUNULCtDQUFTOztBQUVUOztBQUVBLGlFQUFlLE1BQU0sRTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMvU1A7QUFDRztBQUNEO0FBQ0M7QUFDQTtBQUNqQjs7Ozs7Ozs7Ozs7Ozs7QUNMaUM7QUFDRjs7QUFFL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVEsMkRBQXFCO0FBQzdCLE1BQU0sdURBQW1CLENBQUMseURBQW1CO0FBQzdDLGFBQWEsMkRBQXFCO0FBQ2xDLE1BQU0scURBQWlCLENBQUMseURBQW1CO0FBQzNDO0FBQ0EsQ0FBQyxFOzs7Ozs7Ozs7Ozs7Ozs7QUNiK0I7QUFDZ0I7QUFDakI7QUFDSzs7QUFFcEMsa0JBQWtCLGtFQUE0QjtBQUM5QyxhQUFhOztBQUViLHFFQUErQjtBQUMvQixNQUFNLG1EQUFlO0FBQ3JCLElBQUksbURBQWUsYUFBYSx1REFBaUIsY0FBYyx1REFBaUI7QUFDaEYsQ0FBQzs7QUFFRCxvREFBZTtBQUNmLE1BQU0sbURBQWU7QUFDckIsUUFBUSx5REFBcUI7QUFDN0IsaURBQWlELFFBQVEsR0FBRyxRQUFRLEtBQUssb0JBQW9CLEdBQUcsb0JBQW9CO0FBQ3BIO0FBQ0EsQ0FBQzs7QUFFRCxvREFBZTtBQUNmLE1BQU0sbURBQWU7QUFDckIsUUFBUSx5REFBcUI7QUFDN0IsTUFBTSxpREFBYSxDQUFDLHlEQUFxQjtBQUN6QyxNQUFNLHlEQUFxQjtBQUMzQjtBQUNBLEtBQUs7QUFDTCxNQUFNLHlEQUFxQjtBQUMzQixnQkFBZ0Isb0RBQWdCLFVBQVUsdURBQWlCO0FBQzNELGdCQUFnQixvREFBZ0IsVUFBVSx1REFBaUI7QUFDM0QsaURBQWlELFFBQVEsR0FBRyxRQUFRLEtBQUssb0JBQW9CLEdBQUcsb0JBQW9CO0FBQ3BIO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsNkNBQU87QUFDUCw2Q0FBTywyQkFBMkIsd0JBQXdCO0FBQzFELEVBQUUsbURBQWE7QUFDZixDQUFDO0FBQ0Q7QUFDQSxDQUFDLEU7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDekNtQzs7QUFFcEM7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVPLHFDQUFxQyxxQkFBcUIsbUJBQW1CO0FBQ3BGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLG1EQUFlO0FBQ25CLElBQUksbURBQWU7QUFDbkIsbURBQW1ELGNBQWM7QUFDakU7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBOztBQUVPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBLEdBQUc7QUFDSCxDOzs7Ozs7Ozs7Ozs7OztBQ3ZFZ0M7QUFDQztBQUNIOztBQUU5QjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSwrQ0FBVTtBQUNWO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBOztBQUVBOztBQUVBLGdCQUFnQixxREFBaUI7QUFDakM7QUFDQSxRQUFRLDJEQUFxQixnQkFBZ0IseURBQW1CO0FBQ2hFO0FBQ0EsWUFBWSxvREFBZ0I7QUFDNUI7QUFDQSxrQkFBa0IscURBQWlCO0FBQ25DLGNBQWMsb0RBQWdCO0FBQzlCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLG1EQUFlO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLG1EQUFlO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CO0FBQ3BCOztBQUVBLGdCQUFnQixxREFBaUI7QUFDakM7QUFDQTtBQUNBLE1BQU0sb0RBQWdCO0FBQ3RCOztBQUVBO0FBQ0EsQzs7Ozs7Ozs7Ozs7Ozs7QUNyRW9DO0FBQ1k7QUFDbEI7O0FBRTlCO0FBQ0E7QUFDQTs7QUFFQSxvREFBZTtBQUNmLG1CQUFtQixtREFBZTtBQUNsQztBQUNBLFFBQVEsdURBQWlCO0FBQ3pCLFFBQVEsdURBQWlCO0FBQ3pCO0FBQ0EsQ0FBQzs7QUFFRCxvREFBZTtBQUNmLG1CQUFtQixtREFBZTtBQUNsQztBQUNBLElBQUksaURBQVc7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsb0RBQWU7QUFDZjtBQUNBLENBQUM7O0FBRUQsNkNBQU8sbUJBQW1CLHdCQUF3QixDOzs7Ozs7Ozs7Ozs7Ozs7QUM5QkY7QUFDaEI7QUFDRDtBQUNLOztBQUVwQztBQUNBOztBQUVBLG9EQUFlO0FBQ2YsTUFBTSxtREFBZTtBQUNyQiw2QkFBNkIsb0RBQWdCLFNBQVMsdURBQWlCO0FBQ3ZFLDRCQUE0QixvREFBZ0IsU0FBUyx1REFBaUI7QUFDdEUsd0JBQXdCLG9EQUFnQjtBQUN4QztBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0EsSUFBSSxrREFBYztBQUNsQixDQUFDOztBQUVELDZDQUFPLHNCOzs7Ozs7Ozs7Ozs7Ozs7QUMzQjZCO0FBQ1k7QUFDaEI7QUFDVTs7QUFFMUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLG9EQUFlO0FBQ2Y7QUFDQSxxQkFBcUIsbURBQWU7QUFDcEM7QUFDQTtBQUNBO0FBQ0EsSUFBSSxvREFBZ0I7QUFDcEIsTUFBTSx5REFBbUI7QUFDekI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELG9EQUFlO0FBQ2Y7QUFDQTtBQUNBLENBQUM7O0FBRUQsb0RBQWU7QUFDZjtBQUNBO0FBQ0EsV0FBVyxvREFBZ0I7QUFDM0IsV0FBVyxvREFBZ0I7QUFDM0IsQ0FBQzs7QUFFRCxnREFBVTtBQUNWLDZDQUFPLHFCOzs7Ozs7Ozs7Ozs7Ozs7QUN2Q3lDOztBQUV6QztBQUNQO0FBQ0E7QUFDQTs7QUFFQSxvREFBZTtBQUNmO0FBQ0EsSUFBSSxtREFBYTtBQUNqQixDQUFDOztBQUVELG9EQUFlO0FBQ2Y7QUFDQSxJQUFJLG1EQUFhO0FBQ2pCLENBQUMsQzs7Ozs7O1VDZkQ7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7O1VBRUE7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7Ozs7O1dDckJBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQSxnQ0FBZ0MsWUFBWTtXQUM1QztXQUNBLEU7Ozs7O1dDUEE7V0FDQTtXQUNBO1dBQ0E7V0FDQSx3Q0FBd0MseUNBQXlDO1dBQ2pGO1dBQ0E7V0FDQSxFOzs7OztXQ1BBLHdGOzs7OztXQ0FBO1dBQ0E7V0FDQTtXQUNBLHNEQUFzRCxrQkFBa0I7V0FDeEU7V0FDQSwrQ0FBK0MsY0FBYztXQUM3RCxFOzs7Ozs7Ozs7Ozs7OztBQ05rQztBQUNkO0FBQ0c7O0FBRXZCLFNBQVMsc0RBQWU7QUFDeEIsU0FBUyxzREFBZTtBQUN4QixvREFBYTs7QUFFYixTQUFTLHNEQUFlO0FBQ3hCLFNBQVMsc0RBQWU7QUFDeEIsU0FBUyxzREFBZTtBQUN4QixTQUFTLHNEQUFlO0FBQ3hCLG9EQUFhO0FBQ2Isb0RBQWE7QUFDYixvREFBYTtBQUNiLG9EQUFhOztBQUViLDhFIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBDb3B5cmlnaHQgSm95ZW50LCBJbmMuIGFuZCBvdGhlciBOb2RlIGNvbnRyaWJ1dG9ycy5cbi8vXG4vLyBQZXJtaXNzaW9uIGlzIGhlcmVieSBncmFudGVkLCBmcmVlIG9mIGNoYXJnZSwgdG8gYW55IHBlcnNvbiBvYnRhaW5pbmcgYVxuLy8gY29weSBvZiB0aGlzIHNvZnR3YXJlIGFuZCBhc3NvY2lhdGVkIGRvY3VtZW50YXRpb24gZmlsZXMgKHRoZVxuLy8gXCJTb2Z0d2FyZVwiKSwgdG8gZGVhbCBpbiB0aGUgU29mdHdhcmUgd2l0aG91dCByZXN0cmljdGlvbiwgaW5jbHVkaW5nXG4vLyB3aXRob3V0IGxpbWl0YXRpb24gdGhlIHJpZ2h0cyB0byB1c2UsIGNvcHksIG1vZGlmeSwgbWVyZ2UsIHB1Ymxpc2gsXG4vLyBkaXN0cmlidXRlLCBzdWJsaWNlbnNlLCBhbmQvb3Igc2VsbCBjb3BpZXMgb2YgdGhlIFNvZnR3YXJlLCBhbmQgdG8gcGVybWl0XG4vLyBwZXJzb25zIHRvIHdob20gdGhlIFNvZnR3YXJlIGlzIGZ1cm5pc2hlZCB0byBkbyBzbywgc3ViamVjdCB0byB0aGVcbi8vIGZvbGxvd2luZyBjb25kaXRpb25zOlxuLy9cbi8vIFRoZSBhYm92ZSBjb3B5cmlnaHQgbm90aWNlIGFuZCB0aGlzIHBlcm1pc3Npb24gbm90aWNlIHNoYWxsIGJlIGluY2x1ZGVkXG4vLyBpbiBhbGwgY29waWVzIG9yIHN1YnN0YW50aWFsIHBvcnRpb25zIG9mIHRoZSBTb2Z0d2FyZS5cbi8vXG4vLyBUSEUgU09GVFdBUkUgSVMgUFJPVklERUQgXCJBUyBJU1wiLCBXSVRIT1VUIFdBUlJBTlRZIE9GIEFOWSBLSU5ELCBFWFBSRVNTXG4vLyBPUiBJTVBMSUVELCBJTkNMVURJTkcgQlVUIE5PVCBMSU1JVEVEIFRPIFRIRSBXQVJSQU5USUVTIE9GXG4vLyBNRVJDSEFOVEFCSUxJVFksIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFIEFORCBOT05JTkZSSU5HRU1FTlQuIElOXG4vLyBOTyBFVkVOVCBTSEFMTCBUSEUgQVVUSE9SUyBPUiBDT1BZUklHSFQgSE9MREVSUyBCRSBMSUFCTEUgRk9SIEFOWSBDTEFJTSxcbi8vIERBTUFHRVMgT1IgT1RIRVIgTElBQklMSVRZLCBXSEVUSEVSIElOIEFOIEFDVElPTiBPRiBDT05UUkFDVCwgVE9SVCBPUlxuLy8gT1RIRVJXSVNFLCBBUklTSU5HIEZST00sIE9VVCBPRiBPUiBJTiBDT05ORUNUSU9OIFdJVEggVEhFIFNPRlRXQVJFIE9SIFRIRVxuLy8gVVNFIE9SIE9USEVSIERFQUxJTkdTIElOIFRIRSBTT0ZUV0FSRS5cblxuJ3VzZSBzdHJpY3QnO1xuXG52YXIgUiA9IHR5cGVvZiBSZWZsZWN0ID09PSAnb2JqZWN0JyA/IFJlZmxlY3QgOiBudWxsXG52YXIgUmVmbGVjdEFwcGx5ID0gUiAmJiB0eXBlb2YgUi5hcHBseSA9PT0gJ2Z1bmN0aW9uJ1xuICA/IFIuYXBwbHlcbiAgOiBmdW5jdGlvbiBSZWZsZWN0QXBwbHkodGFyZ2V0LCByZWNlaXZlciwgYXJncykge1xuICAgIHJldHVybiBGdW5jdGlvbi5wcm90b3R5cGUuYXBwbHkuY2FsbCh0YXJnZXQsIHJlY2VpdmVyLCBhcmdzKTtcbiAgfVxuXG52YXIgUmVmbGVjdE93bktleXNcbmlmIChSICYmIHR5cGVvZiBSLm93bktleXMgPT09ICdmdW5jdGlvbicpIHtcbiAgUmVmbGVjdE93bktleXMgPSBSLm93bktleXNcbn0gZWxzZSBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scykge1xuICBSZWZsZWN0T3duS2V5cyA9IGZ1bmN0aW9uIFJlZmxlY3RPd25LZXlzKHRhcmdldCkge1xuICAgIHJldHVybiBPYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lcyh0YXJnZXQpXG4gICAgICAuY29uY2F0KE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHModGFyZ2V0KSk7XG4gIH07XG59IGVsc2Uge1xuICBSZWZsZWN0T3duS2V5cyA9IGZ1bmN0aW9uIFJlZmxlY3RPd25LZXlzKHRhcmdldCkge1xuICAgIHJldHVybiBPYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lcyh0YXJnZXQpO1xuICB9O1xufVxuXG5mdW5jdGlvbiBQcm9jZXNzRW1pdFdhcm5pbmcod2FybmluZykge1xuICBpZiAoY29uc29sZSAmJiBjb25zb2xlLndhcm4pIGNvbnNvbGUud2Fybih3YXJuaW5nKTtcbn1cblxudmFyIE51bWJlcklzTmFOID0gTnVtYmVyLmlzTmFOIHx8IGZ1bmN0aW9uIE51bWJlcklzTmFOKHZhbHVlKSB7XG4gIHJldHVybiB2YWx1ZSAhPT0gdmFsdWU7XG59XG5cbmZ1bmN0aW9uIEV2ZW50RW1pdHRlcigpIHtcbiAgRXZlbnRFbWl0dGVyLmluaXQuY2FsbCh0aGlzKTtcbn1cbm1vZHVsZS5leHBvcnRzID0gRXZlbnRFbWl0dGVyO1xubW9kdWxlLmV4cG9ydHMub25jZSA9IG9uY2U7XG5cbi8vIEJhY2t3YXJkcy1jb21wYXQgd2l0aCBub2RlIDAuMTAueFxuRXZlbnRFbWl0dGVyLkV2ZW50RW1pdHRlciA9IEV2ZW50RW1pdHRlcjtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5fZXZlbnRzID0gdW5kZWZpbmVkO1xuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5fZXZlbnRzQ291bnQgPSAwO1xuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5fbWF4TGlzdGVuZXJzID0gdW5kZWZpbmVkO1xuXG4vLyBCeSBkZWZhdWx0IEV2ZW50RW1pdHRlcnMgd2lsbCBwcmludCBhIHdhcm5pbmcgaWYgbW9yZSB0aGFuIDEwIGxpc3RlbmVycyBhcmVcbi8vIGFkZGVkIHRvIGl0LiBUaGlzIGlzIGEgdXNlZnVsIGRlZmF1bHQgd2hpY2ggaGVscHMgZmluZGluZyBtZW1vcnkgbGVha3MuXG52YXIgZGVmYXVsdE1heExpc3RlbmVycyA9IDEwO1xuXG5mdW5jdGlvbiBjaGVja0xpc3RlbmVyKGxpc3RlbmVyKSB7XG4gIGlmICh0eXBlb2YgbGlzdGVuZXIgIT09ICdmdW5jdGlvbicpIHtcbiAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdUaGUgXCJsaXN0ZW5lclwiIGFyZ3VtZW50IG11c3QgYmUgb2YgdHlwZSBGdW5jdGlvbi4gUmVjZWl2ZWQgdHlwZSAnICsgdHlwZW9mIGxpc3RlbmVyKTtcbiAgfVxufVxuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoRXZlbnRFbWl0dGVyLCAnZGVmYXVsdE1heExpc3RlbmVycycsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4gZGVmYXVsdE1heExpc3RlbmVycztcbiAgfSxcbiAgc2V0OiBmdW5jdGlvbihhcmcpIHtcbiAgICBpZiAodHlwZW9mIGFyZyAhPT0gJ251bWJlcicgfHwgYXJnIDwgMCB8fCBOdW1iZXJJc05hTihhcmcpKSB7XG4gICAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcignVGhlIHZhbHVlIG9mIFwiZGVmYXVsdE1heExpc3RlbmVyc1wiIGlzIG91dCBvZiByYW5nZS4gSXQgbXVzdCBiZSBhIG5vbi1uZWdhdGl2ZSBudW1iZXIuIFJlY2VpdmVkICcgKyBhcmcgKyAnLicpO1xuICAgIH1cbiAgICBkZWZhdWx0TWF4TGlzdGVuZXJzID0gYXJnO1xuICB9XG59KTtcblxuRXZlbnRFbWl0dGVyLmluaXQgPSBmdW5jdGlvbigpIHtcblxuICBpZiAodGhpcy5fZXZlbnRzID09PSB1bmRlZmluZWQgfHxcbiAgICAgIHRoaXMuX2V2ZW50cyA9PT0gT2JqZWN0LmdldFByb3RvdHlwZU9mKHRoaXMpLl9ldmVudHMpIHtcbiAgICB0aGlzLl9ldmVudHMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuICAgIHRoaXMuX2V2ZW50c0NvdW50ID0gMDtcbiAgfVxuXG4gIHRoaXMuX21heExpc3RlbmVycyA9IHRoaXMuX21heExpc3RlbmVycyB8fCB1bmRlZmluZWQ7XG59O1xuXG4vLyBPYnZpb3VzbHkgbm90IGFsbCBFbWl0dGVycyBzaG91bGQgYmUgbGltaXRlZCB0byAxMC4gVGhpcyBmdW5jdGlvbiBhbGxvd3Ncbi8vIHRoYXQgdG8gYmUgaW5jcmVhc2VkLiBTZXQgdG8gemVybyBmb3IgdW5saW1pdGVkLlxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5zZXRNYXhMaXN0ZW5lcnMgPSBmdW5jdGlvbiBzZXRNYXhMaXN0ZW5lcnMobikge1xuICBpZiAodHlwZW9mIG4gIT09ICdudW1iZXInIHx8IG4gPCAwIHx8IE51bWJlcklzTmFOKG4pKSB7XG4gICAgdGhyb3cgbmV3IFJhbmdlRXJyb3IoJ1RoZSB2YWx1ZSBvZiBcIm5cIiBpcyBvdXQgb2YgcmFuZ2UuIEl0IG11c3QgYmUgYSBub24tbmVnYXRpdmUgbnVtYmVyLiBSZWNlaXZlZCAnICsgbiArICcuJyk7XG4gIH1cbiAgdGhpcy5fbWF4TGlzdGVuZXJzID0gbjtcbiAgcmV0dXJuIHRoaXM7XG59O1xuXG5mdW5jdGlvbiBfZ2V0TWF4TGlzdGVuZXJzKHRoYXQpIHtcbiAgaWYgKHRoYXQuX21heExpc3RlbmVycyA9PT0gdW5kZWZpbmVkKVxuICAgIHJldHVybiBFdmVudEVtaXR0ZXIuZGVmYXVsdE1heExpc3RlbmVycztcbiAgcmV0dXJuIHRoYXQuX21heExpc3RlbmVycztcbn1cblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5nZXRNYXhMaXN0ZW5lcnMgPSBmdW5jdGlvbiBnZXRNYXhMaXN0ZW5lcnMoKSB7XG4gIHJldHVybiBfZ2V0TWF4TGlzdGVuZXJzKHRoaXMpO1xufTtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5lbWl0ID0gZnVuY3Rpb24gZW1pdCh0eXBlKSB7XG4gIHZhciBhcmdzID0gW107XG4gIGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSBhcmdzLnB1c2goYXJndW1lbnRzW2ldKTtcbiAgdmFyIGRvRXJyb3IgPSAodHlwZSA9PT0gJ2Vycm9yJyk7XG5cbiAgdmFyIGV2ZW50cyA9IHRoaXMuX2V2ZW50cztcbiAgaWYgKGV2ZW50cyAhPT0gdW5kZWZpbmVkKVxuICAgIGRvRXJyb3IgPSAoZG9FcnJvciAmJiBldmVudHMuZXJyb3IgPT09IHVuZGVmaW5lZCk7XG4gIGVsc2UgaWYgKCFkb0Vycm9yKVxuICAgIHJldHVybiBmYWxzZTtcblxuICAvLyBJZiB0aGVyZSBpcyBubyAnZXJyb3InIGV2ZW50IGxpc3RlbmVyIHRoZW4gdGhyb3cuXG4gIGlmIChkb0Vycm9yKSB7XG4gICAgdmFyIGVyO1xuICAgIGlmIChhcmdzLmxlbmd0aCA+IDApXG4gICAgICBlciA9IGFyZ3NbMF07XG4gICAgaWYgKGVyIGluc3RhbmNlb2YgRXJyb3IpIHtcbiAgICAgIC8vIE5vdGU6IFRoZSBjb21tZW50cyBvbiB0aGUgYHRocm93YCBsaW5lcyBhcmUgaW50ZW50aW9uYWwsIHRoZXkgc2hvd1xuICAgICAgLy8gdXAgaW4gTm9kZSdzIG91dHB1dCBpZiB0aGlzIHJlc3VsdHMgaW4gYW4gdW5oYW5kbGVkIGV4Y2VwdGlvbi5cbiAgICAgIHRocm93IGVyOyAvLyBVbmhhbmRsZWQgJ2Vycm9yJyBldmVudFxuICAgIH1cbiAgICAvLyBBdCBsZWFzdCBnaXZlIHNvbWUga2luZCBvZiBjb250ZXh0IHRvIHRoZSB1c2VyXG4gICAgdmFyIGVyciA9IG5ldyBFcnJvcignVW5oYW5kbGVkIGVycm9yLicgKyAoZXIgPyAnICgnICsgZXIubWVzc2FnZSArICcpJyA6ICcnKSk7XG4gICAgZXJyLmNvbnRleHQgPSBlcjtcbiAgICB0aHJvdyBlcnI7IC8vIFVuaGFuZGxlZCAnZXJyb3InIGV2ZW50XG4gIH1cblxuICB2YXIgaGFuZGxlciA9IGV2ZW50c1t0eXBlXTtcblxuICBpZiAoaGFuZGxlciA9PT0gdW5kZWZpbmVkKVxuICAgIHJldHVybiBmYWxzZTtcblxuICBpZiAodHlwZW9mIGhhbmRsZXIgPT09ICdmdW5jdGlvbicpIHtcbiAgICBSZWZsZWN0QXBwbHkoaGFuZGxlciwgdGhpcywgYXJncyk7XG4gIH0gZWxzZSB7XG4gICAgdmFyIGxlbiA9IGhhbmRsZXIubGVuZ3RoO1xuICAgIHZhciBsaXN0ZW5lcnMgPSBhcnJheUNsb25lKGhhbmRsZXIsIGxlbik7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47ICsraSlcbiAgICAgIFJlZmxlY3RBcHBseShsaXN0ZW5lcnNbaV0sIHRoaXMsIGFyZ3MpO1xuICB9XG5cbiAgcmV0dXJuIHRydWU7XG59O1xuXG5mdW5jdGlvbiBfYWRkTGlzdGVuZXIodGFyZ2V0LCB0eXBlLCBsaXN0ZW5lciwgcHJlcGVuZCkge1xuICB2YXIgbTtcbiAgdmFyIGV2ZW50cztcbiAgdmFyIGV4aXN0aW5nO1xuXG4gIGNoZWNrTGlzdGVuZXIobGlzdGVuZXIpO1xuXG4gIGV2ZW50cyA9IHRhcmdldC5fZXZlbnRzO1xuICBpZiAoZXZlbnRzID09PSB1bmRlZmluZWQpIHtcbiAgICBldmVudHMgPSB0YXJnZXQuX2V2ZW50cyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gICAgdGFyZ2V0Ll9ldmVudHNDb3VudCA9IDA7XG4gIH0gZWxzZSB7XG4gICAgLy8gVG8gYXZvaWQgcmVjdXJzaW9uIGluIHRoZSBjYXNlIHRoYXQgdHlwZSA9PT0gXCJuZXdMaXN0ZW5lclwiISBCZWZvcmVcbiAgICAvLyBhZGRpbmcgaXQgdG8gdGhlIGxpc3RlbmVycywgZmlyc3QgZW1pdCBcIm5ld0xpc3RlbmVyXCIuXG4gICAgaWYgKGV2ZW50cy5uZXdMaXN0ZW5lciAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICB0YXJnZXQuZW1pdCgnbmV3TGlzdGVuZXInLCB0eXBlLFxuICAgICAgICAgICAgICAgICAgbGlzdGVuZXIubGlzdGVuZXIgPyBsaXN0ZW5lci5saXN0ZW5lciA6IGxpc3RlbmVyKTtcblxuICAgICAgLy8gUmUtYXNzaWduIGBldmVudHNgIGJlY2F1c2UgYSBuZXdMaXN0ZW5lciBoYW5kbGVyIGNvdWxkIGhhdmUgY2F1c2VkIHRoZVxuICAgICAgLy8gdGhpcy5fZXZlbnRzIHRvIGJlIGFzc2lnbmVkIHRvIGEgbmV3IG9iamVjdFxuICAgICAgZXZlbnRzID0gdGFyZ2V0Ll9ldmVudHM7XG4gICAgfVxuICAgIGV4aXN0aW5nID0gZXZlbnRzW3R5cGVdO1xuICB9XG5cbiAgaWYgKGV4aXN0aW5nID09PSB1bmRlZmluZWQpIHtcbiAgICAvLyBPcHRpbWl6ZSB0aGUgY2FzZSBvZiBvbmUgbGlzdGVuZXIuIERvbid0IG5lZWQgdGhlIGV4dHJhIGFycmF5IG9iamVjdC5cbiAgICBleGlzdGluZyA9IGV2ZW50c1t0eXBlXSA9IGxpc3RlbmVyO1xuICAgICsrdGFyZ2V0Ll9ldmVudHNDb3VudDtcbiAgfSBlbHNlIHtcbiAgICBpZiAodHlwZW9mIGV4aXN0aW5nID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAvLyBBZGRpbmcgdGhlIHNlY29uZCBlbGVtZW50LCBuZWVkIHRvIGNoYW5nZSB0byBhcnJheS5cbiAgICAgIGV4aXN0aW5nID0gZXZlbnRzW3R5cGVdID1cbiAgICAgICAgcHJlcGVuZCA/IFtsaXN0ZW5lciwgZXhpc3RpbmddIDogW2V4aXN0aW5nLCBsaXN0ZW5lcl07XG4gICAgICAvLyBJZiB3ZSd2ZSBhbHJlYWR5IGdvdCBhbiBhcnJheSwganVzdCBhcHBlbmQuXG4gICAgfSBlbHNlIGlmIChwcmVwZW5kKSB7XG4gICAgICBleGlzdGluZy51bnNoaWZ0KGxpc3RlbmVyKTtcbiAgICB9IGVsc2Uge1xuICAgICAgZXhpc3RpbmcucHVzaChsaXN0ZW5lcik7XG4gICAgfVxuXG4gICAgLy8gQ2hlY2sgZm9yIGxpc3RlbmVyIGxlYWtcbiAgICBtID0gX2dldE1heExpc3RlbmVycyh0YXJnZXQpO1xuICAgIGlmIChtID4gMCAmJiBleGlzdGluZy5sZW5ndGggPiBtICYmICFleGlzdGluZy53YXJuZWQpIHtcbiAgICAgIGV4aXN0aW5nLndhcm5lZCA9IHRydWU7XG4gICAgICAvLyBObyBlcnJvciBjb2RlIGZvciB0aGlzIHNpbmNlIGl0IGlzIGEgV2FybmluZ1xuICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXJlc3RyaWN0ZWQtc3ludGF4XG4gICAgICB2YXIgdyA9IG5ldyBFcnJvcignUG9zc2libGUgRXZlbnRFbWl0dGVyIG1lbW9yeSBsZWFrIGRldGVjdGVkLiAnICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgZXhpc3RpbmcubGVuZ3RoICsgJyAnICsgU3RyaW5nKHR5cGUpICsgJyBsaXN0ZW5lcnMgJyArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICdhZGRlZC4gVXNlIGVtaXR0ZXIuc2V0TWF4TGlzdGVuZXJzKCkgdG8gJyArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICdpbmNyZWFzZSBsaW1pdCcpO1xuICAgICAgdy5uYW1lID0gJ01heExpc3RlbmVyc0V4Y2VlZGVkV2FybmluZyc7XG4gICAgICB3LmVtaXR0ZXIgPSB0YXJnZXQ7XG4gICAgICB3LnR5cGUgPSB0eXBlO1xuICAgICAgdy5jb3VudCA9IGV4aXN0aW5nLmxlbmd0aDtcbiAgICAgIFByb2Nlc3NFbWl0V2FybmluZyh3KTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gdGFyZ2V0O1xufVxuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmFkZExpc3RlbmVyID0gZnVuY3Rpb24gYWRkTGlzdGVuZXIodHlwZSwgbGlzdGVuZXIpIHtcbiAgcmV0dXJuIF9hZGRMaXN0ZW5lcih0aGlzLCB0eXBlLCBsaXN0ZW5lciwgZmFsc2UpO1xufTtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5vbiA9IEV2ZW50RW1pdHRlci5wcm90b3R5cGUuYWRkTGlzdGVuZXI7XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUucHJlcGVuZExpc3RlbmVyID1cbiAgICBmdW5jdGlvbiBwcmVwZW5kTGlzdGVuZXIodHlwZSwgbGlzdGVuZXIpIHtcbiAgICAgIHJldHVybiBfYWRkTGlzdGVuZXIodGhpcywgdHlwZSwgbGlzdGVuZXIsIHRydWUpO1xuICAgIH07XG5cbmZ1bmN0aW9uIG9uY2VXcmFwcGVyKCkge1xuICBpZiAoIXRoaXMuZmlyZWQpIHtcbiAgICB0aGlzLnRhcmdldC5yZW1vdmVMaXN0ZW5lcih0aGlzLnR5cGUsIHRoaXMud3JhcEZuKTtcbiAgICB0aGlzLmZpcmVkID0gdHJ1ZTtcbiAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gMClcbiAgICAgIHJldHVybiB0aGlzLmxpc3RlbmVyLmNhbGwodGhpcy50YXJnZXQpO1xuICAgIHJldHVybiB0aGlzLmxpc3RlbmVyLmFwcGx5KHRoaXMudGFyZ2V0LCBhcmd1bWVudHMpO1xuICB9XG59XG5cbmZ1bmN0aW9uIF9vbmNlV3JhcCh0YXJnZXQsIHR5cGUsIGxpc3RlbmVyKSB7XG4gIHZhciBzdGF0ZSA9IHsgZmlyZWQ6IGZhbHNlLCB3cmFwRm46IHVuZGVmaW5lZCwgdGFyZ2V0OiB0YXJnZXQsIHR5cGU6IHR5cGUsIGxpc3RlbmVyOiBsaXN0ZW5lciB9O1xuICB2YXIgd3JhcHBlZCA9IG9uY2VXcmFwcGVyLmJpbmQoc3RhdGUpO1xuICB3cmFwcGVkLmxpc3RlbmVyID0gbGlzdGVuZXI7XG4gIHN0YXRlLndyYXBGbiA9IHdyYXBwZWQ7XG4gIHJldHVybiB3cmFwcGVkO1xufVxuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLm9uY2UgPSBmdW5jdGlvbiBvbmNlKHR5cGUsIGxpc3RlbmVyKSB7XG4gIGNoZWNrTGlzdGVuZXIobGlzdGVuZXIpO1xuICB0aGlzLm9uKHR5cGUsIF9vbmNlV3JhcCh0aGlzLCB0eXBlLCBsaXN0ZW5lcikpO1xuICByZXR1cm4gdGhpcztcbn07XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUucHJlcGVuZE9uY2VMaXN0ZW5lciA9XG4gICAgZnVuY3Rpb24gcHJlcGVuZE9uY2VMaXN0ZW5lcih0eXBlLCBsaXN0ZW5lcikge1xuICAgICAgY2hlY2tMaXN0ZW5lcihsaXN0ZW5lcik7XG4gICAgICB0aGlzLnByZXBlbmRMaXN0ZW5lcih0eXBlLCBfb25jZVdyYXAodGhpcywgdHlwZSwgbGlzdGVuZXIpKTtcbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG5cbi8vIEVtaXRzIGEgJ3JlbW92ZUxpc3RlbmVyJyBldmVudCBpZiBhbmQgb25seSBpZiB0aGUgbGlzdGVuZXIgd2FzIHJlbW92ZWQuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLnJlbW92ZUxpc3RlbmVyID1cbiAgICBmdW5jdGlvbiByZW1vdmVMaXN0ZW5lcih0eXBlLCBsaXN0ZW5lcikge1xuICAgICAgdmFyIGxpc3QsIGV2ZW50cywgcG9zaXRpb24sIGksIG9yaWdpbmFsTGlzdGVuZXI7XG5cbiAgICAgIGNoZWNrTGlzdGVuZXIobGlzdGVuZXIpO1xuXG4gICAgICBldmVudHMgPSB0aGlzLl9ldmVudHM7XG4gICAgICBpZiAoZXZlbnRzID09PSB1bmRlZmluZWQpXG4gICAgICAgIHJldHVybiB0aGlzO1xuXG4gICAgICBsaXN0ID0gZXZlbnRzW3R5cGVdO1xuICAgICAgaWYgKGxpc3QgPT09IHVuZGVmaW5lZClcbiAgICAgICAgcmV0dXJuIHRoaXM7XG5cbiAgICAgIGlmIChsaXN0ID09PSBsaXN0ZW5lciB8fCBsaXN0Lmxpc3RlbmVyID09PSBsaXN0ZW5lcikge1xuICAgICAgICBpZiAoLS10aGlzLl9ldmVudHNDb3VudCA9PT0gMClcbiAgICAgICAgICB0aGlzLl9ldmVudHMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuICAgICAgICBlbHNlIHtcbiAgICAgICAgICBkZWxldGUgZXZlbnRzW3R5cGVdO1xuICAgICAgICAgIGlmIChldmVudHMucmVtb3ZlTGlzdGVuZXIpXG4gICAgICAgICAgICB0aGlzLmVtaXQoJ3JlbW92ZUxpc3RlbmVyJywgdHlwZSwgbGlzdC5saXN0ZW5lciB8fCBsaXN0ZW5lcik7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGxpc3QgIT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgcG9zaXRpb24gPSAtMTtcblxuICAgICAgICBmb3IgKGkgPSBsaXN0Lmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XG4gICAgICAgICAgaWYgKGxpc3RbaV0gPT09IGxpc3RlbmVyIHx8IGxpc3RbaV0ubGlzdGVuZXIgPT09IGxpc3RlbmVyKSB7XG4gICAgICAgICAgICBvcmlnaW5hbExpc3RlbmVyID0gbGlzdFtpXS5saXN0ZW5lcjtcbiAgICAgICAgICAgIHBvc2l0aW9uID0gaTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChwb3NpdGlvbiA8IDApXG4gICAgICAgICAgcmV0dXJuIHRoaXM7XG5cbiAgICAgICAgaWYgKHBvc2l0aW9uID09PSAwKVxuICAgICAgICAgIGxpc3Quc2hpZnQoKTtcbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgc3BsaWNlT25lKGxpc3QsIHBvc2l0aW9uKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChsaXN0Lmxlbmd0aCA9PT0gMSlcbiAgICAgICAgICBldmVudHNbdHlwZV0gPSBsaXN0WzBdO1xuXG4gICAgICAgIGlmIChldmVudHMucmVtb3ZlTGlzdGVuZXIgIT09IHVuZGVmaW5lZClcbiAgICAgICAgICB0aGlzLmVtaXQoJ3JlbW92ZUxpc3RlbmVyJywgdHlwZSwgb3JpZ2luYWxMaXN0ZW5lciB8fCBsaXN0ZW5lcik7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUub2ZmID0gRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5yZW1vdmVMaXN0ZW5lcjtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5yZW1vdmVBbGxMaXN0ZW5lcnMgPVxuICAgIGZ1bmN0aW9uIHJlbW92ZUFsbExpc3RlbmVycyh0eXBlKSB7XG4gICAgICB2YXIgbGlzdGVuZXJzLCBldmVudHMsIGk7XG5cbiAgICAgIGV2ZW50cyA9IHRoaXMuX2V2ZW50cztcbiAgICAgIGlmIChldmVudHMgPT09IHVuZGVmaW5lZClcbiAgICAgICAgcmV0dXJuIHRoaXM7XG5cbiAgICAgIC8vIG5vdCBsaXN0ZW5pbmcgZm9yIHJlbW92ZUxpc3RlbmVyLCBubyBuZWVkIHRvIGVtaXRcbiAgICAgIGlmIChldmVudHMucmVtb3ZlTGlzdGVuZXIgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgIHRoaXMuX2V2ZW50cyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gICAgICAgICAgdGhpcy5fZXZlbnRzQ291bnQgPSAwO1xuICAgICAgICB9IGVsc2UgaWYgKGV2ZW50c1t0eXBlXSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgaWYgKC0tdGhpcy5fZXZlbnRzQ291bnQgPT09IDApXG4gICAgICAgICAgICB0aGlzLl9ldmVudHMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgIGRlbGV0ZSBldmVudHNbdHlwZV07XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgICB9XG5cbiAgICAgIC8vIGVtaXQgcmVtb3ZlTGlzdGVuZXIgZm9yIGFsbCBsaXN0ZW5lcnMgb24gYWxsIGV2ZW50c1xuICAgICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgdmFyIGtleXMgPSBPYmplY3Qua2V5cyhldmVudHMpO1xuICAgICAgICB2YXIga2V5O1xuICAgICAgICBmb3IgKGkgPSAwOyBpIDwga2V5cy5sZW5ndGg7ICsraSkge1xuICAgICAgICAgIGtleSA9IGtleXNbaV07XG4gICAgICAgICAgaWYgKGtleSA9PT0gJ3JlbW92ZUxpc3RlbmVyJykgY29udGludWU7XG4gICAgICAgICAgdGhpcy5yZW1vdmVBbGxMaXN0ZW5lcnMoa2V5KTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnJlbW92ZUFsbExpc3RlbmVycygncmVtb3ZlTGlzdGVuZXInKTtcbiAgICAgICAgdGhpcy5fZXZlbnRzID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiAgICAgICAgdGhpcy5fZXZlbnRzQ291bnQgPSAwO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICAgIH1cblxuICAgICAgbGlzdGVuZXJzID0gZXZlbnRzW3R5cGVdO1xuXG4gICAgICBpZiAodHlwZW9mIGxpc3RlbmVycyA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICB0aGlzLnJlbW92ZUxpc3RlbmVyKHR5cGUsIGxpc3RlbmVycyk7XG4gICAgICB9IGVsc2UgaWYgKGxpc3RlbmVycyAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIC8vIExJRk8gb3JkZXJcbiAgICAgICAgZm9yIChpID0gbGlzdGVuZXJzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XG4gICAgICAgICAgdGhpcy5yZW1vdmVMaXN0ZW5lcih0eXBlLCBsaXN0ZW5lcnNbaV0pO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG5cbmZ1bmN0aW9uIF9saXN0ZW5lcnModGFyZ2V0LCB0eXBlLCB1bndyYXApIHtcbiAgdmFyIGV2ZW50cyA9IHRhcmdldC5fZXZlbnRzO1xuXG4gIGlmIChldmVudHMgPT09IHVuZGVmaW5lZClcbiAgICByZXR1cm4gW107XG5cbiAgdmFyIGV2bGlzdGVuZXIgPSBldmVudHNbdHlwZV07XG4gIGlmIChldmxpc3RlbmVyID09PSB1bmRlZmluZWQpXG4gICAgcmV0dXJuIFtdO1xuXG4gIGlmICh0eXBlb2YgZXZsaXN0ZW5lciA9PT0gJ2Z1bmN0aW9uJylcbiAgICByZXR1cm4gdW53cmFwID8gW2V2bGlzdGVuZXIubGlzdGVuZXIgfHwgZXZsaXN0ZW5lcl0gOiBbZXZsaXN0ZW5lcl07XG5cbiAgcmV0dXJuIHVud3JhcCA/XG4gICAgdW53cmFwTGlzdGVuZXJzKGV2bGlzdGVuZXIpIDogYXJyYXlDbG9uZShldmxpc3RlbmVyLCBldmxpc3RlbmVyLmxlbmd0aCk7XG59XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUubGlzdGVuZXJzID0gZnVuY3Rpb24gbGlzdGVuZXJzKHR5cGUpIHtcbiAgcmV0dXJuIF9saXN0ZW5lcnModGhpcywgdHlwZSwgdHJ1ZSk7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLnJhd0xpc3RlbmVycyA9IGZ1bmN0aW9uIHJhd0xpc3RlbmVycyh0eXBlKSB7XG4gIHJldHVybiBfbGlzdGVuZXJzKHRoaXMsIHR5cGUsIGZhbHNlKTtcbn07XG5cbkV2ZW50RW1pdHRlci5saXN0ZW5lckNvdW50ID0gZnVuY3Rpb24oZW1pdHRlciwgdHlwZSkge1xuICBpZiAodHlwZW9mIGVtaXR0ZXIubGlzdGVuZXJDb3VudCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIHJldHVybiBlbWl0dGVyLmxpc3RlbmVyQ291bnQodHlwZSk7XG4gIH0gZWxzZSB7XG4gICAgcmV0dXJuIGxpc3RlbmVyQ291bnQuY2FsbChlbWl0dGVyLCB0eXBlKTtcbiAgfVxufTtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5saXN0ZW5lckNvdW50ID0gbGlzdGVuZXJDb3VudDtcbmZ1bmN0aW9uIGxpc3RlbmVyQ291bnQodHlwZSkge1xuICB2YXIgZXZlbnRzID0gdGhpcy5fZXZlbnRzO1xuXG4gIGlmIChldmVudHMgIT09IHVuZGVmaW5lZCkge1xuICAgIHZhciBldmxpc3RlbmVyID0gZXZlbnRzW3R5cGVdO1xuXG4gICAgaWYgKHR5cGVvZiBldmxpc3RlbmVyID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICByZXR1cm4gMTtcbiAgICB9IGVsc2UgaWYgKGV2bGlzdGVuZXIgIT09IHVuZGVmaW5lZCkge1xuICAgICAgcmV0dXJuIGV2bGlzdGVuZXIubGVuZ3RoO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiAwO1xufVxuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmV2ZW50TmFtZXMgPSBmdW5jdGlvbiBldmVudE5hbWVzKCkge1xuICByZXR1cm4gdGhpcy5fZXZlbnRzQ291bnQgPiAwID8gUmVmbGVjdE93bktleXModGhpcy5fZXZlbnRzKSA6IFtdO1xufTtcblxuZnVuY3Rpb24gYXJyYXlDbG9uZShhcnIsIG4pIHtcbiAgdmFyIGNvcHkgPSBuZXcgQXJyYXkobik7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgbjsgKytpKVxuICAgIGNvcHlbaV0gPSBhcnJbaV07XG4gIHJldHVybiBjb3B5O1xufVxuXG5mdW5jdGlvbiBzcGxpY2VPbmUobGlzdCwgaW5kZXgpIHtcbiAgZm9yICg7IGluZGV4ICsgMSA8IGxpc3QubGVuZ3RoOyBpbmRleCsrKVxuICAgIGxpc3RbaW5kZXhdID0gbGlzdFtpbmRleCArIDFdO1xuICBsaXN0LnBvcCgpO1xufVxuXG5mdW5jdGlvbiB1bndyYXBMaXN0ZW5lcnMoYXJyKSB7XG4gIHZhciByZXQgPSBuZXcgQXJyYXkoYXJyLmxlbmd0aCk7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgcmV0Lmxlbmd0aDsgKytpKSB7XG4gICAgcmV0W2ldID0gYXJyW2ldLmxpc3RlbmVyIHx8IGFycltpXTtcbiAgfVxuICByZXR1cm4gcmV0O1xufVxuXG5mdW5jdGlvbiBvbmNlKGVtaXR0ZXIsIG5hbWUpIHtcbiAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICBmdW5jdGlvbiBldmVudExpc3RlbmVyKCkge1xuICAgICAgaWYgKGVycm9yTGlzdGVuZXIgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICBlbWl0dGVyLnJlbW92ZUxpc3RlbmVyKCdlcnJvcicsIGVycm9yTGlzdGVuZXIpO1xuICAgICAgfVxuICAgICAgcmVzb2x2ZShbXS5zbGljZS5jYWxsKGFyZ3VtZW50cykpO1xuICAgIH07XG4gICAgdmFyIGVycm9yTGlzdGVuZXI7XG5cbiAgICAvLyBBZGRpbmcgYW4gZXJyb3IgbGlzdGVuZXIgaXMgbm90IG9wdGlvbmFsIGJlY2F1c2VcbiAgICAvLyBpZiBhbiBlcnJvciBpcyB0aHJvd24gb24gYW4gZXZlbnQgZW1pdHRlciB3ZSBjYW5ub3RcbiAgICAvLyBndWFyYW50ZWUgdGhhdCB0aGUgYWN0dWFsIGV2ZW50IHdlIGFyZSB3YWl0aW5nIHdpbGxcbiAgICAvLyBiZSBmaXJlZC4gVGhlIHJlc3VsdCBjb3VsZCBiZSBhIHNpbGVudCB3YXkgdG8gY3JlYXRlXG4gICAgLy8gbWVtb3J5IG9yIGZpbGUgZGVzY3JpcHRvciBsZWFrcywgd2hpY2ggaXMgc29tZXRoaW5nXG4gICAgLy8gd2Ugc2hvdWxkIGF2b2lkLlxuICAgIGlmIChuYW1lICE9PSAnZXJyb3InKSB7XG4gICAgICBlcnJvckxpc3RlbmVyID0gZnVuY3Rpb24gZXJyb3JMaXN0ZW5lcihlcnIpIHtcbiAgICAgICAgZW1pdHRlci5yZW1vdmVMaXN0ZW5lcihuYW1lLCBldmVudExpc3RlbmVyKTtcbiAgICAgICAgcmVqZWN0KGVycik7XG4gICAgICB9O1xuXG4gICAgICBlbWl0dGVyLm9uY2UoJ2Vycm9yJywgZXJyb3JMaXN0ZW5lcik7XG4gICAgfVxuXG4gICAgZW1pdHRlci5vbmNlKG5hbWUsIGV2ZW50TGlzdGVuZXIpO1xuICB9KTtcbn1cbiIsImltcG9ydCB7IEV2ZW50RW1pdHRlciB9IGZyb20gJ2V2ZW50cydcclxuXHJcbmNvbnN0IGVuZ2luZSA9IG5ldyBFdmVudEVtaXR0ZXIoKVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgZW5naW5lOyIsImltcG9ydCBlbmdpbmUgZnJvbSAnLi9ldmVudHMnXHJcblxyXG5leHBvcnQgY29uc3QgZ3JhcGggPSB7XHJcbiAgdmVydGV4OiB7XHJcbiAgICBrZXlzOiBuZXcgU2V0KCksXHJcbiAgICBtYXA6IHt9XHJcbiAgfSxcclxuICBlZGdlczoge1xyXG4gICAga2V5czogbmV3IFNldCgpLFxyXG4gICAgbWFwOiB7fVxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqINCU0L7QsdCw0LLQuNGC0Ywg0L3QvtCy0YPRjiDQstC10YDRiNC40L3RgyDQsiDQs9GA0LDRhFxyXG4gICAqIEBwYXJhbSB7bnVtYmVyfSB4XHJcbiAgICogQHBhcmFtIHtudW1iZXJ9IHlcclxuICAgKiBAcmV0dXJucyB7U3ltYm9sfVxyXG4gICAqL1xyXG4gIGFkZFZlcnRleCh4LCB5LCBsYWJlbD0nIycsIHN0eWxlID0gMCkge1xyXG4gICAgbGV0IGtleSA9IFN5bWJvbChsYWJlbCk7XHJcbiAgICB0aGlzLnZlcnRleC5rZXlzLmFkZChrZXkpO1xyXG4gICAgdGhpcy52ZXJ0ZXgubWFwW2tleV0gPSB7XHJcbiAgICAgIHg6IHgsXHJcbiAgICAgIHk6IHksXHJcbiAgICAgIGxhYmVsOiBsYWJlbCxcclxuICAgICAgc3R5bGU6IHN0eWxlLFxyXG4gICAgICBjb25uZWN0ZWQ6IG5ldyBTZXQoKSxcclxuICAgICAgaW52ZXJzZTogbmV3IFNldCgpXHJcbiAgICB9O1xyXG4gICAgZW5naW5lLmVtaXQoJ2FkZFZlcnRleCcsIGtleSk7XHJcbiAgICByZXR1cm4ga2V5O1xyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqINCU0L7QsdCw0LLQuNGC0Ywg0L3QvtCy0L7QtSDRgNC10LHRgNC+XHJcbiAgICogQHBhcmFtIHtTeW1ib2x9IHN0YXJ0XHJcbiAgICogQHBhcmFtIHtTeW1ib2x9IGVuZFxyXG4gICAqIEByZXR1cm5zIHtTeW1ib2x9XHJcbiAgICovXHJcbiAgYWRkRWRnZShzdGFydCwgZW5kLCBsYWJlbD0nMTAnLCBiZW5kID0gMCwgc3R5bGUgPSAwLCBiaWRpcmVjdGlvbmFsID0gdHJ1ZSkge1xyXG4gICAgbGV0IGtleSA9IFN5bWJvbCgpO1xyXG4gICAgdGhpcy5lZGdlcy5rZXlzLmFkZChrZXkpO1xyXG4gICAgdGhpcy5lZGdlcy5tYXBba2V5XSA9IHtcclxuICAgICAgc3RhcnQ6IHN0YXJ0LFxyXG4gICAgICBlbmQ6IGVuZCxcclxuICAgICAgZGlzdGFuY2U6IG51bGwsXHJcbiAgICAgIGJlbmQ6IGJlbmQsXHJcbiAgICAgIHN0eWxlOiBzdHlsZSxcclxuICAgICAgYmlkaXJlY3Rpb25hbDogYmlkaXJlY3Rpb25hbFxyXG4gICAgfTtcclxuICAgIHRoaXMudmVydGV4Lm1hcFtzdGFydF0uY29ubmVjdGVkLmFkZChrZXkpO1xyXG4gICAgdGhpcy52ZXJ0ZXgubWFwW2VuZF0uaW52ZXJzZS5hZGQoa2V5KTtcclxuICAgIGVuZ2luZS5lbWl0KCdhZGRFZGdlJywga2V5KTtcclxuICAgIHJldHVybiBrZXk7XHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog0KPQtNCw0LvQuNGC0Ywg0LrQsNC60YPRji3RgtC+INCy0LXRgNGI0LjQvdGDXHJcbiAgICogQHBhcmFtIHtTeW1ib2x9IGtleSBcclxuICAgKi9cclxuICBkZXN0cm95RWRnZShrZXkpIHtcclxuICAgIHRoaXMudmVydGV4Lm1hcFt0aGlzLmVkZ2VzLm1hcFtrZXldLmVuZF0uaW52ZXJzZS5kZWxldGUoa2V5KTtcclxuICAgIHRoaXMudmVydGV4Lm1hcFt0aGlzLmVkZ2VzLm1hcFtrZXldLnN0YXJ0XS5jb25uZWN0ZWQuZGVsZXRlKGtleSk7XHJcbiAgICBkZWxldGUgdGhpcy5lZGdlcy5tYXBba2V5XTtcclxuICAgIHRoaXMuZWRnZXMua2V5cy5kZWxldGUoa2V5KTtcclxuICAgIGVuZ2luZS5lbWl0KCdkZXN0cm95RWRnZScsIGtleSk7XHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog0KPQtNCw0LvRj9C10Lwg0LLQtdGA0YjQuNC90YNcclxuICAgKiBAcGFyYW0ge1N5bWJvbH0ga2V5IFxyXG4gICAqL1xyXG4gIGRlc3Ryb3lWZXJ0ZXgoa2V5KSB7XHJcbiAgICBmb3IgKGxldCBpIG9mIHRoaXMudmVydGV4Lm1hcFtrZXldLmNvbm5lY3RlZCkge1xyXG4gICAgICB0aGlzLmRlc3Ryb3lFZGdlKGkpO1xyXG4gICAgfVxyXG4gICAgZm9yIChsZXQgaSBvZiB0aGlzLnZlcnRleC5tYXBba2V5XS5pbnZlcnNlKSB7XHJcbiAgICAgIHRoaXMuZGVzdHJveUVkZ2UoaSk7XHJcbiAgICB9XHJcbiAgICBkZWxldGUgdGhpcy52ZXJ0ZXgubWFwW2tleV07XHJcbiAgICB0aGlzLnZlcnRleC5rZXlzLmRlbGV0ZShrZXkpO1xyXG4gICAgZW5naW5lLmVtaXQoJ2Rlc3Ryb3lWZXJ0ZXgnLCBrZXkpO1xyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqINCc0LXQvdGP0LXQvCDRgdGC0LjQu9GMINGA0LXQsdGA0LBcclxuICAgKiBAcGFyYW0ge1N5bWJvbH0ga2V5IFxyXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBuZXdTdHlsZSBcclxuICAgKi9cclxuICBjaGFuZ2VFZGdlU3R5bGUoa2V5LCBuZXdTdHlsZSkge1xyXG4gICAgdGhpcy5lZGdlcy5tYXBba2V5XS5zdHlsZSA9IG5ld1N0eWxlO1xyXG4gICAgZW5naW5lLmVtaXQoJ2NoYW5nZUVkZ2VTdHlsZScsIGtleSk7XHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog0JzQtdC90Y/QtdC8INGB0YLQuNC70Ywg0LLQtdGA0YjQuNC90YtcclxuICAgKiBAcGFyYW0ge1N5bWJvbH0ga2V5IFxyXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBuZXdTdHlsZSBcclxuICAgKi9cclxuICBjaGFuZ2VWZXJ0ZXhTdHlsZShrZXksIG5ld1N0eWxlKSB7XHJcbiAgICB0aGlzLnZlcnRleC5tYXBba2V5XS5zdHlsZSA9IG5ld1N0eWxlO1xyXG4gICAgZW5naW5lLmVtaXQoJ2NoYW5nZVZlcnRleFN0eWxlJywga2V5KTtcclxuICB9LFxyXG4gIC8qKlxyXG4gICAqINCf0LXRgNC10LTQstC40LPQsNC10Lwg0LLQtdGA0YjQuNC90YNcclxuICAgKiBAcGFyYW0ge1N5bWJvbH0ga2V5IFxyXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBuZXdYIFxyXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBuZXdZIFxyXG4gICAqL1xyXG4gIG1vdmVWZXJ0ZXgoa2V5LCBuZXdYLCBuZXdZKSB7XHJcbiAgICB0aGlzLnZlcnRleC5tYXBba2V5XS54ID0gbmV3WDtcclxuICAgIHRoaXMudmVydGV4Lm1hcFtrZXldLnkgPSBuZXdZO1xyXG4gICAgZW5naW5lLmVtaXQoJ21vdmVWZXJ0ZXgnLCBrZXkpO1xyXG4gIH0sXHJcblxyXG4gIHNldExhYmVsKGtleSwgbmV3TGFiZWwpIHtcclxuICAgIHRoaXMudmVydGV4Lm1hcFtrZXldLmxhYmVsID0gbmV3TGFiZWw7XHJcbiAgICBlbmdpbmUuZW1pdCgnc2V0TGFiZWwnLCBrZXkpO1xyXG4gIH1cclxufTsiLCJpbXBvcnQgeyBncmFwaCB9IGZyb20gJy4vZ3JhcGgnXHJcbmltcG9ydCBlbmdpbmUgZnJvbSAnLi9ldmVudHMnXHJcbmltcG9ydCB7IEV2ZW50RW1pdHRlciB9IGZyb20gJ2V2ZW50cydcclxuXHJcbi8vINCd0LAg0LrQsNC60L7QvCDRgNCw0YHRgdGC0L7Rj9C90LjQuCDQvtGCINCy0LXRgNGI0LjQvdGLINGB0YLQsNCy0LjQvCDQv9C10YDQv9C10L3QtNC40LrRg9C70Y/RgCDRgNC10LHRgNGDXHJcbi8vINGBINC60L7QvdGC0YDQvtC70YzQvdGL0LzQuCDRgtC+0YfQutCw0LzQuCDQutGA0LjQstC+0Lkg0JHQtdC30YzQtSAo0LTQu9GPINCz0L3Rg9GC0YvRhSDRgNGR0LHQtdGAKVxyXG5jb25zdCBzcGxpdEN1cnZlID0gMC4zO1xyXG5cclxuY29uc3QgblNwbGl0Q3VydmUgPSAxIC0gc3BsaXRDdXJ2ZTtcclxuXHJcbmNvbnN0IG5zID0gJ2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJztcclxuXHJcbmV4cG9ydCBjb25zdCByZW5kZXJFdmVudHMgPSBuZXcgRXZlbnRFbWl0dGVyKClcclxuXHJcbi8qKiBcclxuICogU1ZHINGA0LXQvdC00LXRgFxyXG4qL1xyXG5jb25zdCByZW5kZXIgPSB7XHJcbiAgZG9tOiB7XHJcbiAgICB2ZXJ0ZXg6IHt9LFxyXG4gICAgZWRnZToge30sXHJcbiAgICB2ZXJ0ZXhHcm91cDogZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLndvcmtzcGFjZSBzdmcgI3ZlcnRleC1ncm91cCcpLFxyXG4gICAgZWRnZUdyb3VwOiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcud29ya3NwYWNlIHN2ZyAjZWRnZS1ncm91cCcpLFxyXG4gICAgc3ZnOiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcud29ya3NwYWNlIHN2ZycpLFxyXG4gICAgd29ya3NwYWNlOiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcud29ya3NwYWNlJyksXHJcbiAgICBzZWxlY3Rpb25DaXJjbGU6IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2NpcmNsZSNzZWxlY3Rpb24nKVxyXG4gIH0sXHJcbiAgcG9zaXRpb246IHtcclxuICAgIHg6IDAsXHJcbiAgICB5OiAwXHJcbiAgfSxcclxuICB3aWV3cG9ydDoge1xyXG4gICAgd2lkdGg6IDEwMCxcclxuICAgIGhlaWdodDogMTAwLFxyXG4gICAgaGFsZndpZHRoOiA1MCxcclxuICAgIGhhbGZoZWlnaHQ6IDUwLFxyXG4gICAgb2Zmc2V0WDogNTAsXHJcbiAgICBvZmZzZXRZOiA1MFxyXG4gIH0sXHJcbiAgc3R5bGU6IHtcclxuICAgIHZlcnRleDogW1xyXG4gICAgICB7IGJnOiAnI2ZmZicsIGZnOiAnIzAwMCcsIGJyOiAnIzAwMCcgfSxcclxuICAgICAgeyBiZzogJyMwMDAnLCBmZzogJyNmZmYnLCBicjogJyMwMDAnIH0sXHJcbiAgICAgIHsgYmc6ICcjYmJiJywgZmc6ICcjMDAwJywgYnI6ICcjMDAwJyB9XHJcbiAgICBdLFxyXG4gICAgZWRnZTogW1xyXG4gICAgICAwLFxyXG4gICAgICAyLFxyXG4gICAgICAxMCxcclxuICAgICAgJzUgNSAyIDUnXHJcbiAgICBdLFxyXG4gICAgcmFkaXVzOiAyMFxyXG4gIH0sXHJcbiAgc2VsZWN0aW9uOiB7XHJcbiAgICAvKiogQHR5cGUge251bGwgfCAndmVydGV4JyB8ICdlZGdlJ30gKi9cclxuICAgIHR5cGU6IG51bGwsXHJcbiAgICBpZDogbnVsbCxcclxuICB9LFxyXG4gIGFkZFZlcnRleChrZXkpIHtcclxuICAgIGNvbnNvbGUubG9nKCclY1JFTkRFUiVjIEFkZCB2ZXJ0ZXggJWNTeW1ib2woJyArIChrZXkuZGVzY3JpcHRpb24gPz8gJycpICsgJyknLFxyXG4gICAgICAnY29sb3I6IHdoaXRlOyBiYWNrZ3JvdW5kOiAjNGM5NjkyOyBwYWRkaW5nOiA1cHggMTBweDsgZGlzcGxheTogaW5saW5lLWJsb2NrOycsXHJcbiAgICAgICdkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IHBhZGRpbmc6IDVweCA1cHgnLFxyXG4gICAgICAnZGlzcGxheTogaW5saW5lLWJsb2NrOyBwYWRkaW5nOiA1cHggMHB4OyBjb2xvcjogIzRjOTY5MicpO1xyXG4gICAgbGV0IHZlcnRleCA9IGdyYXBoLnZlcnRleC5tYXBba2V5XTtcclxuICAgIGxldCBzdHlsZSA9IHRoaXMuc3R5bGUudmVydGV4W3ZlcnRleC5zdHlsZV1cclxuXHJcbiAgICBsZXQgZ3JvdXAgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50TlMobnMsICdnJyk7XHJcbiAgICB0aGlzLmRvbS52ZXJ0ZXhHcm91cC5hcHBlbmRDaGlsZChncm91cCk7XHJcblxyXG4gICAgbGV0IGNpcmMgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50TlMobnMsICdjaXJjbGUnKTtcclxuICAgIGNpcmMuc2V0QXR0cmlidXRlTlMobnVsbCwgJ3N0cm9rZS13aWR0aCcsICcyJyk7XHJcbiAgICBjaXJjLnNldEF0dHJpYnV0ZU5TKG51bGwsICdjeCcsIHRoaXMud2lld3BvcnQub2Zmc2V0WCArIHZlcnRleC54KTtcclxuICAgIGNpcmMuc2V0QXR0cmlidXRlTlMobnVsbCwgJ2N5JywgdGhpcy53aWV3cG9ydC5vZmZzZXRZICsgdmVydGV4LnkpO1xyXG4gICAgY2lyYy5zZXRBdHRyaWJ1dGVOUyhudWxsLCAncicsIHRoaXMuc3R5bGUucmFkaXVzKTtcclxuICAgIGNpcmMuc2V0QXR0cmlidXRlTlMobnVsbCwgJ2ZpbGwnLCB0aGlzLnN0eWxlLnZlcnRleFt2ZXJ0ZXguc3R5bGVdLmJnKTtcclxuICAgIGNpcmMuc2V0QXR0cmlidXRlTlMobnVsbCwgJ3N0cm9rZScsIHRoaXMuc3R5bGUudmVydGV4W3ZlcnRleC5zdHlsZV0uYnIpO1xyXG4gICAgZ3JvdXAuYXBwZW5kQ2hpbGQoY2lyYyk7XHJcblxyXG4gICAgbGV0IHRleHQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50TlMobnMsICd0ZXh0Jyk7XHJcbiAgICB0ZXh0LnNldEF0dHJpYnV0ZU5TKG51bGwsICd4JywgdGhpcy53aWV3cG9ydC5vZmZzZXRYICsgdmVydGV4LngpO1xyXG4gICAgdGV4dC5zZXRBdHRyaWJ1dGVOUyhudWxsLCAneScsIHRoaXMud2lld3BvcnQub2Zmc2V0WSArIHZlcnRleC55KTtcclxuICAgIHRleHQuc2V0QXR0cmlidXRlTlMobnVsbCwgJ2ZpbGwnLCBzdHlsZS5mZyk7XHJcbiAgICB0ZXh0LnRleHRDb250ZW50ID0gdmVydGV4LmxhYmVsO1xyXG5cclxuICAgIGdyb3VwLmFwcGVuZENoaWxkKHRleHQpO1xyXG4gICAgdGhpcy5kb20udmVydGV4W2tleV0gPSB7XHJcbiAgICAgIGdyb3VwOiBncm91cCxcclxuICAgICAgY2lyYzogY2lyYyxcclxuICAgICAgdGV4dDogdGV4dCxcclxuICAgIH07XHJcbiAgICBncm91cC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZWRvd24nLCAoZSkgPT4gcmVuZGVyRXZlbnRzLmVtaXQoJ3ZlcnRleC1tb3VzZS1kb3duJywga2V5LCBlKSlcclxuICAgIHJlbmRlckV2ZW50cy5lbWl0KCd2ZXJ0ZXgtYWRkZWQnLCBrZXksIHRoaXMuZG9tLnZlcnRleFtrZXldKVxyXG4gIH0sXHJcbiAgZGVzdHJveVZlcnRleChrZXkpIHtcclxuICAgIGNvbnNvbGUubG9nKCclY1JFTkRFUiVjIERlc3Ryb3kgdmVydGV4ICVjU3ltYm9sKCcgKyAoa2V5LmRlc2NyaXB0aW9uID8/ICcnKSArICcpJyxcclxuICAgICAgJ2NvbG9yOiB3aGl0ZTsgYmFja2dyb3VuZDogIzRjOTY5MjsgcGFkZGluZzogNXB4IDEwcHg7IGRpc3BsYXk6IGlubGluZS1ibG9jazsnLFxyXG4gICAgICAnZGlzcGxheTogaW5saW5lLWJsb2NrOyBwYWRkaW5nOiA1cHggNXB4JyxcclxuICAgICAgJ2Rpc3BsYXk6IGlubGluZS1ibG9jazsgcGFkZGluZzogNXB4IDBweDsgY29sb3I6ICM0Yzk2OTInKTtcclxuICAgIGlmICh0aGlzLnNlbGVjdGlvbi5pZCA9PSBrZXkpIHtcclxuICAgICAgdGhpcy5zZWxlY3QobnVsbCk7XHJcbiAgICB9XHJcbiAgICBsZXQgdmVydGV4ID0gdGhpcy5kb20udmVydGV4W2tleV07XHJcbiAgICB2ZXJ0ZXguZ3JvdXAucmVtb3ZlKCk7XHJcbiAgICB2ZXJ0ZXguY2lyYy5yZW1vdmUoKTtcclxuICAgIHZlcnRleC50ZXh0LnJlbW92ZSgpO1xyXG4gICAgZGVsZXRlIHZlcnRleC5ncm91cDtcclxuICAgIGRlbGV0ZSB2ZXJ0ZXguY2lyYztcclxuICAgIGRlbGV0ZSB2ZXJ0ZXgudGV4dDtcclxuICAgIGRlbGV0ZSB0aGlzLmRvbS52ZXJ0ZXhba2V5XTtcclxuICAgIHJlbmRlckV2ZW50cy5lbWl0KCd2ZXJ0ZXgtZGVzdHJveWVkJywga2V5KVxyXG4gIH0sXHJcbiAgYWRkRWRnZShrZXkpIHtcclxuICAgIGNvbnNvbGUubG9nKCclY1JFTkRFUiVjIEFkZCBlZGdlICVjU3ltYm9sKCcgKyAoa2V5LmRlc2NyaXB0aW9uID8/ICcnKSArICcpJyxcclxuICAgICAgJ2NvbG9yOiB3aGl0ZTsgYmFja2dyb3VuZDogIzRjOTY5MjsgcGFkZGluZzogNXB4IDEwcHg7IGRpc3BsYXk6IGlubGluZS1ibG9jazsnLFxyXG4gICAgICAnZGlzcGxheTogaW5saW5lLWJsb2NrOyBwYWRkaW5nOiA1cHggNXB4JyxcclxuICAgICAgJ2Rpc3BsYXk6IGlubGluZS1ibG9jazsgcGFkZGluZzogNXB4IDBweDsgY29sb3I6ICM0Yzk2OTInKTtcclxuICAgIGxldCBzdGFydCA9IGdyYXBoLnZlcnRleC5tYXBbZ3JhcGguZWRnZXMubWFwW2tleV0uc3RhcnRdO1xyXG4gICAgbGV0IGVuZCA9IGdyYXBoLnZlcnRleC5tYXBbZ3JhcGguZWRnZXMubWFwW2tleV0uZW5kXTtcclxuXHJcbiAgICBsZXQgZ3JvdXAgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50TlMobnMsICdnJyk7XHJcbiAgICB0aGlzLmRvbS5lZGdlR3JvdXAuYXBwZW5kQ2hpbGQoZ3JvdXApO1xyXG5cclxuICAgIGxldCBjbGlja1pvbmUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50TlMobnMsICdwYXRoJyk7XHJcbiAgICBjbGlja1pvbmUuc2V0QXR0cmlidXRlTlMobnVsbCwgJ2ZpbGwnLCAnbm9uZScpO1xyXG4gICAgY2xpY2tab25lLnNldEF0dHJpYnV0ZU5TKG51bGwsICdzdHJva2UnLCAnIzRjOTY5MicpO1xyXG4gICAgY2xpY2tab25lLnNldEF0dHJpYnV0ZU5TKG51bGwsICdvcGFjaXR5JywgJzAnKTtcclxuICAgIGNsaWNrWm9uZS5zZXRBdHRyaWJ1dGVOUyhudWxsLCAnc3Ryb2tlLXdpZHRoJywgJzE1Jyk7XHJcbiAgICBncm91cC5hcHBlbmRDaGlsZChjbGlja1pvbmUpO1xyXG5cclxuICAgIGxldCB2aXNpYmxlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudE5TKG5zLCAncGF0aCcpO1xyXG4gICAgdmlzaWJsZS5zZXRBdHRyaWJ1dGVOUyhudWxsLCAnZmlsbCcsICdub25lJyk7XHJcbiAgICB2aXNpYmxlLnNldEF0dHJpYnV0ZU5TKG51bGwsICdzdHJva2Utd2lkdGgnLCAnMicpO1xyXG4gICAgdmlzaWJsZS5zZXRBdHRyaWJ1dGVOUyhudWxsLCAnc3Ryb2tlJywgJyMwMDAnKTtcclxuICAgIGdyb3VwLmFwcGVuZENoaWxkKHZpc2libGUpO1xyXG5cclxuICAgIGdyb3VwLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlZG93bicsIChlKSA9PiByZW5kZXJFdmVudHMuZW1pdCgnZWRnZS1tb3VzZS1kb3duJywga2V5LCBlKSlcclxuXHJcbiAgICB0aGlzLmRvbS5lZGdlW2tleV0gPSB7XHJcbiAgICAgIGdyb3VwOiBncm91cCxcclxuICAgICAgdmlzaWJsZTogdmlzaWJsZSxcclxuICAgICAgY2xpY2tab25lOiBjbGlja1pvbmVcclxuICAgIH07XHJcbiAgICByZW5kZXJFdmVudHMuZW1pdCgnZWRnZS1hZGRlZCcsIGtleSwgdGhpcy5kb20uZWRnZVtrZXldKVxyXG4gICAgdGhpcy5yZXBvc2l0aW9uRWRnZShrZXkpO1xyXG4gIH0sXHJcbiAgZGVzdHJveUVkZ2Uoa2V5KSB7XHJcbiAgICBjb25zb2xlLmxvZygnJWNSRU5ERVIlYyBEZXN0cm95IGVkZ2UgJWNTeW1ib2woJyArIChrZXkuZGVzY3JpcHRpb24gPz8gJycpICsgJyknLFxyXG4gICAgICAnY29sb3I6IHdoaXRlOyBiYWNrZ3JvdW5kOiAjNGM5NjkyOyBwYWRkaW5nOiA1cHggMTBweDsgZGlzcGxheTogaW5saW5lLWJsb2NrOycsXHJcbiAgICAgICdkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IHBhZGRpbmc6IDVweCA1cHgnLFxyXG4gICAgICAnZGlzcGxheTogaW5saW5lLWJsb2NrOyBwYWRkaW5nOiA1cHggMHB4OyBjb2xvcjogIzRjOTY5MicpO1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0aW9uLmlkID09IGtleSkge1xyXG4gICAgICB0aGlzLnNlbGVjdChudWxsKTtcclxuICAgIH1cclxuICAgIGxldCBlZGdlID0gdGhpcy5kb20uZWRnZVtrZXldO1xyXG4gICAgZWRnZS5ncm91cC5yZW1vdmUoKTtcclxuICAgIGVkZ2UudmlzaWJsZS5yZW1vdmUoKTtcclxuICAgIGVkZ2UuY2xpY2tab25lLnJlbW92ZSgpO1xyXG4gICAgZGVsZXRlIGVkZ2UuZ3JvdXA7XHJcbiAgICBkZWxldGUgZWRnZS52aXNpYmxlO1xyXG4gICAgZGVsZXRlIGVkZ2UuY2xpY2tab25lO1xyXG4gICAgZGVsZXRlIHRoaXMuZG9tLmVkZ2Vba2V5XTtcclxuICAgIHJlbmRlckV2ZW50cy5lbWl0KCdlZGdlLWRlc3Ryb3llZCcsIGtleSlcclxuICB9LFxyXG4gIHJlcG9zaXRpb25FZGdlKGlkKSB7XHJcbiAgICBsZXQgZWRnZSA9IGdyYXBoLmVkZ2VzLm1hcFtpZF07XHJcbiAgICBsZXQgc3RhcnQgPSBncmFwaC52ZXJ0ZXgubWFwW2VkZ2Uuc3RhcnRdO1xyXG4gICAgbGV0IGVuZCA9IGdyYXBoLnZlcnRleC5tYXBbZWRnZS5lbmRdO1xyXG4gICAgaWYgKGVkZ2UuYmVuZCkge1xyXG4gICAgICBsZXQgdiA9IHtcclxuICAgICAgICB4OiBlbmQueCAtIHN0YXJ0LngsXHJcbiAgICAgICAgeTogZW5kLnkgLSBzdGFydC55XHJcbiAgICAgIH1cclxuICAgICAgbGV0IGQgPSBNYXRoLnNxcnQodi54ICogdi54ICsgdi55ICogdi55KTtcclxuICAgICAgbGV0IHBhdGggPVxyXG4gICAgICAgIGBNICR7c3RhcnQueCArIHRoaXMud2lld3BvcnQub2Zmc2V0WH0gYCArXHJcbiAgICAgICAgYCR7c3RhcnQueSArIHRoaXMud2lld3BvcnQub2Zmc2V0WX0gYCArXHJcbiAgICAgICAgYEMgJHtzdGFydC54ICsgc3BsaXRDdXJ2ZSAqIHYueCArIHYueSAvIGQgKiBlZGdlLmJlbmQgKyB0aGlzLndpZXdwb3J0Lm9mZnNldFh9IGAgK1xyXG4gICAgICAgIGAke3N0YXJ0LnkgKyBzcGxpdEN1cnZlICogdi55IC0gdi54IC8gZCAqIGVkZ2UuYmVuZCArIHRoaXMud2lld3BvcnQub2Zmc2V0WX0gYCArXHJcbiAgICAgICAgYCR7c3RhcnQueCArIG5TcGxpdEN1cnZlICogdi54ICsgdi55IC8gZCAqIGVkZ2UuYmVuZCArIHRoaXMud2lld3BvcnQub2Zmc2V0WH0gYCArXHJcbiAgICAgICAgYCR7c3RhcnQueSArIG5TcGxpdEN1cnZlICogdi55IC0gdi54IC8gZCAqIGVkZ2UuYmVuZCArIHRoaXMud2lld3BvcnQub2Zmc2V0WX0gYCArXHJcbiAgICAgICAgYCR7ZW5kLnggKyB0aGlzLndpZXdwb3J0Lm9mZnNldFh9IGAgK1xyXG4gICAgICAgIGAke2VuZC55ICsgdGhpcy53aWV3cG9ydC5vZmZzZXRZfWA7XHJcbiAgICAgIHRoaXMuZG9tLmVkZ2VbaWRdLnZpc2libGUuc2V0QXR0cmlidXRlTlMobnVsbCwgJ2QnLCBwYXRoKTtcclxuICAgICAgdGhpcy5kb20uZWRnZVtpZF0uY2xpY2tab25lLnNldEF0dHJpYnV0ZU5TKG51bGwsICdkJywgcGF0aCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBsZXQgcGF0aCA9XHJcbiAgICAgICAgYE0gJHtzdGFydC54ICsgdGhpcy53aWV3cG9ydC5vZmZzZXRYfSAke3N0YXJ0LnkgKyB0aGlzLndpZXdwb3J0Lm9mZnNldFl9IGAgK1xyXG4gICAgICAgIGBMICR7ZW5kLnggKyB0aGlzLndpZXdwb3J0Lm9mZnNldFh9ICR7ZW5kLnkgKyB0aGlzLndpZXdwb3J0Lm9mZnNldFl9YFxyXG4gICAgICB0aGlzLmRvbS5lZGdlW2lkXS52aXNpYmxlLnNldEF0dHJpYnV0ZU5TKG51bGwsICdkJywgcGF0aCk7XHJcbiAgICAgIHRoaXMuZG9tLmVkZ2VbaWRdLmNsaWNrWm9uZS5zZXRBdHRyaWJ1dGVOUyhudWxsLCAnZCcsIHBhdGgpO1xyXG4gICAgfVxyXG4gIH0sXHJcbiAgbW92ZSh4LCB5KSB7XHJcbiAgICB0aGlzLnBvc2l0aW9uLnggPSB4O1xyXG4gICAgdGhpcy5wb3NpdGlvbi55ID0geTtcclxuICAgIHRoaXMuZG9tLmVkZ2VHcm91cC5zZXRBdHRyaWJ1dGVOUyhudWxsLCAndHJhbnNmb3JtJywgJ3RyYW5zbGF0ZSgnICsgdGhpcy5wb3NpdGlvbi54ICsgJywgJyArIHRoaXMucG9zaXRpb24ueSArICcpJyk7XHJcbiAgICB0aGlzLmRvbS52ZXJ0ZXhHcm91cC5zZXRBdHRyaWJ1dGVOUyhudWxsLCAndHJhbnNmb3JtJywgJ3RyYW5zbGF0ZSgnICsgdGhpcy5wb3NpdGlvbi54ICsgJywgJyArIHRoaXMucG9zaXRpb24ueSArICcpJyk7XHJcbiAgICB0aGlzLmRvbS5zZWxlY3Rpb25DaXJjbGUuc2V0QXR0cmlidXRlTlMobnVsbCwgJ3RyYW5zZm9ybScsICd0cmFuc2xhdGUoJyArIHRoaXMucG9zaXRpb24ueCArICcsICcgKyB0aGlzLnBvc2l0aW9uLnkgKyAnKScpO1xyXG4gICAgZG9jdW1lbnQuYm9keS5zdHlsZS5iYWNrZ3JvdW5kUG9zaXRpb25YID0geCAlIDEwMCAtIDUgKyAncHgnO1xyXG4gICAgZG9jdW1lbnQuYm9keS5zdHlsZS5iYWNrZ3JvdW5kUG9zaXRpb25ZID0geSAlIDEwMCAtIDUgKyAncHgnO1xyXG4gIH0sXHJcbiAgcmVzaXplKCkge1xyXG4gICAgbGV0IHJlY3QgPSB0aGlzLmRvbS53b3Jrc3BhY2UuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcbiAgICB0aGlzLmRvbS5zdmcuc2V0QXR0cmlidXRlKCd3aWR0aCcsIHJlY3Qud2lkdGgpO1xyXG4gICAgdGhpcy5kb20uc3ZnLnNldEF0dHJpYnV0ZSgnaGVpZ2h0JywgcmVjdC5oZWlnaHQpO1xyXG4gICAgdGhpcy53aWV3cG9ydC5oYWxmd2lkdGggPSBNYXRoLnJvdW5kKCh0aGlzLndpZXdwb3J0LndpZHRoID0gcmVjdC53aWR0aCkgLyAyKTtcclxuICAgIHRoaXMud2lld3BvcnQuaGFsZmhlaWdodCA9IE1hdGgucm91bmQoKHRoaXMud2lld3BvcnQuaGVpZ2h0ID0gcmVjdC5oZWlnaHQpIC8gMik7XHJcbiAgICB0aGlzLndpZXdwb3J0Lm9mZnNldFggPSAwO1xyXG4gICAgdGhpcy53aWV3cG9ydC5vZmZzZXRZID0gMDtcclxuICAgIC8vdGhpcy5yZXBvc2l0aW9uKCk7XHJcbiAgfSxcclxuICAvKipcclxuICAgKiBcclxuICAgKiBAcGFyYW0ge251bGx8J3ZlcnRleCd8J2VkZ2UnfSB0eXBlIFxyXG4gICAqIEBwYXJhbSB7U3ltYm9sfSBrZXkgXHJcbiAgICovXHJcbiAgc2VsZWN0KHR5cGUsIGtleSkge1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0aW9uLnR5cGUgPT0gJ3ZlcnRleCcpIHtcclxuICAgICAgdGhpcy5kb20uc2VsZWN0aW9uQ2lyY2xlLnNldEF0dHJpYnV0ZU5TKG51bGwsICdvcGFjaXR5JywgMCk7XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMuc2VsZWN0aW9uLnR5cGUgPT0gJ2VkZ2UnKSB7XHJcbiAgICAgIHRoaXMuZG9tLmVkZ2VbdGhpcy5zZWxlY3Rpb24uaWRdLmNsaWNrWm9uZS5zZXRBdHRyaWJ1dGVOUyhudWxsLCAnb3BhY2l0eScsIDApO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuc2VsZWN0aW9uLnR5cGUgPSB0eXBlO1xyXG4gICAgdGhpcy5zZWxlY3Rpb24uaWQgPSBrZXk7XHJcblxyXG4gICAgaWYgKHR5cGUgPT0gJ3ZlcnRleCcpIHtcclxuICAgICAgdGhpcy5kb20uc2VsZWN0aW9uQ2lyY2xlLnNldEF0dHJpYnV0ZU5TKG51bGwsICdvcGFjaXR5JywgMC41KTtcclxuICAgICAgdGhpcy5kb20uc2VsZWN0aW9uQ2lyY2xlLnNldEF0dHJpYnV0ZU5TKG51bGwsICdjeCcsIGdyYXBoLnZlcnRleC5tYXBba2V5XS54KTtcclxuICAgICAgdGhpcy5kb20uc2VsZWN0aW9uQ2lyY2xlLnNldEF0dHJpYnV0ZU5TKG51bGwsICdjeScsIGdyYXBoLnZlcnRleC5tYXBba2V5XS55KTtcclxuICAgIH0gZWxzZSBpZiAodHlwZSA9PSAnZWRnZScpIHtcclxuICAgICAgdGhpcy5kb20uZWRnZVtrZXldLmNsaWNrWm9uZS5zZXRBdHRyaWJ1dGVOUyhudWxsLCAnb3BhY2l0eScsIDAuNSk7XHJcbiAgICB9XHJcbiAgICByZW5kZXJFdmVudHMuZW1pdCgnc2VsZWN0aW9uLWNoYW5nZWQnLCB0eXBlLCBrZXkpXHJcbiAgfSxcclxuICBtb3ZlU2VsZWN0aW9uKCkge1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0aW9uLnR5cGUgPT0gJ3ZlcnRleCcpIHtcclxuICAgICAgLy8gdGhpcy5kb20uc2VsZWN0aW9uQ2lyY2xlLnNldEF0dHJpYnV0ZU5TKG51bGwsICdvcGFjaXR5JywgMSk7XHJcbiAgICAgIHRoaXMuZG9tLnNlbGVjdGlvbkNpcmNsZS5zZXRBdHRyaWJ1dGVOUyhudWxsLCAnY3gnLCBncmFwaC52ZXJ0ZXgubWFwW3RoaXMuc2VsZWN0aW9uLmlkXS54KTtcclxuICAgICAgdGhpcy5kb20uc2VsZWN0aW9uQ2lyY2xlLnNldEF0dHJpYnV0ZU5TKG51bGwsICdjeScsIGdyYXBoLnZlcnRleC5tYXBbdGhpcy5zZWxlY3Rpb24uaWRdLnkpO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLnNlbGVjdGlvbi50eXBlID09ICdlZGdlJykge1xyXG4gICAgICAvLyB0aGlzLmRvbS5lZGdlW3RoaXMuc2VsZWN0aW9uLmlkXS5jbGlja1pvbmUuc2V0QXR0cmlidXRlTlMobnVsbCwgJ29wYWNpdHknLCAxKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIC8vIHRoaXMuZG9tLnNlbGVjdGlvbkNpcmNsZS5zZXRBdHRyaWJ1dGVOUyhudWxsLCAnb3BhY2l0eScsIDApO1xyXG4gICAgfVxyXG4gIH0sXHJcbiAgbW92ZVZlcnRleChrZXkpIHtcclxuICAgIGxldCB2ZXJ0ZXggPSBncmFwaC52ZXJ0ZXgubWFwW2tleV07XHJcbiAgICB0aGlzLmRvbS52ZXJ0ZXhba2V5XS5jaXJjLnNldEF0dHJpYnV0ZU5TKG51bGwsICdjeCcsIHRoaXMud2lld3BvcnQub2Zmc2V0WCArIHZlcnRleC54KTtcclxuICAgIHRoaXMuZG9tLnZlcnRleFtrZXldLmNpcmMuc2V0QXR0cmlidXRlTlMobnVsbCwgJ2N5JywgdGhpcy53aWV3cG9ydC5vZmZzZXRZICsgdmVydGV4LnkpO1xyXG4gICAgdGhpcy5kb20udmVydGV4W2tleV0udGV4dC5zZXRBdHRyaWJ1dGVOUyhudWxsLCAneCcsIHRoaXMud2lld3BvcnQub2Zmc2V0WCArIHZlcnRleC54KTtcclxuICAgIHRoaXMuZG9tLnZlcnRleFtrZXldLnRleHQuc2V0QXR0cmlidXRlTlMobnVsbCwgJ3knLCB0aGlzLndpZXdwb3J0Lm9mZnNldFkgKyB2ZXJ0ZXgueSk7XHJcbiAgICBmb3IgKGxldCBpIG9mIHZlcnRleC5jb25uZWN0ZWQpXHJcbiAgICAgIHRoaXMucmVwb3NpdGlvbkVkZ2UoaSk7XHJcbiAgICBmb3IgKGxldCBpIG9mIHZlcnRleC5pbnZlcnNlKVxyXG4gICAgICB0aGlzLnJlcG9zaXRpb25FZGdlKGkpO1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0aW9uLnR5cGUgPT0gJ3ZlcnRleCcgJiYgdGhpcy5zZWxlY3Rpb24uaWQgPT0ga2V5KVxyXG4gICAgICB0aGlzLm1vdmVTZWxlY3Rpb24oKTtcclxuICB9LFxyXG4gIHJlc3R5bGVWZXJ0ZXgoa2V5KSB7XHJcbiAgICBsZXQgdmVydGV4ID0gZ3JhcGgudmVydGV4Lm1hcFtrZXldO1xyXG4gICAgbGV0IGRvbSA9IHRoaXMuZG9tLnZlcnRleFtrZXldO1xyXG4gICAgZG9tLmNpcmMuc2V0QXR0cmlidXRlTlMobnVsbCwgJ2ZpbGwnLCB0aGlzLnN0eWxlLnZlcnRleFt2ZXJ0ZXguc3R5bGVdLmJnKTtcclxuICAgIGRvbS5jaXJjLnNldEF0dHJpYnV0ZU5TKG51bGwsICdzdHJva2UnLCB0aGlzLnN0eWxlLnZlcnRleFt2ZXJ0ZXguc3R5bGVdLmJyKTtcclxuICAgIGRvbS50ZXh0LnNldEF0dHJpYnV0ZU5TKG51bGwsICdmaWxsJywgdGhpcy5zdHlsZS52ZXJ0ZXhbdmVydGV4LnN0eWxlXS5mZyk7XHJcbiAgfSxcclxuICBzZXRMYWJlbChrZXkpIHtcclxuICAgIHRoaXMuZG9tLnZlcnRleFtrZXldLnRleHQudGV4dENvbnRlbnQgPSBncmFwaC52ZXJ0ZXgubWFwW2tleV0ubGFiZWw7XHJcbiAgfVxyXG59XHJcblxyXG5yZW5kZXIuZG9tLnN2Zy5hZGRFdmVudExpc3RlbmVyKCdtb3VzZW1vdmUnLCBlID0+IHJlbmRlckV2ZW50cy5lbWl0KCdtb3VzZS1tb3ZlJywgZSkpXHJcbnJlbmRlci5kb20uc3ZnLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlZG93bicsIGUgPT4gcmVuZGVyRXZlbnRzLmVtaXQoJ21vdXNlLWRvd24nLCBlKSlcclxuZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignbW91c2V1cCcsIGUgPT4gcmVuZGVyRXZlbnRzLmVtaXQoJ21vdXNlLXVwJywgZSkpXHJcblxyXG5jb25zdCBsb2FkID0ge1xyXG4gIGVkZ2VzOiB7XHJcbiAgICBwYXJzZShzdHIpIHtcclxuICAgICAgcmV0dXJuIHN0ci5zcGxpdCgnXFxuJylcclxuICAgICAgICAubWFwKGkgPT4gaS5zcGxpdCgnICcpLmZpbHRlcihpID0+IGkpKVxyXG4gICAgICAgIC5tYXAoaSA9PiAoaS5sZW5ndGggPT0gMSA/IHsgdHlwZTogJ3ZlcnRleCcsIGxhYmVsOiBpWzBdIH0gOlxyXG4gICAgICAgICAgaS5sZW5ndGggPT0gMiB8fCBpLmxlbmd0aCA9PSAzID8geyB0eXBlOiAnZWRnZScsIHN0YXJ0OiBpWzBdLCBlbmQ6IGlbMV0sIHc6IGlbMl0gfSA6XHJcbiAgICAgICAgICAgIGkubGVuZ3RoID09IDAgPyB7IHR5cGU6ICdub3RoaW5nJyB9IDpcclxuICAgICAgICAgICAgICB7IHR5cGU6ICdzb21ldGhpbmcnIH0pKVxyXG4gICAgfVxyXG4gIH1cclxufTtcclxuXHJcbndpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLCBfID0+IHJlbmRlci5yZXNpemUoKSk7XHJcbnJlbmRlci5yZXNpemUoKTtcclxucmVuZGVyLm1vdmUocmVuZGVyLndpZXdwb3J0LmhhbGZ3aWR0aCwgcmVuZGVyLndpZXdwb3J0LmhhbGZoZWlnaHQpO1xyXG5cclxuZW5naW5lLm9uKCdhZGRWZXJ0ZXgnLCBkID0+IHJlbmRlci5hZGRWZXJ0ZXgoZCkpXHJcbmVuZ2luZS5vbignYWRkRWRnZScsIGQgPT4gcmVuZGVyLmFkZEVkZ2UoZCkpXHJcbmVuZ2luZS5vbignZGVzdHJveUVkZ2UnLCBkID0+IHJlbmRlci5kZXN0cm95RWRnZShkKSlcclxuZW5naW5lLm9uKCdkZXN0cm95VmVydGV4JywgZCA9PiByZW5kZXIuZGVzdHJveVZlcnRleChkKSlcclxuLy8gZW5naW5lLm9uKCdjaGFuZ2VFZGdlU3R5bGUnLCByZW5kZXIuY2hhbmdlRWRnZVN0eWxlKVxyXG5lbmdpbmUub24oJ2NoYW5nZVZlcnRleFN0eWxlJywgZCA9PiByZW5kZXIucmVzdHlsZVZlcnRleChkKSlcclxuZW5naW5lLm9uKCdtb3ZlVmVydGV4JywgZCA9PiByZW5kZXIubW92ZVZlcnRleChkKSlcclxuZW5naW5lLm9uKCdzZXRMYWJlbCcsIGQgPT4gcmVuZGVyLnNldExhYmVsKGQpKVxyXG5cclxuY29uc29sZS5sb2cocmVuZGVyKVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgcmVuZGVyOyIsImltcG9ydCAnLi9wYW4nXHJcbmltcG9ydCAnLi9zZWxlY3QnXHJcbmltcG9ydCAnLi9iYXNpYydcclxuaW1wb3J0ICcuL2NyZWF0ZSdcclxuaW1wb3J0ICcuL3JlbmFtZSdcclxuLy8gTm90IGFjdHVhbGx5IHRvb2xcclxuaW1wb3J0ICcuL2ZvcmNlJyIsImltcG9ydCB7IGdyYXBoIH0gZnJvbSBcIi4uL2dyYXBoXCI7XG5pbXBvcnQgcmVuZGVyIGZyb20gXCIuLi9yZW5kZXJcIjtcblxuZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcigna2V5ZG93bicsIGZ1bmN0aW9uKGUpIHtcbiAgLy8gSWdub3JlIGV2ZW50cyBpbiBpbnB1dCBmaWVsZHNcbiAgaWYgKFsnaW5wdXQnLCAndGV4dGFyZWEnXS5pbmNsdWRlcyhlLnRhcmdldC50YWdOYW1lLnRvTG93ZXJDYXNlKCkpKVxuICAgIHJldHVybjtcbiAgaWYgKGUua2V5ID09ICdEZWxldGUnKSB7XG4gICAgaWYgKHJlbmRlci5zZWxlY3Rpb24udHlwZSA9PSAndmVydGV4JylcbiAgICAgIGdyYXBoLmRlc3Ryb3lWZXJ0ZXgocmVuZGVyLnNlbGVjdGlvbi5pZCk7XG4gICAgZWxzZSBpZiAocmVuZGVyLnNlbGVjdGlvbi50eXBlID09ICdlZGdlJylcbiAgICAgIGdyYXBoLmRlc3Ryb3lFZGdlKHJlbmRlci5zZWxlY3Rpb24uaWQpO1xuICB9XG59KTsiLCJpbXBvcnQgeyBncmFwaCB9IGZyb20gXCIuLi9ncmFwaFwiXG5pbXBvcnQgcmVuZGVyLCB7IHJlbmRlckV2ZW50cyB9IGZyb20gXCIuLi9yZW5kZXJcIlxuaW1wb3J0IHsgZGVmdG9vbCB9IGZyb20gXCIuL2ZuXCI7XG5pbXBvcnQgeyBhY3RpdmVUb29sIH0gZnJvbSBcIi4vc3RhdGVcIlxuXG5sZXQgcGF0aFByZXZpZXcgPSByZW5kZXIuZG9tLnN2Zy5xdWVyeVNlbGVjdG9yKCcjZWRnZS1wcmV2aWV3Jyk7XG5sZXQgc3RhcnQgPSB7IHg6IDAsIHk6IDAgfVxuXG5yZW5kZXIuZG9tLnN2Zy5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGUgPT4ge1xuICBpZiAoYWN0aXZlVG9vbC5uYW1lID09ICdhZGQtdmVydGV4JylcbiAgICBncmFwaC5hZGRWZXJ0ZXgoZS5jbGllbnRYIC0gcmVuZGVyLnBvc2l0aW9uLngsIGUuY2xpZW50WSAtIHJlbmRlci5wb3NpdGlvbi55KTtcbn0pO1xuXG5yZW5kZXJFdmVudHMub24oJ21vdXNlLW1vdmUnLCBlID0+IHtcbiAgaWYgKGFjdGl2ZVRvb2wubmFtZSA9PSAnYWRkLWVkZ2UnKSB7XG4gICAgaWYgKGFjdGl2ZVRvb2wub3B0cy5zdGFydClcbiAgICAgIHBhdGhQcmV2aWV3LnNldEF0dHJpYnV0ZU5TKG51bGwsICdkJywgYG0gJHtzdGFydC54fSAke3N0YXJ0Lnl9IGwgJHtlLmNsaWVudFggLSBzdGFydC54fSAke2UuY2xpZW50WSAtIHN0YXJ0Lnl9IHpgKVxuICB9XG59KTtcblxucmVuZGVyRXZlbnRzLm9uKCd2ZXJ0ZXgtbW91c2UtZG93bicsIChrZXksIGUpID0+IHtcbiAgaWYgKGFjdGl2ZVRvb2wubmFtZSA9PSAnYWRkLWVkZ2UnKSB7XG4gICAgaWYgKGFjdGl2ZVRvb2wub3B0cy5zdGFydCkge1xuICAgICAgZ3JhcGguYWRkRWRnZShhY3RpdmVUb29sLm9wdHMuc3RhcnQsIGtleSk7XG4gICAgICBhY3RpdmVUb29sLm9wdHMuc3RhcnQgPSBudWxsO1xuICAgICAgcGF0aFByZXZpZXcuc3R5bGUub3BhY2l0eSA9IDA7XG4gICAgfSBlbHNlIHtcbiAgICAgIGFjdGl2ZVRvb2wub3B0cy5zdGFydCA9IGtleTtcbiAgICAgIHN0YXJ0LnggPSBncmFwaC52ZXJ0ZXgubWFwW2tleV0ueCArIHJlbmRlci5wb3NpdGlvbi54O1xuICAgICAgc3RhcnQueSA9IGdyYXBoLnZlcnRleC5tYXBba2V5XS55ICsgcmVuZGVyLnBvc2l0aW9uLnk7XG4gICAgICBwYXRoUHJldmlldy5zZXRBdHRyaWJ1dGVOUyhudWxsLCAnZCcsIGBtICR7c3RhcnQueH0gJHtzdGFydC55fSBsICR7ZS5jbGllbnRYIC0gc3RhcnQueH0gJHtlLmNsaWVudFkgLSBzdGFydC55fSB6YClcbiAgICAgIHBhdGhQcmV2aWV3LnN0eWxlLm9wYWNpdHkgPSAxO1xuICAgIH1cbiAgfVxufSlcblxuZGVmdG9vbChcIiNhZGQtdmVydGV4XCIsICdhZGQtdmVydGV4JylcbmRlZnRvb2woXCIjYWRkLWVkZ2VcIiwgJ2FkZC1lZGdlJywgeyBhbGxvd1NlbGVjdGlvbjogZmFsc2UgfSwgKCkgPT4ge1xuICByZW5kZXIuc2VsZWN0KG51bGwpO1xufSwgKCkgPT4ge1xuICBwYXRoUHJldmlldy5zdHlsZS5vcGFjaXR5ID0gMDtcbn0pOyIsImltcG9ydCB7IGFjdGl2ZVRvb2wgfSBmcm9tICcuL3N0YXRlJ1xyXG5cclxubGV0IHNlbGVjdGVkID0gbnVsbDtcclxubGV0IHNlbGVjdGVkRXhpdCA9ICgpID0+IHt9O1xyXG5cclxubGV0IHJlbmFtZUlucHV0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI2xhYmVsJyk7XHJcblxyXG5mdW5jdGlvbiBkZWVwQ2xvbmUob2JqKSB7XHJcbiAgY29uc3QgY2xPYmogPSB7fTtcclxuICBmb3IoY29uc3QgaSBpbiBvYmopIHtcclxuICAgIGlmIChvYmpbaV0gaW5zdGFuY2VvZiBPYmplY3QpIHtcclxuICAgICAgY2xPYmpbaV0gPSBkZWVwQ2xvbmUob2JqW2ldKTtcclxuICAgICAgY29udGludWU7XHJcbiAgICB9XHJcbiAgICBjbE9ialtpXSA9IG9ialtpXTtcclxuICB9XHJcbiAgcmV0dXJuIGNsT2JqO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gZGVmdG9vbChzZWwsIG5hbWUsIG9wdHMgPSB7fSwgb25BY3RpdmUgPSAoKSA9PiB7fSwgb25FeGl0ID0gKCkgPT4ge30pIHtcclxuICBsZXQgZWxlbSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3Ioc2VsKTtcclxuICBvcHRzLmFsbG93U2VsZWN0aW9uID0gb3B0cy5hbGxvd1NlbGVjdGlvbiA/PyB0cnVlO1xyXG5cclxuICBsZXQgZm4gPSAoKSA9PiB7XHJcbiAgICBpZiAoc2VsZWN0ZWQpIHtcclxuICAgICAgc2VsZWN0ZWQuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJyk7XHJcbiAgICAgIHNlbGVjdGVkRXhpdCgpO1xyXG4gICAgfVxyXG4gICAgc2VsZWN0ZWQgPSBlbGVtO1xyXG4gICAgZWxlbS5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKTtcclxuICAgIGFjdGl2ZVRvb2wubmFtZSA9IG5hbWU7XHJcbiAgICBhY3RpdmVUb29sLm9wdHMgPSBkZWVwQ2xvbmUob3B0cyk7O1xyXG4gICAgcmVuYW1lSW5wdXQuc2V0QXR0cmlidXRlKCdzdHlsZScsIFwidG9wOiAtMTAwcHg7IGxlZnQ6IC0xMDBweDtcIilcclxuICAgIG9uQWN0aXZlKCk7XHJcbiAgICBzZWxlY3RlZEV4aXQgPSBvbkV4aXQ7XHJcbiAgfTtcclxuXHJcbiAgaWYgKGVsZW0uY2xhc3NMaXN0LmNvbnRhaW5zKCdhY3RpdmUnKSlcclxuICAgIGZuKCk7XHJcbiAgZWxlbS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZuKTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIG1ha2VUb2dnbGUoc2VsLCBjYWxsYmFjaywgZGZsdD1mYWxzZSkge1xyXG4gIGxldCBlbGVtID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihzZWwpO1xyXG4gIGxldCBvbiA9IGRmbHQ7XHJcbiAgZWxlbS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIChlKSA9PiB7XHJcbiAgICBvbiA9ICFvbjtcclxuICAgIGVsZW0uY2xhc3NMaXN0LnRvZ2dsZSgnYWN0aXZlJywgb24pO1xyXG4gICAgY2FsbGJhY2sob24pO1xyXG4gIH0pO1xyXG59XHJcblxyXG5sZXQgY3VycmVudFBvcHVwID0gbnVsbDtcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBtYWtlUG9wdXAoc2VsZWN0b3IsIGJ1dHRvblNlbGVjdG9yLCB3aGVuKSB7XHJcbiAgbGV0IGJ1dHRvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYnV0dG9uU2VsZWN0b3IpO1xyXG4gIGxldCBwb3B1cCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3Ioc2VsZWN0b3IpO1xyXG4gIHBvcHVwLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XHJcbiAgYnV0dG9uLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZSA9PiB7XHJcbiAgICBpZiAod2hlbiAmJiAhd2hlbigpKVxyXG4gICAgICByZXR1cm47XHJcbiAgICBlLnNob3VsZE5vdENsb3NlUG9wdXAgPSB0cnVlO1xyXG4gICAgaWYgKGN1cnJlbnRQb3B1cClcclxuICAgICAgY3VycmVudFBvcHVwLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XHJcbiAgICBwb3B1cC5zdHlsZS5kaXNwbGF5ID0gbnVsbDtcclxuICAgIGN1cnJlbnRQb3B1cCA9IHBvcHVwO1xyXG4gIH0pO1xyXG5cclxuICBwb3B1cC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGUgPT4ge1xyXG4gICAgZS5zaG91bGROb3RDbG9zZVBvcHVwID0gdHJ1ZTtcclxuICB9KTtcclxufSIsImltcG9ydCB7IGdyYXBoIH0gZnJvbSAnLi4vZ3JhcGgnXG5pbXBvcnQgeyBtYWtlVG9nZ2xlIH0gZnJvbSAnLi9mbidcbmltcG9ydCByZW5kZXIgZnJvbSAnLi4vcmVuZGVyJ1xuXG5jb25zdCBwcmVmZXJyZWRFZGdlTGVuZ3RoID0gMTAwO1xuY29uc3QgcHJlZmVycmVkRGlzdGFuY2UgPSAxMDA7XG5jb25zdCBmb3JjZUZhY3RvciA9IDAuMDU7XG5jb25zdCByZXR1cm5TcGVlZCA9IDE7XG5cbmxldCBpc1BoeXNpY3NBY3RpdmUgPSBmYWxzZTtcblxubWFrZVRvZ2dsZSgnI2ZvcmNlJywgKHN0KSA9PiB7XG4gIGlzUGh5c2ljc0FjdGl2ZSA9IHN0O1xuICBpZiAoc3QpXG4gICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKHN0ZXApO1xufSk7XG5cbmZ1bmN0aW9uIHN0ZXAoKSB7XG4gIGlmICghaXNQaHlzaWNzQWN0aXZlKVxuICAgIHJldHVybjtcbiAgXG4gIGxldCByZXN1bHRpbmcgPSB7fTtcbiAgXG4gIGZvciAobGV0IGkgb2YgZ3JhcGgudmVydGV4LmtleXMpIHtcbiAgICAvLyBEbyBub3QgYXBwbHkgb24gc2VsZWN0ZWRcbiAgICBpZiAocmVuZGVyLnNlbGVjdGlvbi50eXBlID09ICd2ZXJ0ZXgnICYmIHJlbmRlci5zZWxlY3Rpb24uaWQgPT0gaSlcbiAgICAgIGNvbnRpbnVlO1xuICAgIGxldCBhID0gZ3JhcGgudmVydGV4Lm1hcFtpXTtcbiAgICBsZXQgeCA9IGEueCwgeSA9IGEueTtcbiAgICBmb3IgKGxldCBqIG9mIGdyYXBoLnZlcnRleC5rZXlzKSB7XG4gICAgICBsZXQgYiA9IGdyYXBoLnZlcnRleC5tYXBbal07XG4gICAgICAvLyBjb25zb2xlLmxvZyhpLCBhLCBqLCBiKTtcbiAgICAgIGxldCBkeCA9IGEueCAtIGIueCxcbiAgICAgICAgICBkeSA9IGEueSAtIGIueTtcbiAgICAgIGlmIChkeCA9PSAwKSBkeCA9IE1hdGgucmFuZG9tKCkgKiAyIC0gMTtcbiAgICAgIGlmIChkeSA9PSAwKSBkeSA9IE1hdGgucmFuZG9tKCkgKiAyIC0gMTtcbiAgICAgIGxldCBkID0gTWF0aC5zcXJ0KGR4KmR4ICsgZHkqZHkpO1xuICAgICAgaWYgKGkgPT0gailcbiAgICAgICAgY29udGludWU7XG4gICAgICBsZXQgZm91bmQgPSBmYWxzZTtcbiAgICAgIGZvciAobGV0IHEgb2YgYS5jb25uZWN0ZWQpXG4gICAgICAgIGlmIChmb3VuZCB8fCBncmFwaC5lZGdlcy5tYXBbcV0uZW5kID09IGopIHtcbiAgICAgICAgICBmb3VuZCA9IHRydWU7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIGZvciAobGV0IHEgb2YgYS5pbnZlcnNlKVxuICAgICAgICBpZiAoZm91bmQgfHwgZ3JhcGguZWRnZXMubWFwW3FdLnN0YXJ0ID09IGopIHtcbiAgICAgICAgICBmb3VuZCA9IHRydWU7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIGlmIChmb3VuZCkge1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhpLCAnY29ubmVjdGVkIHRvJywgailcbiAgICAgICAgeCAtPSBkeCAvIGQgKiAoZC1wcmVmZXJyZWRFZGdlTGVuZ3RoKSAqIGZvcmNlRmFjdG9yO1xuICAgICAgICB5IC09IGR5IC8gZCAqIChkLXByZWZlcnJlZEVkZ2VMZW5ndGgpICogZm9yY2VGYWN0b3I7XG4gICAgICB9IGVsc2UgaWYgKGQgPCBwcmVmZXJyZWREaXN0YW5jZSkge1xuICAgICAgICB4IC09IGR4IC8gZCAqIChkLXByZWZlcnJlZERpc3RhbmNlKSAqIGZvcmNlRmFjdG9yO1xuICAgICAgICB5IC09IGR5IC8gZCAqIChkLXByZWZlcnJlZERpc3RhbmNlKSAqIGZvcmNlRmFjdG9yO1xuICAgICAgfVxuICAgIH1cbiAgICByZXN1bHRpbmdbaV0gPSB7IHg6IHgsIHk6IHkgfTtcbiAgfVxuXG4gIGZvciAobGV0IGkgb2YgZ3JhcGgudmVydGV4LmtleXMpIHtcbiAgICAvLyBjb25zb2xlLmxvZygnTW92ZScsIGksICd0bycsIHJlc3VsdGluZ1tpXSk7XG4gICAgaWYgKHJlc3VsdGluZ1tpXSlcbiAgICAgIGdyYXBoLm1vdmVWZXJ0ZXgoaSwgcmVzdWx0aW5nW2ldLngsIHJlc3VsdGluZ1tpXS55KVxuICB9XG5cbiAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKHN0ZXApO1xufSIsImltcG9ydCB7IGFjdGl2ZVRvb2wgfSBmcm9tICcuL3N0YXRlJ1xyXG5pbXBvcnQgcmVuZGVyLCB7IHJlbmRlckV2ZW50cyB9IGZyb20gJy4uL3JlbmRlcidcclxuaW1wb3J0IHsgZGVmdG9vbCB9IGZyb20gJy4vZm4nXHJcblxyXG5sZXQgaXNBY3RpdmUgPSBmYWxzZTtcclxubGV0IHggPSAwO1xyXG5sZXQgeSA9IDA7XHJcblxyXG5yZW5kZXJFdmVudHMub24oJ21vdXNlLWRvd24nLCBlID0+IHtcclxuICBpZiAoZS5jdHJsS2V5IHx8IGFjdGl2ZVRvb2wubmFtZSA9PSAnbW92ZScpIHtcclxuICAgIGlzQWN0aXZlID0gdHJ1ZTtcclxuICAgIHggPSByZW5kZXIucG9zaXRpb24ueCAtIGUuY2xpZW50WDtcclxuICAgIHkgPSByZW5kZXIucG9zaXRpb24ueSAtIGUuY2xpZW50WTtcclxuICB9XHJcbn0pO1xyXG5cclxucmVuZGVyRXZlbnRzLm9uKCdtb3VzZS1tb3ZlJywgZSA9PiB7XHJcbiAgaWYgKGUuY3RybEtleSB8fCBhY3RpdmVUb29sLm5hbWUgPT0gJ21vdmUnKSB7XHJcbiAgICBpZiAoIWlzQWN0aXZlKSByZXR1cm47XHJcbiAgICByZW5kZXIubW92ZShcclxuICAgICAgeCArIGUuY2xpZW50WCxcclxuICAgICAgeSArIGUuY2xpZW50WVxyXG4gICAgKTtcclxuICB9XHJcbn0pO1xyXG5cclxucmVuZGVyRXZlbnRzLm9uKCdtb3VzZS11cCcsIGUgPT4ge1xyXG4gIGlzQWN0aXZlID0gZmFsc2U7XHJcbn0pXHJcblxyXG5kZWZ0b29sKFwiI21vdmVcIiwgJ21vdmUnLCB7IGFsbG93U2VsZWN0aW9uOiBmYWxzZSB9KSIsImltcG9ydCByZW5kZXIsIHsgcmVuZGVyRXZlbnRzIH0gZnJvbSBcIi4uL3JlbmRlclwiXG5pbXBvcnQgeyBncmFwaCB9IGZyb20gXCIuLi9ncmFwaFwiXG5pbXBvcnQgeyBkZWZ0b29sIH0gZnJvbSBcIi4vZm5cIjtcbmltcG9ydCB7IGFjdGl2ZVRvb2wgfSBmcm9tIFwiLi9zdGF0ZVwiXG5cbmxldCByZW5hbWVJbnB1dCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNsYWJlbCcpO1xubGV0IGFjdGl2ZVZlcnRleCA9IG51bGw7XG5cbnJlbmRlckV2ZW50cy5vbigndmVydGV4LW1vdXNlLWRvd24nLCAodngsIGV2dCkgPT4ge1xuICBpZiAoYWN0aXZlVG9vbC5uYW1lID09ICdyZW5hbWUnKSB7XG4gICAgcmVuYW1lSW5wdXQuc3R5bGUubGVmdCA9IGdyYXBoLnZlcnRleC5tYXBbdnhdLnggKyByZW5kZXIucG9zaXRpb24ueCArICdweCc7XG4gICAgcmVuYW1lSW5wdXQuc3R5bGUudG9wID0gZ3JhcGgudmVydGV4Lm1hcFt2eF0ueSArIHJlbmRlci5wb3NpdGlvbi55ICsgJ3B4JztcbiAgICByZW5hbWVJbnB1dC52YWx1ZSA9IGdyYXBoLnZlcnRleC5tYXBbdnhdLmxhYmVsO1xuICAgIC8vIEJ1ZyBpbiBmaXJlZm94P1xuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgcmVuYW1lSW5wdXQuZm9jdXMoKVxuICAgICAgcmVuYW1lSW5wdXQuc2VsZWN0KClcbiAgICB9LCAxKTtcbiAgICBhY3RpdmVWZXJ0ZXggPSB2eDtcbiAgfVxufSk7XG5cbnJlbmFtZUlucHV0LmFkZEV2ZW50TGlzdGVuZXIoJ2lucHV0JywgKCkgPT4ge1xuICBpZiAoYWN0aXZlVmVydGV4KVxuICAgIGdyYXBoLnNldExhYmVsKGFjdGl2ZVZlcnRleCwgcmVuYW1lSW5wdXQudmFsdWUpO1xufSlcblxuZGVmdG9vbCgnI3JlbmFtZScsICdyZW5hbWUnKTsiLCJpbXBvcnQgeyBhY3RpdmVUb29sIH0gZnJvbSAnLi9zdGF0ZSdcclxuaW1wb3J0IHJlbmRlciwgeyByZW5kZXJFdmVudHMgfSBmcm9tICcuLi9yZW5kZXInXHJcbmltcG9ydCB7IGdyYXBoIH0gZnJvbSAnLi4vZ3JhcGgnXHJcbmltcG9ydCB7IGRlZnRvb2wsIG1ha2VUb2dnbGUgfSBmcm9tICcuL2ZuJ1xyXG5cclxubGV0IGRyYWcgPSB7XHJcbiAgYWN0aXZlOiBmYWxzZSxcclxuICB4OiAwLFxyXG4gIHk6IDAsXHJcbiAgc25hcDogZmFsc2VcclxufVxyXG5cclxucmVuZGVyRXZlbnRzLm9uKCdtb3VzZS1tb3ZlJywgZSA9PiB7XHJcbiAgLy9jb25zb2xlLmxvZygnbW91c2UtbW92ZScpXHJcbiAgaWYgKGRyYWcuYWN0aXZlICYmIGFjdGl2ZVRvb2wubmFtZSA9PSAnc2VsZWN0Jykge1xyXG4gICAgaWYgKCFkcmFnLmFjdGl2ZSkgcmV0dXJuO1xyXG4gICAgbGV0IHggPSBkcmFnLnggKyBlLmNsaWVudFgsXHJcbiAgICAgICAgeSA9IGRyYWcueSArIGUuY2xpZW50WVxyXG4gICAgZ3JhcGgubW92ZVZlcnRleChcclxuICAgICAgcmVuZGVyLnNlbGVjdGlvbi5pZCxcclxuICAgICAgZHJhZy5zbmFwID8gTWF0aC5yb3VuZCh4IC8gMTApICogMTAgOiB4LFxyXG4gICAgICBkcmFnLnNuYXAgPyBNYXRoLnJvdW5kKHkgLyAxMCkgKiAxMCA6IHlcclxuICAgICk7XHJcbiAgfVxyXG59KTtcclxuXHJcbnJlbmRlckV2ZW50cy5vbignbW91c2UtdXAnLCBlID0+IHtcclxuICAvL2NvbnNvbGUubG9nKCdtb3VzZS11cCcpXHJcbiAgZHJhZy5hY3RpdmUgPSBmYWxzZTtcclxufSk7XHJcblxyXG5yZW5kZXJFdmVudHMub24oJ3ZlcnRleC1tb3VzZS1kb3duJywgKGtleSwgZSkgPT4ge1xyXG4gIC8vY29uc29sZS5sb2coJ3ZlcnRleC1tb3VzZS1kb3duJylcclxuICBkcmFnLmFjdGl2ZSA9IHRydWU7XHJcbiAgZHJhZy54ID0gZ3JhcGgudmVydGV4Lm1hcFtrZXldLnggLSBlLmNsaWVudFg7XHJcbiAgZHJhZy55ID0gZ3JhcGgudmVydGV4Lm1hcFtrZXldLnkgLSBlLmNsaWVudFk7XHJcbn0pXHJcblxyXG5tYWtlVG9nZ2xlKCcjc25hcC1ncmlkJywgKHN0KSA9PiBkcmFnLnNuYXAgPSBzdClcclxuZGVmdG9vbChcIiNzZWxlY3RcIiwgJ3NlbGVjdCcpIiwiaW1wb3J0IHJlbmRlciwgeyByZW5kZXJFdmVudHMgfSBmcm9tICcuLi9yZW5kZXInXHJcblxyXG5leHBvcnQgbGV0IGFjdGl2ZVRvb2wgPSB7XHJcbiAgbmFtZTogbnVsbCxcclxuICBvcHRzOiB7fVxyXG59XHJcblxyXG5yZW5kZXJFdmVudHMub24oJ3ZlcnRleC1tb3VzZS1kb3duJywgKGtleSkgPT4ge1xyXG4gIGlmIChhY3RpdmVUb29sLm9wdHMuYWxsb3dTZWxlY3Rpb24pXHJcbiAgICByZW5kZXIuc2VsZWN0KCd2ZXJ0ZXgnLCBrZXkpO1xyXG59KVxyXG5cclxucmVuZGVyRXZlbnRzLm9uKCdlZGdlLW1vdXNlLWRvd24nLCAoa2V5KSA9PiB7XHJcbiAgaWYgKGFjdGl2ZVRvb2wub3B0cy5hbGxvd1NlbGVjdGlvbilcclxuICAgIHJlbmRlci5zZWxlY3QoJ2VkZ2UnLCBrZXkpO1xyXG59KSIsIi8vIFRoZSBtb2R1bGUgY2FjaGVcbnZhciBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX18gPSB7fTtcblxuLy8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbmZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG5cdGlmKF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF0pIHtcblx0XHRyZXR1cm4gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXS5leHBvcnRzO1xuXHR9XG5cdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG5cdHZhciBtb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdID0ge1xuXHRcdC8vIG5vIG1vZHVsZS5pZCBuZWVkZWRcblx0XHQvLyBubyBtb2R1bGUubG9hZGVkIG5lZWRlZFxuXHRcdGV4cG9ydHM6IHt9XG5cdH07XG5cblx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG5cdF9fd2VicGFja19tb2R1bGVzX19bbW9kdWxlSWRdKG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG5cdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG5cdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbn1cblxuIiwiLy8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbl9fd2VicGFja19yZXF1aXJlX18ubiA9IChtb2R1bGUpID0+IHtcblx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG5cdFx0KCkgPT4gKG1vZHVsZVsnZGVmYXVsdCddKSA6XG5cdFx0KCkgPT4gKG1vZHVsZSk7XG5cdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsIHsgYTogZ2V0dGVyIH0pO1xuXHRyZXR1cm4gZ2V0dGVyO1xufTsiLCIvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9ucyBmb3IgaGFybW9ueSBleHBvcnRzXG5fX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSAoZXhwb3J0cywgZGVmaW5pdGlvbikgPT4ge1xuXHRmb3IodmFyIGtleSBpbiBkZWZpbml0aW9uKSB7XG5cdFx0aWYoX193ZWJwYWNrX3JlcXVpcmVfXy5vKGRlZmluaXRpb24sIGtleSkgJiYgIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBrZXkpKSB7XG5cdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywga2V5LCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZGVmaW5pdGlvbltrZXldIH0pO1xuXHRcdH1cblx0fVxufTsiLCJfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSAob2JqLCBwcm9wKSA9PiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iaiwgcHJvcCkpIiwiLy8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuX193ZWJwYWNrX3JlcXVpcmVfXy5yID0gKGV4cG9ydHMpID0+IHtcblx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG5cdH1cblx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbn07IiwiaW1wb3J0IHsgZ3JhcGggfSBmcm9tICcuL2pzL2dyYXBoJ1xyXG5pbXBvcnQgJy4vanMvcmVuZGVyJ1xyXG5pbXBvcnQgJy4vanMvdG9vbHMvYWxsJ1xyXG5cclxubGV0IHYxID0gZ3JhcGguYWRkVmVydGV4KDAsIDEwMCwgXCJhXCIpO1xyXG5sZXQgdjIgPSBncmFwaC5hZGRWZXJ0ZXgoMTAwLCAwLCBcImJcIik7XHJcbmdyYXBoLmFkZEVkZ2UodjEsIHYyKTtcclxuXHJcbmxldCB2MyA9IGdyYXBoLmFkZFZlcnRleCgyMDAsIDIwMCwgXCIxXCIpO1xyXG5sZXQgdjQgPSBncmFwaC5hZGRWZXJ0ZXgoMTAwLCAzMDAsIFwiMlwiKTtcclxubGV0IHY1ID0gZ3JhcGguYWRkVmVydGV4KDIwMCwgNDAwLCBcIjNcIik7XHJcbmxldCB2NiA9IGdyYXBoLmFkZFZlcnRleCgzMDAsIDMwMCwgXCI0XCIpO1xyXG5ncmFwaC5hZGRFZGdlKHYzLCB2NCwgJycsIC01MCk7XHJcbmdyYXBoLmFkZEVkZ2UodjQsIHY1LCAnJywgLTUwKTtcclxuZ3JhcGguYWRkRWRnZSh2NSwgdjYsICcnLCAtNTApO1xyXG5ncmFwaC5hZGRFZGdlKHY2LCB2MywgJycsIC01MCk7XHJcblxyXG4vLyBsb2FkLmxpc3QoJyAxICAgMiAgXFxuMiAzXFxuMyA0XFxudGhpcyB0aGF0XFxuYSBiIDEwXFxuMTIzXFxuXFxuMTIzIDQ1NiA3ODkgMTAxMScpIl0sInNvdXJjZVJvb3QiOiIifQ==