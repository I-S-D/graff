import { graph } from './js/graph'
import './js/render'
import './js/tools/all'

let v1 = graph.addVertex(0, 100, "a");
let v2 = graph.addVertex(100, 0, "b");
graph.addEdge(v1, v2);

let v3 = graph.addVertex(200, 200, "1");
let v4 = graph.addVertex(100, 300, "2");
let v5 = graph.addVertex(200, 400, "3");
let v6 = graph.addVertex(300, 300, "4");
graph.addEdge(v3, v4, '', -50);
graph.addEdge(v4, v5, '', -50);
graph.addEdge(v5, v6, '', -50);
graph.addEdge(v6, v3, '', -50);

// load.list(' 1   2  \n2 3\n3 4\nthis that\na b 10\n123\n\n123 456 789 1011')