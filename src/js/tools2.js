let state = {
  grab: {
    active: false,
    x: 0,
    y: 0
  },
  drag: {
    active: false,
    x: 0,
    y: 0
  },
  tool: {
    name: null,
    opts: {}
  }
};

let styleButton = document.querySelector('#style');
let vertexStyles = document.querySelectorAll('#vertex-style .presets div');
let renameInput = document.querySelector('#label');

document.addEventListener('click', e => {
  if (!e.shouldNotClosePopup && currentPopup)
    currentPopup.style.display = 'none';
})


render.dom.svg.addEventListener('mousedown', e => {
  if (e.ctrlKey || state.tool.name == 'move') {
    state.grab.active = true;
    state.grab.x = render.position.x - e.clientX;
    state.grab.y = render.position.y - e.clientY;
  }
  render.select(null);
});

render.dom.svg.addEventListener('click', e => {
  if (state.tool.name == 'add-vertex') {
    let v = graph.addVertex(e.clientX - render.position.x, e.clientY - render.position.y);
    render.select('vertex', v);
  }
});

render.dom.svg.addEventListener('mousemove', e => {
  if (e.ctrlKey || state.tool.name == 'move') {
    if (!state.grab.active) return;
    render.move(
      state.grab.x + e.clientX,
      state.grab.y + e.clientY
    );
  } else if((e.shiftKey || state.tool.name == 'select') && state.drag.active) {
    graph.moveVertex(render.selection.id, state.drag.x + e.clientX, state.drag.y + e.clientY);
  }
});

render.dom.svg.addEventListener('mouseup', e => {
  if (e.ctrlKey || state.tool.name == 'move') {
    state.grab.active = false;
  } else if((e.shiftKey || state.tool.name == 'select') && state.drag.active) {
    state.drag.active = false;
  }
})

listeners.onVertexCreated = (key) => {
  render.dom.vertex[key].group.addEventListener('mousedown', (e) => {
    e.stopPropagation();
    if(state.tool.name == 'add-edge' && state.tool.opts.mode == 'next') {
      graph.addEdge(state.tool.opts.start, key);
      state.tool.opts.mode = null;
    } else if(state.tool.name == 'add-edge') {
      state.tool.opts.mode = 'next';
      state.tool.opts.start = key;
    } else if(state.tool.name == 'rename') {
      let vertex = graph.vertex.map[key];
      state.tool.opts.vertex = key;
      renameInput.setAttribute('style', `--x: ${vertex.x + render.position.x}px; --y: ${vertex.y + render.position.y}; --d: ${render.style.radius*2-4}; --fg: ${render.style.vertex[vertex.style].fg}; --bg: ${render.style.vertex[vertex.style].bg}`);
      renameInput.value = graph.vertex.map[key].label;
      setTimeout(() => renameInput.focus(), 10);
    }
    if (!state.tool.opts.allowSelection)
      return;
    render.select('vertex', key);
    state.drag.active = true;
    state.drag.x = graph.vertex.map[key].x - e.clientX;
    state.drag.y = graph.vertex.map[key].y - e.clientY;
  })
};

listeners.onEdgeCreated = (key) => {
  render.dom.edge[key].clickZone.addEventListener('mousedown', (e) => {
    e.stopPropagation();
    if (!state.tool.opts.allowSelection)
      return;
    render.select('edge', key);
  })
};

document.addEventListener('keydown', e => {
  if(e.key == 'Delete' && render.selection.type && e.target.tagName != 'INPUT') {
    if(render.selection.type == 'vertex') {
      graph.destroyVertex(render.selection.id);
    } else if(render.selection.type == 'edge') {
      graph.destroyEdge(render.selection.id);
    }
  }
});

listeners.onSelectionChanged = (type, key) => {
  if (!type)
    styleButton.setAttribute('disabled', 'disabled');
  else
    styleButton.removeAttribute('disabled');
}

vertexStyles.forEach((e, i) => {
  e.setAttribute('style', `--bg: ${render.style.vertex[i].bg}; --fg: ${render.style.vertex[i].fg}; --br: ${render.style.vertex[i].br};`)
  e.addEventListener('click', () => {
    graph.restyleVertex(render.selection.id, i);
  })
})

renameInput.addEventListener('keyup', (e) => {
  if (render.selection.type == 'vertex')
    graph.setLabel(render.selection.id, renameInput.value);
  if (e.key == 'Enter') {
    renameInput.style.opacity = '0';
    state.tool.opts.couldMove = true;
  }
})

renameInput.addEventListener('click', () => {
  render.select('vertex', state.tool.opts.vertex);
})

renameInput.addEventListener('focus', e => {
  renameInput.style.opacity = '1';
  state.tool.opts.couldMove = false;
  renameInput.setSelectionRange(0, renameInput.value.length);
})

renameInput.addEventListener('blur', e => {
  renameInput.style.opacity = '0';
  state.tool.opts.couldMove = true;
})

deftool('#select', 'select');
deftool('#move', 'move', { allowSelection: false });
deftool('#add-vertex', 'add-vertex');
deftool('#add-edge', 'add-edge');
deftool('#rename', 'rename', {couldMove: true}, () => {
  if (render.selection.type == 'vertex') {
    let vertex = graph.vertex.map[render.selection.id];
    tate.tool.opts.vertex = key;
    renameInput.setAttribute('style', `--x: ${vertex.x + render.position.x}px; --y: ${vertex.y + render.position.y}; --d: ${render.style.radius*2-4}; --fg: ${render.style.vertex[vertex.style].fg}; --bg: ${render.style.vertex[vertex.style].bg}`);
    renameInput.value = graph.vertex.map[key].label;
    setTimeout(() => renameInput.focus(), 10);
  }
});

makePopup('#vertex-style', '#style', _ => render.selection.type == 'vertex');
makePopup('#matrix-dialog', '#matrix');
makePopup('#list-dialog', '#list');