import { graph } from './graph'
import engine from './events'
import { EventEmitter } from 'events'

// На каком расстоянии от вершины ставим перпендикуляр ребру
// с контрольными точками кривой Безье (для гнутых рёбер)
const splitCurve = 0.3;

const nSplitCurve = 1 - splitCurve;

const ns = 'http://www.w3.org/2000/svg';

export const renderEvents = new EventEmitter()

/** 
 * SVG рендер
*/
const render = {
  dom: {
    vertex: {},
    edge: {},
    vertexGroup: document.querySelector('.workspace svg #vertex-group'),
    edgeGroup: document.querySelector('.workspace svg #edge-group'),
    svg: document.querySelector('.workspace svg'),
    workspace: document.querySelector('.workspace'),
    selectionCircle: document.querySelector('circle#selection')
  },
  position: {
    x: 0,
    y: 0
  },
  wiewport: {
    width: 100,
    height: 100,
    halfwidth: 50,
    halfheight: 50,
    offsetX: 50,
    offsetY: 50
  },
  style: {
    vertex: [
      { bg: '#fff', fg: '#000', br: '#000' },
      { bg: '#000', fg: '#fff', br: '#000' },
      { bg: '#bbb', fg: '#000', br: '#000' }
    ],
    edge: [
      0,
      2,
      10,
      '5 5 2 5'
    ],
    radius: 20
  },
  selection: {
    /** @type {null | 'vertex' | 'edge'} */
    type: null,
    id: null,
  },
  addVertex(key) {
    console.log('%cRENDER%c Add vertex %cSymbol(' + (key.description ?? '') + ')',
      'color: white; background: #4c9692; padding: 5px 10px; display: inline-block;',
      'display: inline-block; padding: 5px 5px',
      'display: inline-block; padding: 5px 0px; color: #4c9692');
    let vertex = graph.vertex.map[key];
    let style = this.style.vertex[vertex.style]

    let group = document.createElementNS(ns, 'g');
    this.dom.vertexGroup.appendChild(group);

    let circ = document.createElementNS(ns, 'circle');
    circ.setAttributeNS(null, 'stroke-width', '2');
    circ.setAttributeNS(null, 'cx', this.wiewport.offsetX + vertex.x);
    circ.setAttributeNS(null, 'cy', this.wiewport.offsetY + vertex.y);
    circ.setAttributeNS(null, 'r', this.style.radius);
    circ.setAttributeNS(null, 'fill', this.style.vertex[vertex.style].bg);
    circ.setAttributeNS(null, 'stroke', this.style.vertex[vertex.style].br);
    group.appendChild(circ);

    let text = document.createElementNS(ns, 'text');
    text.setAttributeNS(null, 'x', this.wiewport.offsetX + vertex.x);
    text.setAttributeNS(null, 'y', this.wiewport.offsetY + vertex.y);
    text.setAttributeNS(null, 'fill', style.fg);
    text.textContent = vertex.label;

    group.appendChild(text);
    this.dom.vertex[key] = {
      group: group,
      circ: circ,
      text: text,
    };
    group.addEventListener('mousedown', (e) => renderEvents.emit('vertex-mouse-down', key, e))
    renderEvents.emit('vertex-added', key, this.dom.vertex[key])
  },
  destroyVertex(key) {
    console.log('%cRENDER%c Destroy vertex %cSymbol(' + (key.description ?? '') + ')',
      'color: white; background: #4c9692; padding: 5px 10px; display: inline-block;',
      'display: inline-block; padding: 5px 5px',
      'display: inline-block; padding: 5px 0px; color: #4c9692');
    if (this.selection.id == key) {
      this.select(null);
    }
    let vertex = this.dom.vertex[key];
    vertex.group.remove();
    vertex.circ.remove();
    vertex.text.remove();
    delete vertex.group;
    delete vertex.circ;
    delete vertex.text;
    delete this.dom.vertex[key];
    renderEvents.emit('vertex-destroyed', key)
  },
  addEdge(key) {
    console.log('%cRENDER%c Add edge %cSymbol(' + (key.description ?? '') + ')',
      'color: white; background: #4c9692; padding: 5px 10px; display: inline-block;',
      'display: inline-block; padding: 5px 5px',
      'display: inline-block; padding: 5px 0px; color: #4c9692');
    let start = graph.vertex.map[graph.edges.map[key].start];
    let end = graph.vertex.map[graph.edges.map[key].end];

    let group = document.createElementNS(ns, 'g');
    this.dom.edgeGroup.appendChild(group);

    let clickZone = document.createElementNS(ns, 'path');
    clickZone.setAttributeNS(null, 'fill', 'none');
    clickZone.setAttributeNS(null, 'stroke', '#4c9692');
    clickZone.setAttributeNS(null, 'opacity', '0');
    clickZone.setAttributeNS(null, 'stroke-width', '15');
    group.appendChild(clickZone);

    let visible = document.createElementNS(ns, 'path');
    visible.setAttributeNS(null, 'fill', 'none');
    visible.setAttributeNS(null, 'stroke-width', '2');
    visible.setAttributeNS(null, 'stroke', '#000');
    group.appendChild(visible);

    group.addEventListener('mousedown', (e) => renderEvents.emit('edge-mouse-down', key, e))

    this.dom.edge[key] = {
      group: group,
      visible: visible,
      clickZone: clickZone
    };
    renderEvents.emit('edge-added', key, this.dom.edge[key])
    this.repositionEdge(key);
  },
  destroyEdge(key) {
    console.log('%cRENDER%c Destroy edge %cSymbol(' + (key.description ?? '') + ')',
      'color: white; background: #4c9692; padding: 5px 10px; display: inline-block;',
      'display: inline-block; padding: 5px 5px',
      'display: inline-block; padding: 5px 0px; color: #4c9692');
    if (this.selection.id == key) {
      this.select(null);
    }
    let edge = this.dom.edge[key];
    edge.group.remove();
    edge.visible.remove();
    edge.clickZone.remove();
    delete edge.group;
    delete edge.visible;
    delete edge.clickZone;
    delete this.dom.edge[key];
    renderEvents.emit('edge-destroyed', key)
  },
  repositionEdge(id) {
    let edge = graph.edges.map[id];
    let start = graph.vertex.map[edge.start];
    let end = graph.vertex.map[edge.end];
    if (edge.bend) {
      let v = {
        x: end.x - start.x,
        y: end.y - start.y
      }
      let d = Math.sqrt(v.x * v.x + v.y * v.y);
      let path =
        `M ${start.x + this.wiewport.offsetX} ` +
        `${start.y + this.wiewport.offsetY} ` +
        `C ${start.x + splitCurve * v.x + v.y / d * edge.bend + this.wiewport.offsetX} ` +
        `${start.y + splitCurve * v.y - v.x / d * edge.bend + this.wiewport.offsetY} ` +
        `${start.x + nSplitCurve * v.x + v.y / d * edge.bend + this.wiewport.offsetX} ` +
        `${start.y + nSplitCurve * v.y - v.x / d * edge.bend + this.wiewport.offsetY} ` +
        `${end.x + this.wiewport.offsetX} ` +
        `${end.y + this.wiewport.offsetY}`;
      this.dom.edge[id].visible.setAttributeNS(null, 'd', path);
      this.dom.edge[id].clickZone.setAttributeNS(null, 'd', path);
    } else {
      let path =
        `M ${start.x + this.wiewport.offsetX} ${start.y + this.wiewport.offsetY} ` +
        `L ${end.x + this.wiewport.offsetX} ${end.y + this.wiewport.offsetY}`
      this.dom.edge[id].visible.setAttributeNS(null, 'd', path);
      this.dom.edge[id].clickZone.setAttributeNS(null, 'd', path);
    }
  },
  move(x, y) {
    this.position.x = x;
    this.position.y = y;
    this.dom.edgeGroup.setAttributeNS(null, 'transform', 'translate(' + this.position.x + ', ' + this.position.y + ')');
    this.dom.vertexGroup.setAttributeNS(null, 'transform', 'translate(' + this.position.x + ', ' + this.position.y + ')');
    this.dom.selectionCircle.setAttributeNS(null, 'transform', 'translate(' + this.position.x + ', ' + this.position.y + ')');
    document.body.style.backgroundPositionX = x % 100 - 5 + 'px';
    document.body.style.backgroundPositionY = y % 100 - 5 + 'px';
  },
  resize() {
    let rect = this.dom.workspace.getBoundingClientRect();
    this.dom.svg.setAttribute('width', rect.width);
    this.dom.svg.setAttribute('height', rect.height);
    this.wiewport.halfwidth = Math.round((this.wiewport.width = rect.width) / 2);
    this.wiewport.halfheight = Math.round((this.wiewport.height = rect.height) / 2);
    this.wiewport.offsetX = 0;
    this.wiewport.offsetY = 0;
    //this.reposition();
  },
  /**
   * 
   * @param {null|'vertex'|'edge'} type 
   * @param {Symbol} key 
   */
  select(type, key) {
    if (this.selection.type == 'vertex') {
      this.dom.selectionCircle.setAttributeNS(null, 'opacity', 0);
    } else if (this.selection.type == 'edge') {
      this.dom.edge[this.selection.id].clickZone.setAttributeNS(null, 'opacity', 0);
    }

    this.selection.type = type;
    this.selection.id = key;

    if (type == 'vertex') {
      this.dom.selectionCircle.setAttributeNS(null, 'opacity', 0.5);
      this.dom.selectionCircle.setAttributeNS(null, 'cx', graph.vertex.map[key].x);
      this.dom.selectionCircle.setAttributeNS(null, 'cy', graph.vertex.map[key].y);
    } else if (type == 'edge') {
      this.dom.edge[key].clickZone.setAttributeNS(null, 'opacity', 0.5);
    }
    renderEvents.emit('selection-changed', type, key)
  },
  moveSelection() {
    if (this.selection.type == 'vertex') {
      // this.dom.selectionCircle.setAttributeNS(null, 'opacity', 1);
      this.dom.selectionCircle.setAttributeNS(null, 'cx', graph.vertex.map[this.selection.id].x);
      this.dom.selectionCircle.setAttributeNS(null, 'cy', graph.vertex.map[this.selection.id].y);
    } else if (this.selection.type == 'edge') {
      // this.dom.edge[this.selection.id].clickZone.setAttributeNS(null, 'opacity', 1);
    } else {
      // this.dom.selectionCircle.setAttributeNS(null, 'opacity', 0);
    }
  },
  moveVertex(key) {
    let vertex = graph.vertex.map[key];
    this.dom.vertex[key].circ.setAttributeNS(null, 'cx', this.wiewport.offsetX + vertex.x);
    this.dom.vertex[key].circ.setAttributeNS(null, 'cy', this.wiewport.offsetY + vertex.y);
    this.dom.vertex[key].text.setAttributeNS(null, 'x', this.wiewport.offsetX + vertex.x);
    this.dom.vertex[key].text.setAttributeNS(null, 'y', this.wiewport.offsetY + vertex.y);
    for (let i of vertex.connected)
      this.repositionEdge(i);
    for (let i of vertex.inverse)
      this.repositionEdge(i);
    if (this.selection.type == 'vertex' && this.selection.id == key)
      this.moveSelection();
  },
  restyleVertex(key) {
    let vertex = graph.vertex.map[key];
    let dom = this.dom.vertex[key];
    dom.circ.setAttributeNS(null, 'fill', this.style.vertex[vertex.style].bg);
    dom.circ.setAttributeNS(null, 'stroke', this.style.vertex[vertex.style].br);
    dom.text.setAttributeNS(null, 'fill', this.style.vertex[vertex.style].fg);
  },
  setLabel(key) {
    this.dom.vertex[key].text.textContent = graph.vertex.map[key].label;
  }
}

render.dom.svg.addEventListener('mousemove', e => renderEvents.emit('mouse-move', e))
render.dom.svg.addEventListener('mousedown', e => renderEvents.emit('mouse-down', e))
document.addEventListener('mouseup', e => renderEvents.emit('mouse-up', e))

const load = {
  edges: {
    parse(str) {
      return str.split('\n')
        .map(i => i.split(' ').filter(i => i))
        .map(i => (i.length == 1 ? { type: 'vertex', label: i[0] } :
          i.length == 2 || i.length == 3 ? { type: 'edge', start: i[0], end: i[1], w: i[2] } :
            i.length == 0 ? { type: 'nothing' } :
              { type: 'something' }))
    }
  }
};

window.addEventListener('resize', _ => render.resize());
render.resize();
render.move(render.wiewport.halfwidth, render.wiewport.halfheight);

engine.on('addVertex', d => render.addVertex(d))
engine.on('addEdge', d => render.addEdge(d))
engine.on('destroyEdge', d => render.destroyEdge(d))
engine.on('destroyVertex', d => render.destroyVertex(d))
// engine.on('changeEdgeStyle', render.changeEdgeStyle)
engine.on('changeVertexStyle', d => render.restyleVertex(d))
engine.on('moveVertex', d => render.moveVertex(d))
engine.on('setLabel', d => render.setLabel(d))

console.log(render)

export default render;