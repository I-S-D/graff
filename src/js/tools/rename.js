import render, { renderEvents } from "../render"
import { graph } from "../graph"
import { deftool } from "./fn";
import { activeTool } from "./state"

let renameInput = document.querySelector('#label');
let activeVertex = null;

renderEvents.on('vertex-mouse-down', (vx, evt) => {
  if (activeTool.name == 'rename') {
    renameInput.style.left = graph.vertex.map[vx].x + render.position.x + 'px';
    renameInput.style.top = graph.vertex.map[vx].y + render.position.y + 'px';
    renameInput.value = graph.vertex.map[vx].label;
    // Bug in firefox?
    setTimeout(() => {
      renameInput.focus()
      renameInput.select()
    }, 1);
    activeVertex = vx;
  }
});

renameInput.addEventListener('input', () => {
  if (activeVertex)
    graph.setLabel(activeVertex, renameInput.value);
})

deftool('#rename', 'rename');