import { graph } from '../graph'
import { makeToggle } from './fn'
import render from '../render'

const preferredEdgeLength = 100;
const preferredDistance = 100;
const forceFactor = 0.05;
const returnSpeed = 1;

let isPhysicsActive = false;

makeToggle('#force', (st) => {
  isPhysicsActive = st;
  if (st)
    requestAnimationFrame(step);
});

function step() {
  if (!isPhysicsActive)
    return;
  
  let resulting = {};
  
  for (let i of graph.vertex.keys) {
    // Do not apply on selected
    if (render.selection.type == 'vertex' && render.selection.id == i)
      continue;
    let a = graph.vertex.map[i];
    let x = a.x, y = a.y;
    for (let j of graph.vertex.keys) {
      let b = graph.vertex.map[j];
      // console.log(i, a, j, b);
      let dx = a.x - b.x,
          dy = a.y - b.y;
      if (dx == 0) dx = Math.random() * 2 - 1;
      if (dy == 0) dy = Math.random() * 2 - 1;
      let d = Math.sqrt(dx*dx + dy*dy);
      if (i == j)
        continue;
      let found = false;
      for (let q of a.connected)
        if (found || graph.edges.map[q].end == j) {
          found = true;
          break;
        }
      for (let q of a.inverse)
        if (found || graph.edges.map[q].start == j) {
          found = true;
          break;
        }
      if (found) {
        // console.log(i, 'connected to', j)
        x -= dx / d * (d-preferredEdgeLength) * forceFactor;
        y -= dy / d * (d-preferredEdgeLength) * forceFactor;
      } else if (d < preferredDistance) {
        x -= dx / d * (d-preferredDistance) * forceFactor;
        y -= dy / d * (d-preferredDistance) * forceFactor;
      }
    }
    resulting[i] = { x: x, y: y };
  }

  for (let i of graph.vertex.keys) {
    // console.log('Move', i, 'to', resulting[i]);
    if (resulting[i])
      graph.moveVertex(i, resulting[i].x, resulting[i].y)
  }

  requestAnimationFrame(step);
}