import { activeTool } from './state'

let selected = null;
let selectedExit = () => {};

let renameInput = document.querySelector('#label');

function deepClone(obj) {
  const clObj = {};
  for(const i in obj) {
    if (obj[i] instanceof Object) {
      clObj[i] = deepClone(obj[i]);
      continue;
    }
    clObj[i] = obj[i];
  }
  return clObj;
}

export function deftool(sel, name, opts = {}, onActive = () => {}, onExit = () => {}) {
  let elem = document.querySelector(sel);
  opts.allowSelection = opts.allowSelection ?? true;

  let fn = () => {
    if (selected) {
      selected.classList.remove('active');
      selectedExit();
    }
    selected = elem;
    elem.classList.add('active');
    activeTool.name = name;
    activeTool.opts = deepClone(opts);;
    renameInput.setAttribute('style', "top: -100px; left: -100px;")
    onActive();
    selectedExit = onExit;
  };

  if (elem.classList.contains('active'))
    fn();
  elem.addEventListener('click', fn);
}

export function makeToggle(sel, callback, dflt=false) {
  let elem = document.querySelector(sel);
  let on = dflt;
  elem.addEventListener('click', (e) => {
    on = !on;
    elem.classList.toggle('active', on);
    callback(on);
  });
}

let currentPopup = null;

export function makePopup(selector, buttonSelector, when) {
  let button = document.querySelector(buttonSelector);
  let popup = document.querySelector(selector);
  popup.style.display = 'none';
  button.addEventListener('click', e => {
    if (when && !when())
      return;
    e.shouldNotClosePopup = true;
    if (currentPopup)
      currentPopup.style.display = 'none';
    popup.style.display = null;
    currentPopup = popup;
  });

  popup.addEventListener('click', e => {
    e.shouldNotClosePopup = true;
  });
}