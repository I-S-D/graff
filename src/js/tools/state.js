import render, { renderEvents } from '../render'

export let activeTool = {
  name: null,
  opts: {}
}

renderEvents.on('vertex-mouse-down', (key) => {
  if (activeTool.opts.allowSelection)
    render.select('vertex', key);
})

renderEvents.on('edge-mouse-down', (key) => {
  if (activeTool.opts.allowSelection)
    render.select('edge', key);
})