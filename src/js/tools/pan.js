import { activeTool } from './state'
import render, { renderEvents } from '../render'
import { deftool } from './fn'

let isActive = false;
let x = 0;
let y = 0;

renderEvents.on('mouse-down', e => {
  if (e.ctrlKey || activeTool.name == 'move') {
    isActive = true;
    x = render.position.x - e.clientX;
    y = render.position.y - e.clientY;
  }
});

renderEvents.on('mouse-move', e => {
  if (e.ctrlKey || activeTool.name == 'move') {
    if (!isActive) return;
    render.move(
      x + e.clientX,
      y + e.clientY
    );
  }
});

renderEvents.on('mouse-up', e => {
  isActive = false;
})

deftool("#move", 'move', { allowSelection: false })