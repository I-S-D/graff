import { graph } from "../graph"
import render, { renderEvents } from "../render"
import { deftool } from "./fn";
import { activeTool } from "./state"

let pathPreview = render.dom.svg.querySelector('#edge-preview');
let start = { x: 0, y: 0 }

render.dom.svg.addEventListener('click', e => {
  if (activeTool.name == 'add-vertex')
    graph.addVertex(e.clientX - render.position.x, e.clientY - render.position.y);
});

renderEvents.on('mouse-move', e => {
  if (activeTool.name == 'add-edge') {
    if (activeTool.opts.start)
      pathPreview.setAttributeNS(null, 'd', `m ${start.x} ${start.y} l ${e.clientX - start.x} ${e.clientY - start.y} z`)
  }
});

renderEvents.on('vertex-mouse-down', (key, e) => {
  if (activeTool.name == 'add-edge') {
    if (activeTool.opts.start) {
      graph.addEdge(activeTool.opts.start, key);
      activeTool.opts.start = null;
      pathPreview.style.opacity = 0;
    } else {
      activeTool.opts.start = key;
      start.x = graph.vertex.map[key].x + render.position.x;
      start.y = graph.vertex.map[key].y + render.position.y;
      pathPreview.setAttributeNS(null, 'd', `m ${start.x} ${start.y} l ${e.clientX - start.x} ${e.clientY - start.y} z`)
      pathPreview.style.opacity = 1;
    }
  }
})

deftool("#add-vertex", 'add-vertex')
deftool("#add-edge", 'add-edge', { allowSelection: false }, () => {
  render.select(null);
}, () => {
  pathPreview.style.opacity = 0;
});