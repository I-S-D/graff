import { activeTool } from './state'
import render, { renderEvents } from '../render'
import { graph } from '../graph'
import { deftool, makeToggle } from './fn'

let drag = {
  active: false,
  x: 0,
  y: 0,
  snap: false
}

renderEvents.on('mouse-move', e => {
  //console.log('mouse-move')
  if (drag.active && activeTool.name == 'select') {
    if (!drag.active) return;
    let x = drag.x + e.clientX,
        y = drag.y + e.clientY
    graph.moveVertex(
      render.selection.id,
      drag.snap ? Math.round(x / 10) * 10 : x,
      drag.snap ? Math.round(y / 10) * 10 : y
    );
  }
});

renderEvents.on('mouse-up', e => {
  //console.log('mouse-up')
  drag.active = false;
});

renderEvents.on('vertex-mouse-down', (key, e) => {
  //console.log('vertex-mouse-down')
  drag.active = true;
  drag.x = graph.vertex.map[key].x - e.clientX;
  drag.y = graph.vertex.map[key].y - e.clientY;
})

makeToggle('#snap-grid', (st) => drag.snap = st)
deftool("#select", 'select')