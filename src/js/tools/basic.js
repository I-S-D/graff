import { graph } from "../graph";
import render from "../render";

document.addEventListener('keydown', function(e) {
  // Ignore events in input fields
  if (['input', 'textarea'].includes(e.target.tagName.toLowerCase()))
    return;
  if (e.key == 'Delete') {
    if (render.selection.type == 'vertex')
      graph.destroyVertex(render.selection.id);
    else if (render.selection.type == 'edge')
      graph.destroyEdge(render.selection.id);
  }
});