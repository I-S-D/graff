import engine from './events'

export const graph = {
  vertex: {
    keys: new Set(),
    map: {}
  },
  edges: {
    keys: new Set(),
    map: {}
  },

  /**
   * Добавить новую вершину в граф
   * @param {number} x
   * @param {number} y
   * @returns {Symbol}
   */
  addVertex(x, y, label='#', style = 0) {
    let key = Symbol(label);
    this.vertex.keys.add(key);
    this.vertex.map[key] = {
      x: x,
      y: y,
      label: label,
      style: style,
      connected: new Set(),
      inverse: new Set()
    };
    engine.emit('addVertex', key);
    return key;
  },

  /**
   * Добавить новое ребро
   * @param {Symbol} start
   * @param {Symbol} end
   * @returns {Symbol}
   */
  addEdge(start, end, label='10', bend = 0, style = 0, bidirectional = true) {
    let key = Symbol();
    this.edges.keys.add(key);
    this.edges.map[key] = {
      start: start,
      end: end,
      distance: null,
      bend: bend,
      style: style,
      bidirectional: bidirectional
    };
    this.vertex.map[start].connected.add(key);
    this.vertex.map[end].inverse.add(key);
    engine.emit('addEdge', key);
    return key;
  },

  /**
   * Удалить какую-то вершину
   * @param {Symbol} key 
   */
  destroyEdge(key) {
    this.vertex.map[this.edges.map[key].end].inverse.delete(key);
    this.vertex.map[this.edges.map[key].start].connected.delete(key);
    delete this.edges.map[key];
    this.edges.keys.delete(key);
    engine.emit('destroyEdge', key);
  },

  /**
   * Удаляем вершину
   * @param {Symbol} key 
   */
  destroyVertex(key) {
    for (let i of this.vertex.map[key].connected) {
      this.destroyEdge(i);
    }
    for (let i of this.vertex.map[key].inverse) {
      this.destroyEdge(i);
    }
    delete this.vertex.map[key];
    this.vertex.keys.delete(key);
    engine.emit('destroyVertex', key);
  },

  /**
   * Меняем стиль ребра
   * @param {Symbol} key 
   * @param {number} newStyle 
   */
  changeEdgeStyle(key, newStyle) {
    this.edges.map[key].style = newStyle;
    engine.emit('changeEdgeStyle', key);
  },

  /**
   * Меняем стиль вершины
   * @param {Symbol} key 
   * @param {number} newStyle 
   */
  changeVertexStyle(key, newStyle) {
    this.vertex.map[key].style = newStyle;
    engine.emit('changeVertexStyle', key);
  },
  /**
   * Передвигаем вершину
   * @param {Symbol} key 
   * @param {number} newX 
   * @param {number} newY 
   */
  moveVertex(key, newX, newY) {
    this.vertex.map[key].x = newX;
    this.vertex.map[key].y = newY;
    engine.emit('moveVertex', key);
  },

  setLabel(key, newLabel) {
    this.vertex.map[key].label = newLabel;
    engine.emit('setLabel', key);
  }
};